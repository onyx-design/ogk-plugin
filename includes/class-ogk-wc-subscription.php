<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

class OGK_WC_Subscription extends WC_Subscription {

	protected $shippable_statuses = array( 'active', 'pending-cancel' );

	/**
	 * Create a new shipment order for a Subscription
	 *
	 * @param   string      $fulfillment_month
	 * @return  WC_Order    created shipment order
	 */
	function create_shipment_order( $fulfillment_month = "now" ) {

		// Create a shipment order
		$shipment_order = wcs_create_order_from_subscription( $this, 'shipment_order' );
		WCS_Related_Order_Store::instance()->add_relation( $shipment_order, $this, 'shipment' );
		$shipment_order->set_status( 'processing' );

		// Generate Fulfillment Month metadata
		$fulfillment_month = OGK_Subscription_Dates::get_fulfillment_month( $fulfillment_month ); //dev:dates
		$shipment_order->update_meta_data( "_fulfillment_month", $fulfillment_month );

		$shipment_order->save();

		return $shipment_order;
	}

	/**
	 * DEV:NOTE - This Function is currently unused
	 * Get the billing interval in months (regardless of actual billing_period unit)
	 *
	 * @return  int
	 */
	function get_billing_interval_months() {
		$billing_interval        = (int) $this->get_billing_interval();
		$billing_period          = $this->get_billing_period();
		$billing_interval_months = 0;

		if ( 'month' == $billing_period ) {
			$billing_interval_months = $billing_interval;
		} elseif ( 'year' == $billing_period ) {
			$billing_interval_months = $billing_interval * 12;
		}

		return $billing_interval_months;
	}

	/**
	 * DEV:NOTE - This function is currently unused
	 * Get last shipment order date
	 *
	 * DEV:NOTE - What, specifically, does strict entail
	 * @param   bool    $strict only return ???
	 * @return  WC_Order|WC_Order_Refund|bool
	 */
	function get_last_shipment_order( $strict = false ) {
		$order_types = $strict ? array( 'shipment' ) : array( 'any', 'shipment' );

		return $this->get_last_order( 'all', $order_types );
	}

	/**
	 * Get the date of next payment or end date
	 *
	 * @return  WC_DateTime|null
	 */
	function get_end_or_next_payment_date( $timezone = 'gmt' ) {
		$next_payment_date = $this->get_date( 'next_payment', $timezone );

		return $next_payment_date ?: $this->get_date( 'end', $timezone );
	}

	/**
	 * Check if subscription has had a shipment this month
	 * - Only used in ONYX_Operation_Export_Subscriptions //dev:dates
	 *
	 * @return  bool    whether sub had a shipment
	 */
	function had_shipment_this_month() {
		$last_shipment_order_date = $this->get_date( 'last_shipment_order' ); 

		if ( empty( $last_shipment_order_date ) ) {
			return false;
		}

		$last_shipment_month = OGK_Subscription_Dates::get_fulfillment_month( $last_shipment_order_date );

		return ( $last_shipment_month === OGK_Subscription_Dates::get_fulfillment_month() );
	}

	/**
	 * Check if subscription had shipment for a given month
	 * - Only used in ONYX_Operation_Generate_Missed_Shipments //dev:dates
	 *
	 * @param   string  $check_fulfillment_month the month to check
	 * @return  bool    whether sub had a shipment
	 */
	function had_shipment_for_month( $check_fulfillment_month, $return_count = false ) {
		$check_fulfillment_month = OGK_Subscription_Dates::get_fulfillment_month( $check_fulfillment_month );

		if ( !$check_fulfillment_month ) {
			return false;
		}

		$shipment_orders = $this->get_related_orders( 'all', array( 'any', 'shipment' ) );
		$order_count = 0;

		foreach ( $shipment_orders as $shipment_order ) {
			$order_fulfillment_month = get_post_meta( $shipment_order->get_id(), '_fulfillment_month', true );

			if ( !$order_fulfillment_month ) {
				$order_fulfillment_month = $shipment_order->get_date_created();
			}

			if ( $order_fulfillment_month ) {
				$order_fulfillment_month = OGK_Subscription_Dates::get_fulfillment_month( $order_fulfillment_month );

				if ( $check_fulfillment_month == $order_fulfillment_month ) {

					if( $return_count ) {
						$order_count++;
					}
					else {
						return true;
					}
				}

			}

		}

		return $return_count ? $order_count : false;
	}

	/**
	 * Check if subscription status is in list of allowed shippable statuses ('active' || 'pending-cancellation')
	 *
	 * @return  bool
	 */
	function is_shippable_status( $force = false ) {
		return ( $force || in_array( $this->get_status(), $this->shippable_statuses ) );
	}

	/**
	 * Helper function for interacting with subscription meta data
	 *
	 * @param   string  $key
	 * @param   mixed   $value must be serializable if non-scalar
	 * @return  void
	 */
	function set_subscription_meta_data( $key, $value ) {
		update_post_meta( $this->get_id(), $key, $value );
	}

	/**
	 * Helper function for interacting with subscription meta data
	 *
	 * @param   string  $key
	 * @return  mixed   value
	 */
	function get_subscription_meta_data( $key ) {
		return get_post_meta( $this->get_id(), $key );
	}

}
