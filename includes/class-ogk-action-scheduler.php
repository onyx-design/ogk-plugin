<?php
/**
 * Scheduler for subscription events
 * Functions modified to allow for custom OGK shippable status logic
 *
 * @class     WCS_Action_Scheduler
 * @version   2.0.0
 * @package   WooCommerce Subscriptions/Classes
 * @category  Class
 * @author    Prospress
 */
class OGK_Action_Scheduler extends WCS_Action_Scheduler {

	public function update_date( $subscription, $date_type, $datetime ) {
		if ( in_array( $date_type, $this->get_date_types_to_schedule() ) ) {
			$action_hook = $this->get_scheduled_action_hook( $subscription, $date_type );

			if ( !empty( $action_hook ) ) {
				$action_args    = $this->get_action_args( $date_type, $subscription );
				$timestamp      = wcs_date_to_time( $datetime );
				$next_scheduled = as_next_scheduled_action( $action_hook, $action_args );

				if ( $next_scheduled !== $timestamp ) {
					// Maybe clear the existing schedule for this hook
					$this->unschedule_actions( $action_hook, $action_args );

					// Only reschedule if it's in the future
					if ( $timestamp > current_time( 'timestamp', true ) && ( 'payment_retry' == $date_type || $subscription->is_shippable_status() ) ) {
						as_schedule_single_action( $timestamp, $action_hook, $action_args );
					}
				}
			}
		}
	}

	/**
	 * When a subscription's status is updated, maybe schedule an event
	 *
	 * @param object $subscription An instance of a WC_Subscription object
	 * @param string $date_type Can be 'trial_end', 'next_payment', 'end', 'end_of_prepaid_term' or a custom date type
	 * @param string $datetime A MySQL formated date/time string in the GMT/UTC timezone.
	 */
	public function update_status( $subscription, $new_status, $old_status ) {

		switch ( $new_status ) {
			case 'active':

				$this->unschedule_actions( 'woocommerce_scheduled_subscription_end_of_prepaid_term', $this->get_action_args( 'end', $subscription ) );

				foreach ( $this->get_date_types_to_schedule() as $date_type ) {

					$action_hook = $this->get_scheduled_action_hook( $subscription, $date_type );

					if ( empty( $action_hook ) ) {
						continue;
					}

					$event_time = $subscription->get_time( $date_type );

					// If there's no payment retry date, avoid calling get_action_args() because it calls the resource intensive WC_Subscription::get_last_order() / get_related_orders()
					if ( 'payment_retry' === $date_type && 0 === $event_time ) {
						continue;
					}

					$action_args    = $this->get_action_args( $date_type, $subscription );
					$next_scheduled = as_next_scheduled_action( $action_hook, $action_args );

					// Maybe clear the existing schedule for this hook
					if ( false !== $next_scheduled && $next_scheduled != $event_time ) {
						$this->unschedule_actions( $action_hook, $action_args );
					}

					if ( 0 != $event_time && $event_time > current_time( 'timestamp', true ) && $next_scheduled != $event_time ) {
						as_schedule_single_action( $event_time, $action_hook, $action_args );
					}
				}

				break;
			case 'pending-cancel':
				// Now that we have the current times, clear the scheduled hooks
				foreach ( $this->get_date_types_to_schedule() as $date_type ) {
					$action_hook = $this->get_scheduled_action_hook( $subscription, $date_type );
					if ( empty( $action_hook ) ) {
						continue;
					}

					if ( $action_hook !== 'woocommerce_scheduled_subscription_shipment' ) {
						$this->unschedule_actions( $action_hook, $this->get_action_args( $date_type, $subscription ) );
					}
				}

				$end_time       = $subscription->get_time( 'end' ); // This will have been set to the correct date already
				$action_args    = $this->get_action_args( 'end', $subscription );
				$next_scheduled = as_next_scheduled_action( 'woocommerce_scheduled_subscription_end_of_prepaid_term', $action_args );

				if ( false !== $next_scheduled && $next_scheduled != $end_time ) {
					$this->unschedule_actions( 'woocommerce_scheduled_subscription_end_of_prepaid_term', $action_args );
				}

				// The end date was set in WC_Subscriptions::update_dates() to the appropriate value, so we can schedule our action for that time
				if ( $end_time > current_time( 'timestamp', true ) && $next_scheduled != $end_time ) {
					as_schedule_single_action( $end_time, 'woocommerce_scheduled_subscription_end_of_prepaid_term', $action_args );
				}
				break;
			case 'on-hold':
			case 'cancelled':
			case 'switched':
			case 'expired':
			case 'trash':
				foreach ( $this->get_date_types_to_schedule() as $date_type ) {

					$action_hook = $this->get_scheduled_action_hook( $subscription, $date_type );

					if ( empty( $action_hook ) ) {
						continue;
					}

					$this->unschedule_actions( $action_hook, $this->get_action_args( $date_type, $subscription ) );
				}
				$this->unschedule_actions( 'woocommerce_scheduled_subscription_expiration', $this->get_action_args( 'end', $subscription ) );
				$this->unschedule_actions( 'woocommerce_scheduled_subscription_end_of_prepaid_term', $this->get_action_args( 'end', $subscription ) );
				break;
		}
	}

}
