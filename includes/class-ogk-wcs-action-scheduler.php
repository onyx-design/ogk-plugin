<?php
/**
 * Scheduler for subscription events
 * Functions modified to allow for custom OGK shippable status logic
 *
 * @class     WCS_Action_Scheduler
 * @version   2.0.0
 * @package   WooCommerce Subscriptions/Classes
 * @category  Class
 * @author    Prospress
 */
class OGK_WCS_Action_Scheduler extends WCS_Action_Scheduler {

	protected static $ogk_recurring_actions = array(
		'ogk_send_pre_renewal_notifications'	=> HOUR_IN_SECONDS * 4
	);

	public static function check_scheduled_actions() {

		$recurring_actions = self::$ogk_recurring_actions;

		foreach( $recurring_actions as $hook => $interval ) {
			
			if( empty( as_next_scheduled_action( $hook ) ) ) {
				
				// If not scheduled, schedule for 5 minutes from now
				as_schedule_recurring_action( time() + 300, $interval, $hook );
			}
		}
	}


	public static function ogk_wcs_pre_renewal_notifications() {

		global $wpdb;

		$pre_renewal_sent_meta_key = '_ogk_wcs_pre_renewal_notification_sent';

		$eight_hours_from_now = date( 'Y-m-d H:i:s', strtotime( '+8 hours' ) );
		$seventy_two_hours_from_now = date( 'Y-m-d H:i:s', strtotime( '+72 hours' ) );
		$ninety_six_hours_ago = date( 'Y-m-d H:i:s', strtotime( '-96 hours' ) );

		$query = "
			SELECT sub.ID as 'subscription_id', next_date.meta_value as 'next_payment_date', notification_sent.meta_value as 'pre_renewal_notification_sent'
			FROM {$wpdb->posts} sub
			LEFT JOIN {$wpdb->postmeta} next_date
				ON next_date.post_id = sub.ID
				AND next_date.meta_key = '_schedule_next_payment'
			LEFT JOIN {$wpdb->postmeta} notification_sent
				ON notification_sent.post_id = sub.ID
				AND notification_sent.meta_key = '{$pre_renewal_sent_meta_key}'
			WHERE (
					sub.post_type = 'shop_subscription'
					AND
					sub.post_status = 'wc-active'
				)
				AND next_date.meta_value BETWEEN '{$eight_hours_from_now}' AND '{$seventy_two_hours_from_now}'
				AND (
					notification_sent.meta_value <= '{$ninety_six_hours_ago}'
					OR
					notification_sent.post_id IS NULL
				)
		";

		// onyx_debug( $query );

		$results = $wpdb->get_results( $query, OBJECT );

		// onyx_debug( $results );

		foreach( $results as $result ) {

			if( $subscription = wcs_get_subscription( $result->subscription_id ) ) {

				if( wcs_is_subscription( $subscription ) && 1 !== $subscription->get_billing_interval_months() ) {

					WC()->mailer()->emails['OGK_WCS_Email_Pre_Renewal']->trigger( $subscription->get_id(), $subscription );
					$subscription->add_order_note( 'Subscription pre-renewal notification sent.' );

					$subscription->update_meta_data( $pre_renewal_sent_meta_key, date( 'Y-m-d H:i:s' ) );
					$subscription->save();


				}

				
			}
		}


		// Query to get the subscriptions that we want to send pre-renewal notifications for


		// Maybe: logic to validate if we are going to send to each subscription


		// send emails
	}



	public function update_date( $subscription, $date_type, $datetime ) {
		if ( in_array( $date_type, $this->get_date_types_to_schedule() ) ) {
			$action_hook = $this->get_scheduled_action_hook( $subscription, $date_type );

			if ( !empty( $action_hook ) ) {
				$action_args    = $this->get_action_args( $date_type, $subscription );
				$timestamp      = wcs_date_to_time( $datetime );
				$next_scheduled = as_next_scheduled_action( $action_hook, $action_args );

				if ( $next_scheduled !== $timestamp ) {
					// Maybe clear the existing schedule for this hook
					$this->unschedule_actions( $action_hook, $action_args );

					// Only reschedule if it's in the future
					if ( $timestamp > current_time( 'timestamp', true ) && ( 'payment_retry' == $date_type || $subscription->is_shippable_status() ) ) {
						as_schedule_single_action( $timestamp, $action_hook, $action_args );
					}
				}
			}
		}
	}

	/**
	 * When a subscription's status is updated, maybe schedule an event
	 *
	 * @param object $subscription An instance of a WC_Subscription object
	 * @param string $date_type Can be 'trial_end', 'next_payment', 'end', 'end_of_prepaid_term' or a custom date type
	 * @param string $datetime A MySQL formated date/time string in the GMT/UTC timezone.
	 */
	public function update_status( $subscription, $new_status, $old_status ) {

		switch ( $new_status ) {
			case 'active':

				$this->unschedule_actions( 'woocommerce_scheduled_subscription_end_of_prepaid_term', $this->get_action_args( 'end', $subscription ) );

				foreach ( $this->get_date_types_to_schedule() as $date_type ) {

					$action_hook = $this->get_scheduled_action_hook( $subscription, $date_type );

					if ( empty( $action_hook ) ) {
						continue;
					}

					$event_time = $subscription->get_time( $date_type );

					// If there's no payment retry date, avoid calling get_action_args() because it calls the resource intensive WC_Subscription::get_last_order() / get_related_orders()
					if ( 'payment_retry' === $date_type && 0 === $event_time ) {
						continue;
					}

					$action_args    = $this->get_action_args( $date_type, $subscription );
					$next_scheduled = as_next_scheduled_action( $action_hook, $action_args );

					// Maybe clear the existing schedule for this hook
					if ( false !== $next_scheduled && $next_scheduled != $event_time ) {
						$this->unschedule_actions( $action_hook, $action_args );
					}

					if ( 0 != $event_time && $event_time > current_time( 'timestamp', true ) && $next_scheduled != $event_time ) {
						as_schedule_single_action( $event_time, $action_hook, $action_args );
					}
				}

				break;
			case 'pending-cancel':
				// Now that we have the current times, clear the scheduled hooks
				foreach ( $this->get_date_types_to_schedule() as $date_type ) {
					$action_hook = $this->get_scheduled_action_hook( $subscription, $date_type );
					if ( empty( $action_hook ) ) {
						continue;
					}

					if ( $action_hook !== 'woocommerce_scheduled_subscription_shipment' ) {
						$this->unschedule_actions( $action_hook, $this->get_action_args( $date_type, $subscription ) );
					}
				}

				$end_time       = $subscription->get_time( 'end' ); // This will have been set to the correct date already
				$action_args    = $this->get_action_args( 'end', $subscription );
				$next_scheduled = as_next_scheduled_action( 'woocommerce_scheduled_subscription_end_of_prepaid_term', $action_args );

				if ( false !== $next_scheduled && $next_scheduled != $end_time ) {
					$this->unschedule_actions( 'woocommerce_scheduled_subscription_end_of_prepaid_term', $action_args );
				}

				// The end date was set in WC_Subscriptions::update_dates() to the appropriate value, so we can schedule our action for that time
				if ( $end_time > current_time( 'timestamp', true ) && $next_scheduled != $end_time ) {
					as_schedule_single_action( $end_time, 'woocommerce_scheduled_subscription_end_of_prepaid_term', $action_args );
				}
				break;
			case 'on-hold':
			case 'cancelled':
			case 'switched':
			case 'expired':
			case 'trash':
				foreach ( $this->get_date_types_to_schedule() as $date_type ) {

					$action_hook = $this->get_scheduled_action_hook( $subscription, $date_type );

					if ( empty( $action_hook ) ) {
						continue;
					}

					$this->unschedule_actions( $action_hook, $this->get_action_args( $date_type, $subscription ) );
				}
				$this->unschedule_actions( 'woocommerce_scheduled_subscription_expiration', $this->get_action_args( 'end', $subscription ) );
				$this->unschedule_actions( 'woocommerce_scheduled_subscription_end_of_prepaid_term', $this->get_action_args( 'end', $subscription ) );
				break;
		}
	}

}
