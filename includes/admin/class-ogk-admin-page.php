<?php
defined( 'ABSPATH' ) || exit;
/**
 * OGK_Admin_Page controls the pages displayed on WP Admin
 */
class OGK_Admin_Page {
	/**
	 * Add actions and filters
	 */
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'add_ogk_admin_pages' ) );
	}

	/**
	 * Add OGK Specific Pages to WP_Admin Menu
	 */
	public function add_ogk_admin_pages() {
		global $current_user;
		get_currentuserinfo();

		if ( 'dev@onyxdesign.net' == $current_user->user_email ) {
			add_menu_page( 'OGK Sandbox', 'OGK Sandbox', 'manage_options', 'ogk-admin-sandbox', array( $this, 'ogk_admin_page_sandbox' ), 'dashicons-admin-tools' );
		}
	}

	/**
	 * Include Sandbox Template File
	 */
	public function ogk_admin_page_sandbox() {
		include OGK_TEMPLATE_PATH . '/admin/admin-page-ogk-sandbox.php';
	}
}
return new OGK_Admin_Page();
