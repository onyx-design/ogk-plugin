<?php
defined( 'ABSPATH' ) || exit;

/**
 * OGK_Hooks is responsible for hooking into and modifying actions and filters
 */
class OGK_Hooks {

	public static function init() {
		// Override WC Classes with OGK Classes
		add_filter( 'woocommerce_order_class', __CLASS__ . '::ogk_wc_subscription_class', 100, 2 );
		add_filter( 'woocommerce_subscriptions_scheduler', __CLASS__ . '::ogk_wcs_action_scheduler_class', 100, 1 );

		// Frontend Hooks
		add_filter( 'wcs_update_all_subscriptions_addresses_checked', __CLASS__ . '::ogk_update_all_addresses_checked' );
		add_filter( 'wcs_view_subscription_actions', __CLASS__ . '::add_edit_address_subscription_action_pending_cancel', 10, 2 );

		// WP Admin Hooks
		add_filter( 'woocommerce_order_actions', __CLASS__ . '::ogk_add_action_send_customer_order_completed_email', 10, 1 );
		add_action( 'manage_shop_order_posts_custom_column', __CLASS__ . '::ogk_contains_subscription_column_content_shipment_orders', 5, 1 );
		add_filter( 'add_meta_boxes', __CLASS__ . '::ogk_shipment_order_related_orders_meta_box', 40 );
		add_filter( 'woocommerce_order_actions', __CLASS__ . '::ogk_add_subscription_actions', 10, 1 );

		// Email Hooks
		add_filter( 'woocommerce_email_order_meta_fields', __CLASS__ . '::ogk_add_customer_email_fields', 10, 3 );
		add_action( 'woocommerce_order_action_ogk_send_customer_order_completed_email', __CLASS__ . '::ogk_send_customer_order_completed_email', 10, 1 );
		add_filter( 'woocommerce_order_status_pending_to_processing', __CLASS__ . '::ogk_wcs_shipment_suppress_status_processing_email', 5, 2 );

		// Action Scheduler Hooks
		add_action( 'woocommerce_subscriptions_scheduled_action_hook', __CLASS__ . '::ogk_wcs_add_scheduled_shipment_action_hook', 10, 2 );
		add_action( 'ogk_send_pre_renewal_notifications', 'OGK_WCS_Action_Scheduler::ogk_wcs_pre_renewal_notifications' );

		// Shipment Order Hooks
		add_action( 'woocommerce_scheduled_subscription_shipment', __CLASS__ . '::ogk_generate_scheduled_shipment', 10, 1 );
		add_action( 'woocommerce_scheduled_subscription_payment', __CLASS__ . '::ogk_validate_scheduled_payment', -10, 1 );
		add_filter( 'woocommerce_subscription_calculated_next_shipment_date', __CLASS__ . '::ogk_calculate_next_shipment_date', 10, 2 );
		add_filter( 'woocommerce_subscription_calculated_next_payment_date', __CLASS__ . '::ogk_calculate_next_payment_date', 10, 2 );
		add_action( 'woocommerce_subscription_payment_complete', __CLASS__ . '::ogk_update_next_shipment_date_after_payment', 100 );
		add_filter( 'woocommerce_subscriptions_admin_related_orders_to_display', __CLASS__ . '::ogk_woocommerce_subscriptions_admin_related_orders_to_display', 100, 3 );
		add_filter( 'woocommerce_subscription_get_last_shipment_order_date', __CLASS__ . '::ogk_wcs_subscription_get_last_shipment_order_date', 10, 3 );
		add_action( 'woocommerce_order_action_ogk_wcs_create_shipment', __CLASS__ . '::ogk_wcs_create_shipment_action_request', 10, 1 );
		add_filter( 'wcs_shipment_order_items', __CLASS__ . '::ogk_shipment_order_items', 100, 3 );
		add_filter( 'wcs_new_order_types', __CLASS__ . '::ogk_wcs_add_shipment_order_type', 100, 1 );
		add_filter( 'wcs_additional_related_order_relation_types', __CLASS__ . '::ogk_wcs_add_order_relation_type', 100, 1 );
		add_filter( 'wcs_new_order_title', __CLASS__ . '::ogc_wcs_shipment_order_title', 100, 3 );
		
		add_filter( 'woocommerce_subscription_dates', __CLASS__ . '::ogk_woocommerce_subscription_dates', 100 );
		add_filter( 'woocommerce_subscription_can_date_be_updated', __CLASS__ . '::ogk_wcs_can_next_shipment_date_be_updated', 100, 3 );
		add_action( 'woocommerce_subscription_date_updated', __CLASS__ . '::ogk_wcs_update_next_shipment_or_payment_add_note', 999, 3 );

		// add_action( 'woocommerce_subscriptions_product_first_renewal_payment_time', __CLASS__ . '::products_first_renewal_payment_time', 10, 4 ); //dev:dates
		add_filter( 'wcs_new_order_created', __CLASS__ . '::ogk_wcs_new_order_created', 100, 3 );
		add_action( 'subscriptions_created_for_order', __CLASS__ . '::ogk_add_fulfillment_month_to_order', 10, 1 );
		add_filter( 'wc_shipment_tracking_get_providers', __CLASS__ . '::ogk_shipment_tracking_add_intl_links', 10, 1 );

		// Shipstation Filters
		add_filter( 'woocommerce_shipstation_export_custom_field_2', __CLASS__ . "::ogk_shipstation_custom_field_2", 100, 1 );
		add_filter( 'woocommerce_shipstation_export_custom_field_2_value', __CLASS__ . "::ogk_shipstation_custom_field_2_value", 100 , 2 );
		add_filter( 'woocommerce_shipstation_export_custom_field_3', __CLASS__ . "::ogk_shipstation_custom_field_3", 100, 1 );
		add_filter( 'woocommerce_shipstation_export_custom_field_3_value', __CLASS__ . "::ogk_shipstation_custom_field_3_value", 100, 2 );

		add_action( 'woocommerce_admin_order_data_after_shipping_address', __CLASS__ . '::ogk_wc_admin_order_tracking' );
		
	}

//

// OVERRIDE CLASSES

//

	/**
	 * Override WC_Subscription class with OGK_WC_Subscription
	 *
	 * @param   CLASS $classname the class which invokes the filter
	 * @param   array $order_type Details about the order type
	 * @return  CLASS the possibly modified class invoked
	 */
	public static function ogk_wc_subscription_class( $classname, $order_type ) {
		if ( false !== strpos( $classname, 'WC_Subscription' ) && 'shop_subscription' == $order_type ) {
			$classname = 'OGK_WC_Subscription';
		}

		return $classname;
	}

	/**
	 * Override WCS_Action_Scheduler class with OGK_Action_Scheduler
	 *
	 * @param   CLASS $classname the class which invokes the filter
	 * @return  CLASS the possibly modified class invoked
	 */
	public static function ogk_wcs_action_scheduler_class( $classname ) {
		if ( false !== strpos( $classname, 'WCS_Action_Scheduler' ) ) {
			$classname = 'OGK_WCS_Action_Scheduler';
		}

		return $classname;
	}

//

// FRONTEND

//

	/**
	 * Automatically default the "update address used for active subscription"
	 * checkbox to true
	 * @return  bool  true
	 */
	public static function ogk_update_all_addresses_checked() {
		return true;
	}

	/**
	 * Add a "Change Shipping Address" button to the "My Subscriptions" table for subscriptions pending-cancellation
	 *
	 * @param array $all_actions The $subscription_id => $actions array with all actions that will be displayed for a subscription on the "My Subscriptions" table
	 * @param array $subscriptions All of a given users subscriptions that will be displayed on the "My Subscriptions" table
	 * @since 1.3
	 */
	public static function add_edit_address_subscription_action_pending_cancel( $actions, $subscription ) {
		if ( $subscription->needs_shipping_address() && $subscription->has_status( array( 'pending-cancel' ) ) ) {
			$actions['change_address'] = array(
				'url'  => add_query_arg( array( 'subscription' => $subscription->get_id() ), wc_get_endpoint_url( 'edit-address', 'shipping' ) ),
				'name' => __( 'Change address', 'woocommerce-subscriptions' ),
			);
		}
		return $actions;
	}

//

// WP ADMIN

//

	/**
	 * Add 'Resend Order Completed Email' action to WP_Admin actions list
	 * @param   array $actions Order Actions
	 * @return  array Order Actions
	 */
	public static function ogk_add_action_send_customer_order_completed_email( $actions ) {
		global $theorder;
		if ( wcs_is_order( $theorder ) ) {
			if ( 'completed' == $theorder->get_status() ) {
				$action_label = wcs_order_contains_renewal( $theorder )
				? 'Resend Custom Renewal Order Completed Email'
				: 'Resend Custom Order Completed Email';
				$actions['ogk_send_customer_order_completed_email'] = $action_label;
			}

		}

		return $actions;
	}

	/**
	 * Adds 'SS' to order list items of type shipment
	 *
	 * @param   string $column of the table in question
	 * @return  void
	 */
	public static function ogk_contains_subscription_column_content_shipment_orders( $column ) {
		global $post;

		if ( 'subscription_relationship' == $column ) {
			if ( WCS_Related_Order_Store::instance()->get_related_subscription_ids( wc_get_order( $post->ID ), 'shipment' ) ) {
				echo '<span class="subscription_shipment_order tips" data-tip="' . esc_attr__( 'Shipment Order', 'woocommerce-subscriptions' ) . '">SS</span>';

				onyx_remove_action_once( 'manage_shop_order_posts_custom_column', 'WC_Subscriptions_Order::add_contains_subscription_column_content', 10 );
			}

		}

	}

	/**
	 * Meta box on WC Admin - ???
	 *
	 * DEV:COMMENT What specifically does this function do?
	 * @return  void
	 */
	public static function ogk_shipment_order_related_orders_meta_box() {
		global $post_ID;

		if ( 'shop_order' === get_post_type( $post_ID ) ) {
			$order                    = wc_get_order( $post_ID );
			$related_subscription_ids = WCS_Related_Order_Store::instance()->get_related_subscription_ids( $order, 'shipment' );
			if ( $related_subscription_ids ) {
				add_meta_box( 'subscription_renewal_orders', __( 'Related Orders', 'woocommerce-subscriptions' ), 'WCS_Meta_Box_Related_Orders::output', 'shop_order', 'normal', 'low' );
				add_filter( 'woocommerce_got_subscriptions', function ( $subscriptions, $args ) use ( $order, $related_subscription_ids ) {
					if ( $args['order_id'] == $order->get_id() ) {
						foreach ( $related_subscription_ids as $related_subscription_id ) {
							$subscriptions[$related_subscription_id] = wcs_get_subscription( $related_subscription_id );
						}

					}

					return $subscriptions;
				}, 100, 2 );
			}

		}

	}

	/**
	 * Add 'Create Shipment Order' to list of WP_Admin Subscription Actions
	 *
	 * @param   array $actions available to the user
	 * @return  array updated list of actions available
	 */
	public static function ogk_add_subscription_actions( $actions ) {
		global $theorder;
		if ( wcs_is_subscription( $theorder ) ) {
			if ( !$theorder->has_status( wcs_get_subscription_ended_statuses() ) ) {
				$actions['ogk_wcs_create_shipment'] = esc_html__( 'Create shipment order', 'woocommerce-subscriptions' );
			}

		}

		return $actions;
	}

//

// EMAILS

//

	/**
	 * Add order meta to email templates.
	 *
	 * @param   array    $fields associative array of fields to include
	 * @param   bool     $sent_to_admin If should sent to admin.
	 * @param   WC_Order $order         Order instance.
	 * @return  array    $fields updated array of fields to include
	 */
	public static function ogk_add_customer_email_fields( $fields, $sent_to_admin, $order ) {
		if ( $order ) {
			$fields['billing_first_name'] = $order->get_billing_first_name();
		}

		return $fields;
	}

	/**
	 * Manually send an order completed email
	 *
	 * @param   WC_Order $order the order
	 * @return  void
	 */
	public static function ogk_send_customer_order_completed_email( $order ) {

		if ( wcs_is_order( $order ) && 'completed' == $order->get_status() ) {
			$wc_emails_class = wcs_order_contains_renewal( $order )
			? 'WCS_Email_Completed_Renewal_Order'
			: 'WC_Email_Customer_Completed_Order';

			$wc_email = WC()->mailer()->emails[$wc_emails_class] ?? false;

			if ( $wc_email ) {
				$wc_email->trigger( $order->get_id(), $order );
				$order->add_order_note( 'Manually sent ' . $wc_email->get_title() . ' email', 0, true );
			}

		}

	}

	/**
	 * Suppresses email if order is of type shipment
	 *
	 * @param   int $order_id
	 * @param   WC_Order $order
	 * @return  void
	 */
	public static function ogk_wcs_shipment_suppress_status_processing_email( $order_id, $order ) {
		if ( wcs_is_order( $order ) && WCS_Related_Order_Store::instance()->get_related_subscription_ids( $order, 'shipment' ) ) {
			WC_Subscriptions_Email::detach_woocommerce_transactional_email( 'woocommerce_order_status_pending_to_processing' );
		}
	}

//

// ACTION SCHEDULER

//

	/**
	 * Add woocommerce_scheduled_subscription_shipment to list of hooks to use in the action scheduler for the date type
	 *
	 * @param   string $hook name of the hook to run or empty if hook to run has not yet been found
	 * @param   string $date_type can be 'trial_end', 'next_payment', 'expiration', 'end_of_prepaid_term' or 'next_shipment' (or other custom)
	 * @return  string name of hook to run
	 */
	public static function ogk_wcs_add_scheduled_shipment_action_hook( $hook, $date_type ) {
		if ( 'next_shipment' == $date_type ) {
			$hook = 'woocommerce_scheduled_subscription_shipment';
		}

		return $hook;
	}

//

// SHIPMENT ORDERS

//

	/**
	 * Generate Shipment Order for a given subscription
	 * DEV:TODO There are a few places in the code where this code is repeated - should be consolidated to one function and reused
	 *
	 * @param   int $subscription_id
	 * @return  void
	 */
	public static function ogk_generate_scheduled_shipment( $subscription_id ) {
		$subscription = wcs_get_subscription( $subscription_id );

		if ( empty( $subscription ) ) {
			throw new Exception( 'Subscription ' . $subscription_id . ' not found.' );
		}

		// Init constant for detection elsewhere (adding order note)
		if( !defined( 'OGK_ACTION_SCHEDULER_SCHEDULED_SHIPMENT') ) {
			define('OGK_ACTION_SCHEDULER_SCHEDULED_SHIPMENT', true );
		}

		if ( $subscription ) {
			if ( $subscription->is_shippable_status() && !$subscription->had_shipment_this_month() ) {
				
				// Create a shipment order
				$new_shipment_order = $subscription->create_shipment_order();

			} else {
				$subscription->add_order_note( "Create scheduled shipment FAILED. Subscription ID: {$subscription->get_id()}, Sub. Status: {$subscription->get_status()}" );
				// throw new Exception( "Subscription status " . $subscription->get_status() . "not in list of shippable." );
			}

			// Update next shipment date
			$next_shipment_date = $subscription->calculate_date( 'next_shipment' ); //dev:dates

			$subscription->update_dates( array( 'next_shipment' => $next_shipment_date ) );
		}

	}

	/**
	 * Validate scheduled payment (prevent multiple shipments in a given fulfillment month)
	 * 
	 * @param int $subscription_id
	 * 
	 * @return void
	 */
	public static function ogk_validate_scheduled_payment( $subscription_id ) {

		$subscription = wcs_get_subscription( $subscription_id );

		// Verify that we won't be sending two orders in the same month
		$last_shipment_order_date_sitetz    	= $subscription->get_date( 'last_shipment_order', 'site' );

		if( 0 == OGK_Subscription_Dates::fulfillment_months_date_diff( "now", $last_shipment_order_date_sitetz ) ) {
			$last_shipment_order_date_gmt    	= $subscription->get_date( 'last_shipment_order', 'gmt' );

			$rescheduled_next_payment_date 		= OGK_Subscription_Dates::date_modify_subscription_month( $last_shipment_order_date_gmt, 1, 'Y-m-d H:i:s', 'gmt' );

			

			$subscription->update_dates( array( 'next_payment' => $rescheduled_next_payment_date ) );

			$subscription_note = "Prevented scheduled renewal order in same month as last shipment. Next Payment Date rescheduled to {$rescheduled_next_payment_date} (Timezone GMT)";
			$subscription->add_order_note( $subscription_note, 0 );

			// Remove additional action hook handlers to prevent renewal order from being created
			remove_action( 'woocommerce_scheduled_subscription_payment', 'WC_Subscriptions_Manager::maybe_process_failed_renewal_for_repair', 0 );
			remove_action( 'woocommerce_scheduled_subscription_payment', 'WC_Subscriptions_Manager::prepare_renewal', 1 );
			remove_action( 'woocommerce_scheduled_subscription_payment', 'WC_Subscriptions_Payment_Gateways::gateway_scheduled_subscription_payment', 10 );
		}
	}

	/**
	 * Calculate next shipment date for the subscription in GMT/UTC.
	 * DEV:TODO Move the date calculation logic out of OGK_Hooks for cleanliness?
	 *
	 * @param   mixed $date
	 * @param   WC_Subscription $subscription
	 * @return  DateTime|int
	 */
	public static function ogk_calculate_next_shipment_date( $date, $subscription ) { //dev:dates

		$date = 0;

		// Determine if this is a 1 month renewal
		$monthly_renewal = ( '1' == $subscription->get_billing_interval() && 'month' == $subscription->get_billing_period() );

		if ( $subscription->is_shippable_status() && !$monthly_renewal ) {

			$timezone = 'gmt';

			// $site_timezone 					= get_option( 'timezone_string' );
			$last_shipment_order_date    	= $subscription->get_date( 'last_shipment_order', $timezone );
			$next_shipment_date 			= OGK_Subscription_Dates::date_modify_subscription_month( $last_shipment_order_date, 1, 'Y-m-d H:i:s', $timezone );

			$end_of_prepaid_term            = $subscription->get_end_or_next_payment_date( $timezone );

			$date_diff_next_shipment__end_of_prepaid_term = OGK_Subscription_Dates::fulfillment_months_date_diff( $next_shipment_date, $end_of_prepaid_term, $timezone );
			$date_diff_next_shipment__last_shipment = OGK_Subscription_Dates::fulfillment_months_date_diff( $last_shipment_order_date, $next_shipment_date, $timezone );

			if( $date_diff_next_shipment__end_of_prepaid_term > 0 && $date_diff_next_shipment__last_shipment > 0 ) {
				// Date is only valid if it is not in the same month as the end
				$date = $next_shipment_date;
			}
		}

		return $date;
	}

	/**
	 * Calculate next payment date for the subscription in GMT/UTC.
	 * DEV:TODO Move the date calculation logic out of OGK_Hooks for cleanliness?
	 *
	 * @param   mixed $date
	 * @param   WC_Subscription $subscription
	 * @return  DateTime|int
	 */
	public static function ogk_calculate_next_payment_date( $date, $subscription ) { //dev:dates

		// $date = 0;
		if( !empty( $date ) ) {

			$last_payment_date = max( $subscription->get_date( 'last_order_date_created' ), $subscription->get_date( 'last_order_date_paid' ) );
			$billing_interval_months = $subscription->get_billing_interval_months();

			$next_payment_date = OGK_Subscription_Dates::date_modify_subscription_month( $last_payment_date, $billing_interval_months, 'object', 'gmt', true );

			// Verify that the next_payment_date is not in the same month as the last payment or (last/next) shipment
			$next_payment_date_sitetz = clone( $next_payment_date );
			$next_payment_date_sitetz->setTimezone( OGK_Subscription_Dates::init_timezone( 'site' ) );

			$max_date_to_check = max( $subscription->get_date( 'last_shipment_order', 'site' ), $subscription->get_date( 'next_shipment', 'site' ) );

			if( 0 >= OGK_Subscription_Dates::fulfillment_months_date_diff( $max_date_to_check, $next_payment_date_sitetz ) ) {
				$next_payment_date = OGK_Subscription_Dates::date_modify_subscription_month( $max_date_to_check, 1, 'object', 'site' );
				
			}
			else if( $billing_interval_months > 1 ) {
				// If calculated next_payment was not in the same month as last payment or (last/next) shipment,
				// try to align the next payment date to the most recent shipment
				$max_date_to_check = OGK_Subscription_Dates::init_datetime( $max_date_to_check, 'site' );
				$max_date_to_check_month_day_index = intval( $max_date_to_check->format( 'j' ) );

				$next_payment_date_sitetz_month_day_index = intval( $next_payment_date_sitetz->format( 'j' ) );

				$month_day_index_diff = abs( ( $next_payment_date_sitetz_month_day_index - $max_date_to_check_month_day_index ) );


				if( $month_day_index_diff > 5 ) {

					$next_payment_date = OGK_Subscription_Dates::date_modify_subscription_month( $next_payment_date_sitetz, 0, 'object', 'site', true, $max_date_to_check_month_day_index );
				}
			}

			$next_payment_date->setTimezone( OGK_Subscription_Dates::init_timezone( 'UTC' ) );

			// Verify that next_payment date is not greater than 1 day before end_date
			$end_time = $subscription->get_time( 'end' );
			if( 0 != $end_time && ( $next_payment_date->getTimestamp() + 23 * HOUR_IN_SECONDS ) > $end_time ) {
				$next_payment_date = 0;
			}
			else {
				$next_payment_date = $next_payment_date->format( 'Y-m-d H:i:s' );
			}

			$date = $next_payment_date;
		}

		return $date;
	}

	/**
	 * Update next shipment dates for a subscription when a payment is successfully completed
	 *
	 * @param   WC_Subscription
	 * @return  void
	 */
	public static function ogk_update_next_shipment_date_after_payment( $subscription ) { //dev:dates

		if ( !wcs_is_subscription( $subscription ) ) {
			return false;
		}

		if( !defined( 'OGK_UPDATE_SHIPMENT_DATE_AFTER_PAYMENT' ) ) {
			define( 'OGK_UPDATE_SHIPMENT_DATE_AFTER_PAYMENT', true );
		}

		$next_shipment_date_gmt = $subscription->calculate_date( 'next_shipment' );

		if ( !empty( $next_shipment_date_gmt ) ) {
			$subscription->update_dates( array( 'next_shipment' => $next_shipment_date_gmt ) );
		}

	}

	/**
	 * Add Shipment orders to list shown in Related Orders meta box on WC_Admin Subscription Page
	 *
	 * @param   array $orders_to_display list of related orders
	 * @param   WC_Subscription $subscription
	 * @param   WP_Post $post the post object
	 * @return  array updated list of related orders to display
	 */
	public static function ogk_woocommerce_subscriptions_admin_related_orders_to_display( $orders_to_display, $subscriptions, $post ) {

		if ( wcs_is_subscription( $post->ID ) ) {
			$this_subscription = wcs_get_subscription( $post->ID );

			$shipment_orders = $this_subscription->get_related_orders( 'ids', 'shipment' );

			foreach ( $shipment_orders as $shipment_order_id ) {
				$order = wc_get_order( $shipment_order_id );
				$order->update_meta_data( '_relationship', 'Shipment Order' );
				$orders_to_display[] = $order;
			}

			// Sort the orders by ID
			usort( $orders_to_display, function ( $order_a, $order_b ) {
				return $order_a->get_id() - $order_b->get_id();
			} );
		} elseif ( wcs_is_order( $post->ID ) ) {
			$order            = wc_get_order( $post->ID );
			$subscription_ids = WCS_Related_Order_Store::instance()->get_related_subscription_ids( $order, 'shipment' );

			if ( !empty( $subscription_ids ) ) {

				foreach ( $subscription_ids as $subscription_id ) {
					$this_subscription = wcs_get_subscription( $subscription_id );
					$shipment_orders   = $this_subscription->get_related_orders( 'ids', 'shipment' );

					foreach ( $shipment_orders as $shipment_order_id ) {
						$order = wc_get_order( $shipment_order_id );
						$order->update_meta_data( '_relationship', 'Shipment Order' );
						$orders_to_display[] = $order;
					}

				}

				// Sort the orders by ID
				usort( $orders_to_display, function ( $order_a, $order_b ) {
					return $order_a->get_id() - $order_b->get_id();
				} );
			}

		}

		return $orders_to_display;
	}

	/**
	 * Get date of last shipment order
	 *
	 * @param   WC_DateTime|null $date of subscriptions schedule if found
	 * @param   WC_Subscription $subscription
	 * @param   string $timezone The timezone of the $datetime param, either 'gmt' or 'site'. Default 'gmt'.
	 * @return	WC_DateTime|null date of last shipment order
	 */
	public static function ogk_wcs_subscription_get_last_shipment_order_date( $date, $subscription, $timezone ) {

		if ( wcs_is_subscription( $subscription ) ) {
			$last_shipment_order = $subscription->get_last_order( 'all', array( 'any', 'shipment' ) );

			if ( wcs_is_order( $last_shipment_order ) ) {
				$date = $last_shipment_order->get_date_created();

				if ( is_a( $date, 'DateTime' ) ) {
					// Don't change the original date object's timezone as this may affect the prop stored on the subscription
					$date = clone $date;

// WC's return values use site timezone by default
					if ( 'gmt' === strtolower( $timezone ) ) {
						$date->setTimezone( new DateTimeZone( 'UTC' ) );
					}

					$date = $date->date( 'Y-m-d H:i:s' );
				}

			}

		}

		return $date;
	}

	/**
	 * Create new Subscription Shipment Order from a given subscription
	 *
	 * @param   WC_Subscription $subscription
	 * @return  WC_Order|WP_Error the created order
	 */
	public static function ogk_wcs_create_shipment_action_request( $subscription ) {

		if ( !wcs_is_subscription( $subscription ) ) {
			return new WP_Error( 'shipment-order-invalid-subscription', 'Failed to create shipment order. Invalid subscription argument.' );
		}

		$shipment_order = $subscription->create_shipment_order();

		if ( is_wp_error( $shipment_order ) ) {
			do_action( 'wcs_failed_to_create_shipment_order', $shipment_order, $subscription );

			return new WP_Error( 'shipment-order-error', $shipment_order->get_error_message() );
		}

		$subscription->add_order_note( 'New shipment order created manually: ' . $shipment_order->get_id(), 0, true );

		return apply_filters( 'wcs_shipment_order_created', $shipment_order, $subscription );
	}

	/**
	 * Function to modify shipment order items
	 *
	 * @param   array $items
	 * @param   WC_Order $new_order
	 * @param   WC_Subscription $subscription
	 * @return  array of modified order items
	 */
	public static function ogk_shipment_order_items( $items, $new_order, $subscription ) {
		$shipment_order_items = array();

// DEV:TODO Improve this pricing functionality
		foreach ( $items as $item ) {

			if ( !in_array( $item->get_type(), array( 'line_item', 'shipping' ) ) ) {
				continue;
			}

			if ( 'line_item' == $item->get_type() ) {
				$item->set_subtotal( 0 );
			}

			// This currently sets as discount - needs to change cost of order items
			$item->set_total( 0 );
			$shipment_order_items[] = $item;
		}

		return $shipment_order_items;
	}

	/**
	 * Add Shipment Order to allowed Woocommerce Subscription Order Types
	 *
	 * @param   array $wcs_order_types types of wcs order
	 * @return  array updated array of wcs order types
	 */
	public static function ogk_wcs_add_shipment_order_type( $wcs_new_order_types ) {
		array_push( $wcs_new_order_types, 'shipment_order' );

		return $wcs_new_order_types;
	}

	/**
	 * Add Shipment Order Relation Type
	 *
	 * @param   array $relation_types order relation types
	 * @return  array updated order relation types array
	 */
	public static function ogk_wcs_add_order_relation_type( $relation_types ) {
		array_push( $relation_types, 'shipment' );

		return $relation_types;
	}

	/**
	 * Modify created post title to allow for shipment_order type
	 *
	 * @param   string $title Title for a post
	 * @param   string $type type of new order
	 * @param   string $order_date date of order as a string
	 * @return  string Updated title for a post
	 */
	public static function ogc_wcs_shipment_order_title( $title, $type, $order_date ) {

		if ( 'shipment_order' == $type ) {
			$title = sprintf( __( 'Shipment Order &ndash; %s', 'woocommerce-subscriptions' ), $order_date );
		}

		return $title;
	}

	/**
	 * Modify array of subscription dates to include next shipment
	 *
	 * @param   array $dates
	 * @return  array of dates including next_shipment
	 */
	public static function ogk_woocommerce_subscription_dates( $dates ) {

		if ( !isset( $dates['next_shipment'] ) ) {
			$dates['next_shipment'] = 'Next Shipment';
		}

		return $dates;
	}

	/**
	 * Adjust can_date_be_updated to account for next_shipment
	 *
	 * @param   boolean $can_date_be_updated
	 * @param   string $date_type | 'date_created', 'trial_end', 'next_payment', 'last_order_date_created','end', 'next_shipment'
	 * @param   WC_Subscription $subscription
	 * @return  boolean true if date can be updated
	 */
	public static function ogk_wcs_can_next_shipment_date_be_updated( $can_date_be_updated, $date_type, $subscription ) {

		if ( 'next_shipment' == $date_type ) {
			$can_date_be_updated = true;
		}

		return $can_date_be_updated;
	}

	/**
	 * Add a note whenever subscription next_shipment or next_payment date is updated
	 */
	
	/**
	 * Undocumented function
	 *
	 * @param WC_Subscription $subscription
	 * @param string $date_type	date type e.g. 'next_payment' or 'next_shipment'
	 * @param string $datetime	datetime string in gmt timezone
	 * @return void
	 */
	public static function ogk_wcs_update_next_shipment_or_payment_add_note( $subscription, $date_type, $datetime ) {

		if( ( 'next_shipment' == $date_type || 'next_payment' == $date_type ) && $subscription instanceof WC_Subscription ) {

			$datetime_obj = new DateTime( $datetime, new DateTimeZone( 'UTC' ) );
			$local_timezone_str = get_option( 'timezone_string' );
			$datetime_local = $datetime_obj->setTimezone( new DateTimeZone( $local_timezone_str ) )->format( 'Y-m-d H:i:s' ); 

			$subscription_note = 
				empty( $datetime ) 
				? "{$date_type} removed"
				: "{$date_type} date updated to {$datetime_local} ({$local_timezone_str})";
			
			$note_added_by_user = false;

			// NOTE: Multiple conditions below can theoretically be true. They are not "else ifs" intentionally.

			// If during/by ONYX OP
			if( defined( 'ONYX_RUNNING_OP' ) && ONYX_RUNNING_OP && !empty( ONYX_Operation_Base::$request_op ) ) {
				$current_op = ONYX_Operation_Base::$request_op;

				$subscription_note .= " during ONYX Operation {$current_op->ID}, by user ID {$current_op->op_data['user_id']}";
				
			}
			
			// If shipment date updated after payment
			if( defined( 'OGK_UPDATE_SHIPMENT_DATE_AFTER_PAYMENT' ) ) {
				
				$subscription_note .= " - after order payment";
			}

			// If during action scheduler hook
			if( defined( 'OGK_ACTION_SCHEDULER_SCHEDULED_SHIPMENT' ) && OGK_ACTION_SCHEDULER_SCHEDULED_SHIPMENT ) {

				$subscription_note .= " - during Scheduled Action";
			}
			
			if( is_admin() ) {

				$subscription_note .= ' - from WP Admin';
				$note_added_by_user = true;
			}

			
			$subscription->add_order_note( $subscription_note, 0, $note_added_by_user );
		}
	}

	/**
	 * Adjust renewal payment of product to sync with 4th of next renewal payment month
	 * REMOVED: no longer realigning subscription dates //dev:dates
	 *
	 * @param   int $first_renewal_timestamp Unix timestamp respresentation of first renewal
	 * @param   int|WC_Product $product
	 * @param   mixed $from_date A MySQL formatted date/time string
	 * @param   string $timezone The timezone for the returned date
	 * @return  int the updated timestamp
	 */
	// public static function products_first_renewal_payment_time( $first_renewal_timestamp, $product_id, $from_date, $timezone ) {
	// 	return OGK_Subscription_Dates::get_next_renewal_timestamp( $product_id, $first_renewal_timestamp, $from_date );
	// }

	/**
	 * Set fulfillment month and recalculate totals when a new order is generated
	 *
	 * @param   WC_Order $new_order order created from the subscription
	 * @param   WC_Subscription $subscription from which the order is generated
	 * @param   string $type of order. Default values are 'renewal_order'|'resubscribe_order'
	 * @return  WC_Order|WC_Error New order or error object.
	 */
	public static function ogk_wcs_new_order_created( $new_order, $subscription, $type ) {
		$fulfillment_month = OGK_Subscription_Dates::get_fulfillment_month();
		$new_order->update_meta_data( "_fulfillment_month", $fulfillment_month );

		if ( 'shipment_order' == $type ) {
			$new_order->calculate_totals();
		}

		if ( 'shipment_order' == $type || 'renewal_order' == $type ) {
			$new_order -> set_customer_note('');
		}

		$new_order->save();

		return $new_order;
	}

	/**
	 * Add fulfillment month to an order
	 *
	 * @param   WC_Order $new_order order created from the subscription
	 * @param   WC_Subscription $subscription from which the order is generated
	 * @param   string $type of order. Default values are 'renewal_order'|'resubscribe_order'
	 * @return  WC_Order|WC_Error New order or error object.
	 */
	public static function ogk_add_fulfillment_month_to_order( $order ) {
		$fulfillment_month = OGK_Subscription_Dates::get_fulfillment_month();
		$order->update_meta_data( "_fulfillment_month", $fulfillment_month );
		$order->save();

		return $order;
	}

	public static function ogk_shipstation_custom_field_2() {
		return "_fulfillment_month";
	}

	public static function ogk_shipstation_custom_field_2_value( $field_2_value , $order_id ) {

		$order = wc_get_order( $order_id );
		
		// Add subscription_first_order to first time subscriptions
		if ( $order && wcs_order_contains_subscription( $order, 'parent' ) ) {
			$field_2_value .= ' subscription_first_order';
		}

		return $field_2_value;
	}

	public static function ogk_shipstation_custom_field_3() {
		return "_shipstation_custom_field_3";
	}

	public static function ogk_shipstation_custom_field_3_value( $field_3_value , $order_id ) {

		$order = wc_get_order( $order_id );
		
		if ( $order ) {
	
			$subscriptions = wcs_get_subscriptions_for_order( $order, array( 'order_type' => array( 'any', 'shipment' ) ) );

			if( !empty( $subscriptions ) ) {

				$subscription = array_shift( $subscriptions );

				if( $subscription instanceof WC_Subscription ) {

					$subscription_start_date = $subscription->get_date_created();
					$subscription_formatted_start_date = $subscription_start_date->format( 'M, Y' );
					
					// Customer specific info
					$customer_id = $subscription->get_customer_id();

					if( !empty( $customer_id ) ) {

						$customer_subscriptions = wcs_get_users_subscriptions($customer_id);
						$customer_total_subscriptions = count($customer_subscriptions);
	
						// Tested 10 different subscriptions and noticed that the subs are in descending order to the first order --
						// although this may not be the ideal solution, it was quicker than looping through the subscriptions and checking dates
						$customer_first_subscription_date = end($customer_subscriptions)->get_date_created()->date('M Y');
					}
					else {
						// SHOULD NOT BE ABLE TO GET HERE BECAUSE SUBSCRIPTIONS SHOULD ALWAYS HAVE CUSTOMERS
						// BUT IT CAN HAPPEN (IF SUBSCRIPTION WAS CREATED MANUALLY WITHOUT A CUSTOMER)
						$customer_id = '!ERR NO CUST!';
						$customer_total_subscriptions = 1;
						$customer_first_subscription_date = $subscription->get_date_created()->date('M Y');
					}


					$past_shipments = count( $subscription->get_related_orders( 'ids', array( 'any', 'shipment' ) ) ) - 1;
					
	
					$field_3_value = "S.ID: {$subscription->get_id()}; S.FO: {$subscription_formatted_start_date}; S.#PS: {$past_shipments}; C.ID: {$customer_id}; C.FSO: {$customer_first_subscription_date}; C.Subs.Ever: {$customer_total_subscriptions};";
				}
			}
		}

		return $field_3_value;
	}


	/**
	 * DEPRECATED - Hook to append tracking to emails
	 * System replaced by Woocommerce Shipment Tracking Pluguin
	 */
	public static function ogk_wc_admin_order_tracking( $order ) {
		$tracking = OGK_Tracking_Details::get_order_tracking( $order );

		if ( $tracking ) {
			echo '<div class="address">';
			echo '<p><strong>Tracking</strong></p>';
			echo '<p>' . $tracking['name'] . " " . $tracking['number'] . '<br>';
			echo '<a href="' . $tracking['link'] . '" target="_blank">Track &rarr;</a>';
			echo '</p>';
			echo '</div>';
		}

	}

	/**
	 * Add globegistics tracking links
	 *
	 * @param [array] $providers
	 * @return array
	 */
	public static function ogk_shipment_tracking_add_intl_links( $providers ) {
        $countries = array('Australia','Austria','Brazil','Belgium','Canada','Czech Republic','Finland','France','Germany','Ireland','Italy','India','Italy','Netherlands','New Zealand','Poland','Romania','South African','Sweden','United Kingdom');

		foreach( $countries as $country ) {

			$providers[ $country ] = $providers[ $country ] ?? array();

			$providers[ $country ]['globegistics'] = 'https://a1.asendiausa.com/tracking/?trackingnumber=%1$s';
		}

        return $providers;
    }
}