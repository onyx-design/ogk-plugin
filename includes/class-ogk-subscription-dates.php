<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

class OGK_Subscription_Dates {

	public static $ogk_month_last_day = 24;
	public static $ogk_renewal_day    = 4;

	/**
	 * Get the correct timezone
	 *
	 * @param   string              $timezone
	 * @return  DateTimeZone|null   the timezone
	 */
	public static function init_timezone( $timezone = 'default' ) {
		if ( is_a( $timezone, 'DateTimeZone' ) ) {
			return $timezone;
		}

		switch ( $timezone ) {
			case 'default':
			case 'wordpress':
			case 'site':
				$timezone = get_option( 'timezone_string' );
				break;
			case 'utc':
			case 'UTC':
			case 'gmt':
			case null:
			default:
				$timezone = 'UTC';
				break;
		}
		return ( $timezone ? new DateTimeZone( $timezone ) : null );
	}

	/**
	 * Helper function to initialize date object
	 */
	public static function init_datetime( $date = "now", $timezone = 'default' ) {
		$timezone = self::init_timezone( $timezone );

		if ( !is_a( $date, 'DateTime' ) ) {
			// Check if date is ogk month code (YYYYMM)
			if ( self::validate_fulfillment_month_code( $date ) ) {
				$date = date_create_from_format( 'Ym', $date, $timezone );
			} else {
				$date = new DateTime( $date, $timezone );
			}
		}

		return ( clone $date );
	}

	/**
	 * Get the fulfillment month for a given date
	 * Default: returns current fulfillment month
	 *
	 * @param   string          $date used to find fulfillment month
	 * @param   string          $format of date to return
	 * @param   string          $timezone
	 * @param   bool            $reset_time when true sets time to 00:00, else time is retained from date
	 * @return  DateTime|bool   date
	 */
	public static function get_fulfillment_month( $date = "now", $format = 'Ym', $timezone = 'default', $reset_time = true ) {
		$timezone = self::init_timezone( $timezone );

		$date = self::init_datetime( $date );

		if ( empty( $date ) ) {
			return false;
		}

		$year  = intval( $date->format( 'Y' ) );
		$month = intval( $date->format( 'n' ) );

		// REMOVED: NOW FULFILLMENT MONTHS = CALENDAR MONTHS //dev:dates
		// if ( intval( $date->format( 'j' ) ) > self::$ogk_month_last_day ) {
		// 	$month++;
		// 	if ( $month > 12 ) {
		// 		$month = 1;
		// 		$year++;
		// 	}
		// }

		$date->setDate( $year, $month, 1 );
		if ( $reset_time ) {
			$date->setTime( 0, 0, 0, 0 );
		}

		return ( empty( $format ) || 'object' == $format )
		? $date
		: $date->format( $format );
	}

	/**
	 * "Absolute Modulus"
	 * Reverses negative modulus results so that the remainder is always normalized from the "beginning" of the divisor
	 *
	 * @param [type] $dividend
	 * @param [type] $divisor
	 * @return void
	 */
	public static function absmodulus( int $dividend, int $divisor ) {
		return ( ( ($dividend % $divisor) + $divisor ) % $divisor );
	}

	/**
	 * Reliably add/subtract months from dates
	 */
	public static function date_modify_subscription_month( $date = "now", $months_adjust = 1, $format = 'Y-m-d H:i:s', $timezone = 'default', $prevent_first_last_day_of_month = true, $set_day_of_month = false ) {

		$date_start = self::init_datetime( $date, $timezone );

		// Get Date Pieces (as ints)
		$start_date_pieces = array(
			'year'	=> intval( $date_start->format( 'Y' ) ),
			'month'	=> intval( $date_start->format( 'n' ) ),
			'day'	=> intval( $date_start->format( 'j' ) ),
			'hour'	=> intval( $date_start->format( 'G' ) ),
			'minute'=> intval( $date_start->format( 'i' ) ),
			'second'=> intval( $date_start->format( 's' ) )
		);

		$result_date_pieces = $start_date_pieces;

		// Calculate new month & year
		$adjusted_months_relative = $start_date_pieces['month'] + $months_adjust;
		$adjusted_years_relative = intdiv( ($adjusted_months_relative - 1), 12 );

		$result_year = $start_date_pieces['year'] + $adjusted_years_relative;
		$result_month = self::absmodulus( $adjusted_months_relative, 12 ) ?: 12; // If result is 0, set to 12

		// Determine result day of month 
		$result_day = is_int( $set_day_of_month ) ? $set_day_of_month : $start_date_pieces['day'];

		// Determine min and max day of month
		$min_day = 1;
		$max_day = cal_days_in_month( 0, $result_month, $result_year );

		if( $prevent_first_last_day_of_month ) {
			// Force result day not to be first or last day of month 
			// Helps avoid any timezone issues causing fulfillment month crossover 
			$min_day += 1;
			$max_day -= 1;
		}
		
		if( $result_day < $min_day ) {
			$result_day = $min_day;
		}
		else if( $result_day > $max_day ) {
			$result_day = $max_day;
		}


		// Build result date
		$date_result = clone( $date_start );

		$date_result->setDate( $result_year, $result_month, $result_day );
		
		return ( empty( $format ) || 'object' == $format )
		? $date_result
		: $date_result->format( $format );
	}

	/**
	 * Get fulfillment month index for a date
	 * Numerical index is number of months passed since 1970-01-01 (AKA Unix Timestamp: 0)
	 *
	 * @param   string  $date
	 * @param   string  $timezone
	 * @return  int     the month index
	 */
	public static function get_ogk_month_index( $date = "now", $timezone = "default" ) {
		$date        = self::get_fulfillment_month( $date, 'object', $timezone, false );
		$year        = intval( $date->format( 'Y' ) );
		$month       = intval( $date->format( 'n' ) );
		$month_index = (  ( $year - 1970 ) * 12 ) + $month;
		return $month_index;
	}

	/**
	 * Validate a fulfillment month code
	 * Fulfillment month codes are strings in the format of YYYYMM
	 *
	 * @param   string      $fulfillment_month
	 * @return  string|bool the fulfillment month code
	 */
	public static function validate_fulfillment_month_code( $fulfillment_month ) {

		if ( empty( $fulfillment_month ) || 6 !== strlen( $fulfillment_month ) ) {
			return false;
		}
		$check_date = date_create_from_format( 'Ym', $fulfillment_month );

		if ( empty( $check_date ) ) {
			return false;
		}
		return ( $fulfillment_month == $check_date->format( 'Ym' ) ) ? $fulfillment_month : false;
	}

	/**
	 * Get the number of fulfillment months between two dates
	 *
	 * @param   string  $from_date
	 * @param   string  $to_date
	 * @param   string  $timezone
	 * @return  int     the difference between two months
	 */
	public static function fulfillment_months_date_diff( $from_date = "now", $to_date = "now", $timezone = 'default' ) {

		$from_date_month_index = self::get_ogk_month_index( $from_date, $timezone );
		$to_date_month_index   = self::get_ogk_month_index( $to_date, $timezone );
		$date_diff_months      = $to_date_month_index - $from_date_month_index;
		return $date_diff_months;
	}

	/**
	 * Determine if a date is on the first or last day of a month
	 */
	public static function is_date_first_or_last_day_of_month( $date = "now", $timezone = 'default' ) {
		$date = self::init_datetime( $date, $timezone );

		$date_day_of_month = intval( $date->format( 'j' ) );

		if( 1 === $date_day_of_month ) {
			return true;
		}
		else {
			$year        = intval( $date->format( 'Y' ) );
			$month       = intval( $date->format( 'n' ) );

			$max_day_of_month = cal_days_in_month( 0, $month, $year );

			if( $date_day_of_month >= $max_day_of_month ) {
				return true;
			}
		}

		return false;
	}
}
