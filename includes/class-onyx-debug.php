<?php
// Exit if accessed directly

if ( !defined( 'ABSPATH' ) ) {
	exit;
}

class ONYX_Debug {

	use ONYX_Singleton_Class_Abstract;

	protected $debug_active = false;

	public $stats = array();

	public $data = array();

	protected $shutdown_registered = false;

	protected function __construct() {

		//ini_set( "error_reporting", E_ALL );
		error_reporting( E_ALL );
		ini_set( "log_errors", 1 );
	}

	public function __invoke( $arg = null ) {

		$this->log( $arg );
	}

	public function is_active() {
		return $this->debug_active;
	}

	public function set_active( $set = true ) {
		$this->debug_active = (bool) $set;

		if ( $this->debug_active && !$this->shutdown_registered ) {
			$this->shutdown_registered = true;
			add_action( 'shutdown', array( $this, 'debug_shutdown' ) );
		} elseif ( !$this->debug_active ) {
			remove_action( 'shutdown', array( $this, 'debug_shutdown' ) );
		}

		return $this;
	}

	public function log( $msg, $force = false ) {

		if ( $force || $this->debug_active ) {
			error_log(  ( is_scalar( $msg ) ? $msg : print_r( $msg, true ) ) );
		}

	}

	public function stat( $name, $value = null, $log_stats = true ) {

		if ( $log_stats ) {
			$this->set_active();
			$this->shutdown_registered = true;
			add_action( 'shutdown', array( $this, 'debug_shutdown' ) );
			//register_shutdown_function( array( $this, 'debug_shutdown' ) );
		}

		if ( !is_string( $name ) ) {
			return false;
		} else {

// Init the stat if it doesn't exist
			if ( !isset( $this->stats[$name] ) ) {

				$this->stats[$name] = 0;
			}

// If a $value is supplied, store it
			if ( isset( $value ) ) {

				$this->stats[$name] = $value;
			}

			// If no $value is supplied and stat is an integer, increment it
			elseif ( is_int( $this->stats[$name] ) ) {

				$this->stats[$name]++;
			}

			return $this->stats[$name];
		}

	}

	public function debug_shutdown() {
		global $onyx_load_time;

		$this->log( "ONYX DEBUG POST-SHUTDOWN REPORT" );
		$this->log( "ONYX load time: " . $onyx_load_time );
		$this->log( "wpdb queries: " . get_num_queries() );
		$this->log( "Memory Usage: " . memory_get_usage() );
		$this->log( "Memory Peak Usage: " . memory_get_peak_usage() );

		if ( $this->stats ) {

			$this->log( $this->stats );
		}

	}

}
