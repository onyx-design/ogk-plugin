<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * DEPRECATED - System for processing tracking information and linking them to orders
 * Now replaced by WooCommerce Shipment Tracking
 * Class should be removed when safe
 */
class OGK_Tracking_Details {
	public static $tracking_schema = array(
		'ups'   => array(
			'name' => 'UPS',
			'url'  => 'https://www.ups.com/track?tracknum=%s',
		),
		'usps'  => array(
			'name' => 'USPS',
			'url'  => 'https://tools.usps.com/go/TrackConfirmAction?tRef=fullpage&tLabels=%s',
		),
		'dhlgm' => array(
			'name' => 'DHL Global Mail',
			'url'  => 'https://webtrack.dhlglobalmail.com/?trackingnumber=%s',
		),
		'rrd'   => array(
			'name' => 'RR Donnelley',
			'url'  => 'https://track24.net/service/RRD/tracking/%s',
		),
		'other' => array(
			'name' => 'Unknown',
			'url'  => 'https://track24.net/?code=%s',
		),
	);

	public static function get_order_tracking( $order ) {

		$order    = wc_get_order( $order );
		$tracking = false;

		if ( wcs_is_order( $order ) ) {

			$tracking_number = get_post_meta( $order->get_id(), '_tracking_number', true );

			if ( !empty( $tracking_number ) ) {

				$tracking = array(
					'number'    => $tracking_number,
					'provider'  => get_post_meta( $order->get_id(), '_tracking_provider', true ),
					'ship_date' => get_post_meta( $order->get_id(), '_date_shipped', true ),
					'name'      => '',
					'link'      => '',
				);
				self::build_tracking( $tracking );
			}
		}

		return $tracking;

	}

	public static function get_order_tracking_link( $order ) {

		$tracking = self::get_order_tracking( $order );

		return $tracking['link'] ?: false;
	}

	protected static function build_tracking( &$tracking ) {

		if ( $tracking && !empty( $tracking['number'] ) ) {

			if ( empty( $tracking['provider'] ) || 'other' == $tracking['provider'] ) {

				$tracking['provider'] = self::get_provider_from_number( $tracking['provider'] );
			}

			if ( !empty( $tracking['provider'] )
				&& isset( self::$tracking_schema[$tracking['provider']] )
			) {

				$tracking_schema = self::$tracking_schema[$tracking['provider']];

				$tracking['name'] = $tracking_schema['name'] ?: '';

				$tracking['link'] = sprintf( $tracking_schema['url'], $tracking['number'] );
			}
		}

		return $tracking;
	}

	protected static function get_provider_from_number( $tracking_number ) {

		// This can be expanded later
		if ( 0 === strpos( $tracking_number, 'RRD' ) ) {
			return 'rrd';
		} else if ( 0 === strpos( $tracking_number, 'GM' ) ) {
			return 'dhlgm';
		}

		return 'other';
	}
}
