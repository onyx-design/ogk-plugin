<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * GENERAL FUNCTIONS
 *
 * Mainly generic helper functions
 **/
if ( !function_exists( 'is_onyx_admin' ) ) {

	function is_onyx_admin() {

		$current_user = wp_get_current_user();

		if (  ( $current_user->user_email == 'dev@onyxdesign.net' ) ) {
			return true;
		}

		return false;
	}
}

if ( !function_exists( 'onyx_debug' ) ) {
	// Auto-init and access ONYX_Debug class
	function onyx_debug( $arg = null ) {
		$instance = ONYX_Debug::instance();
		return func_num_args() ? $instance( $arg ) : $instance;
	}
}

if ( !function_exists( 'onyx_dev_log' ) ) {
	// Log to onyx-dev-log.txt
	function onyx_dev_log( $msg, $clear_log = false ) {
		$msg = ( is_object( $msg ) || is_array( $msg ) ) ? print_r( $msg, true ) : $msg;
		if ( $clear_log ) {
			file_put_contents( ONYX_CONTENT_DIR . '/onyx_dev_log.txt', "\r\n" . date( 'Y-m-d H:i:s' ) . substr( (string) microtime(), 1, 8 ) . ' - ' . $msg );
		} else {
			file_put_contents( ONYX_CONTENT_DIR . '/onyx_dev_log.txt', "\r\n" . date( 'Y-m-d H:i:s' ) . substr( (string) microtime(), 1, 8 ) . ' - ' . $msg, FILE_APPEND );
		}

	}
}

if ( !function_exists( 'ox_exec_time' ) ) {
	// Get script execution time
	function ox_exec_time() {
		return ( microtime( true ) - $_SERVER["REQUEST_TIME_FLOAT"] );
	}
}

if ( !function_exists( 'log_print_r' ) ) {
	function log_print_r( $log, $message = '' ) {

		$log = print_r( $log, true );

		if ( $message ) {
			$log = "$message: $log";
		}

		error_log( $log );
	}
}

if ( !function_exists( 'log_var' ) ) {
	function log_var( $log, $message = '' ) {

		$log = var_export( $log, true );

		if ( $message ) {
			$log = "$message: $log";
		}

		error_log( $log );
	}
}

if ( !function_exists( 'maybe_str_to_bool' ) ) {
	function maybe_str_to_bool( $boolstr = '' ) {
		if ( !is_bool( $boolstr ) ) {

			$boolstr = strtolower( $boolstr );

			if ( 'true' === $boolstr ) {
				return true;
			} elseif ( 'false' === $boolstr ) {
				return false;
			} else {
				return $boolstr;
			}
		}

		return $boolstr;
	}
}

if ( !function_exists( 'boolstr' ) ) {
	function boolstr( $b ) {
		return $b ? 'true' : 'false';
	}
}

if ( !function_exists( 'onyx_remove_action_once' ) ) {
	function onyx_remove_action_once( $hook, $callback, $priority ) {
		global $wp_filter;

		$wp_hook = $wp_filter[$hook];

		if ( is_a( $wp_hook, 'WP_Hook' ) ) {

			$removal_priority = max( array_keys( $wp_hook->callbacks ) ) + 1;

			remove_action( $hook, $callback, $priority );

			add_action( $hook, function () use ( $hook, $callback, $priority, $wp_hook, $removal_priority ) {

				// Add the action back in
				add_action( $hook, $callback, $priority );

				$wp_hook->offsetUnset( $removal_priority );
			}, $removal_priority );
		}
	}
}
