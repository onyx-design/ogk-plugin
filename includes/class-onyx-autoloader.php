<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

class OGK_Autoloader {
	/**
	 * Path to the includes directory
	 * @var string
	 */
	private $include_path = '';

	private $prefixes = array(
		'admin' => 'admin',
		// 'email'		=> 'email', // Figure out why this isn't loading right and re-enable
	);

	public function __construct() {
		if ( function_exists( '__autoload' ) ) {
			spl_autoload_register( '__autoload' );
		}

		spl_autoload_register( array( $this, 'autoload' ) );

		$this->include_path = OGK_PATH . '/includes/';
	}

	private function get_file_name_from_class( $class ) {
		return 'class-' . str_replace( '_', '-', strtolower( $class ) ) . '.php';
	}

	/**
	 * Scan appropriate subdirs for file
	 * @param  string $class
	 * @return string
	 */
	private function load_class_from_subdir( $file_name ) {
		foreach ( $this->prefixes as $subdir => $prefix ) {

			// Check for prefix pattern in filename
			$file_prefix = 'class-ogk-' . $prefix;

			if (
				0 === strpos( $file_name, $file_prefix )
				&& $this->load_file( $subdir . '/' . $file_name )
			) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Include a class file
	 * @param  string $path
	 * @return bool successful or not
	 */
	private function load_file( $file ) {
		if ( $file && is_readable( $this->include_path . $file ) ) {

			include_once $this->include_path . $file;
			return true;
		}

		return false;
	}

	/**
	 * Auto-load classes on demand to reduce memory consumption.
	 *
	 * @param string $class
	 */
	public function autoload( $class ) {
		// Convert classname to filename
		$file_name = $this->get_file_name_from_class( $class );

		// Check base include path first
		if ( !$this->load_file( $file_name ) ) {

			$this->load_class_from_subdir( $file_name );
		}
	}
}
new OGK_Autoloader();
