<?php
defined( 'ABSPATH' ) || exit;

/**
 * OGK_Email_Loader overrides WooCommerce Emails with files in templates/emails of the same name
 */
class OGK_Email_Loader {
	public function __construct() {

		// Add Custom WC Emails
		add_filter( 'woocommerce_email_classes', array( $this, 'ogk_custom_wc_emails' ) );


		add_filter( 'woocommerce_locate_template', array( __CLASS__, 'wc_email_template_loader' ), 10, 3 );
	}

	public function ogk_custom_wc_emails( $email_classes ) {
		
		// New Emails
		$email_classes['OGK_WCS_Email_Pre_Renewal']				= include_once( OGK_PATH . '/includes/email/class-ogk-wcs-email-pre-renewal.php' );

    	return $email_classes;
	}

	public static function wc_email_template_loader( $template, $template_name, $template_path ) {
		// Check for email templates in our plugin
		if ( 0 === strpos( $template_name, 'emails/' ) ) {
			$basename = basename( $template );
			if ( file_exists( OGK_TEMPLATE_PATH . "emails/{$basename}" ) ) {
				$template = OGK_TEMPLATE_PATH . "emails/{$basename}";
			}
		}

		return $template;
	}
}
return new OGK_Email_Loader();
