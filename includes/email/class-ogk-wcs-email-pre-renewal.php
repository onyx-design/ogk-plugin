<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if( !class_exists( 'OGK_WCS_Email_Pre_Renewal' ) ) {

	/**
	 * Pre-Renewal Email
	 * 
	 * An email sent to the user when their subscription renewal is about to hit.
	 */
	class OGK_WCS_Email_Pre_Renewal extends WC_Email {

		/**
		 * Constructor
		 */
		public function __construct() {
			
			$this->id             	= 'pre_renewal_notification';
			$this->customer_email 	= true;

			$this->heading 			= 'Your Next Shipment';
			$this->subject 			= 'Your Next Renewal Order Is Coming Soon';

			$this->title          	= 'Pre-Renewal Notification';
			$this->description    	= 'Pre-Renewal Notification emails are sent to the user when their subscription renewal is about to hit.';

			$this->greeting 		= 'Hi';
			$this->closing 			= 'Team @GreenKidCrafts';

			$this->template_html  	= 'emails/ogk-wcs-pre-renewal.php';
			$this->template_plain 	= 'emails/plain/ogk-wcs-pre-renewal.php';
			$this->template_base 	= OGK_TEMPLATE_PATH;
			$this->placeholders   	= array(
				'{renewal_date}'	=> '',
			);

			// Triggers for this email
			add_action( 'onyx_email_' . $this->id, array( $this, 'trigger' ), 10, 2 );

			// Call parent constructor
			parent::__construct();
		}

		/**
		 * Get email subject
		 * 
		 * @return 	string
		 */
		public function get_subject() {
			return $this->get_option( 'subject', $this->get_default_subject() );
		}

		/**
		 * Get email heading
		 * 
		 * @return 	string
		 */
		public function get_heading() {
			return $this->get_option( 'heading', $this->get_default_heading() );
		}

		/**
		 * Get default email subject
		 * 
		 * @return 	string
		 */
		public function get_default_subject() {
			return $this->subject;
		}

		/**
		 * Get default email heading
		 * 
		 * @return 	string
		 */
		public function get_default_heading() {
			return $this->heading;
		}

		/**
		 * Get email greeting
		 * 
		 * @return 	string
		 */
		public function get_greeting() {
			return $this->get_option( 'greeting', $this->get_default_greeting() );
		}

		/**
		 * Get email closing
		 * 
		 * @return 	string
		 */
		public function get_closing() {
			return $this->get_option( 'closing', $this->get_default_closing() );
		}

		/**
		 * Get default email greeting
		 * 
		 * @return string
		 */
		public function get_default_greeting() {
			return $this->greeting;
		}

		/**
		 * Get default email closing
		 * 
		 * @return string
		 */
		public function get_default_closing() {
			return $this->closing;
		}

		/**
		 * Trigger the sending of this email
		 */
		public function trigger( $order_id, $order ) {

			$this->setup_locale();

			if( $order_id && !is_a( $order, 'WC_Order' ) ) {
				$order = wc_get_order( $order_id );
			}

			if( is_a( $order, 'WC_Order' ) ) {
				$this->object = $order;
				$this->recipient = $this->object->get_billing_email();

				$this->renewal_date = date( 'F d, Y \a\t g:ia', strtotime( $order->get_date( 'next_payment' ) ) );
				$this->order_total = wc_price( $order->get_total() );
			}

			if( $this->is_enabled() && $this->get_recipient() ) {
				$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
			}

			$this->restore_locale();

		}

		/**
		 * Get content HTML
		 * 
		 */
		public function get_content_html() {
			ob_start();
			wc_get_template( 
				$this->template_html, 
				array(
					'order'				=> $this->object,
					'email_heading' 	=> $this->get_heading(),
					'greeting'			=> $this->get_greeting(),
					'renewal_date'		=> $this->renewal_date,
					'order_total'		=> $this->order_total,
					'closing'			=> $this->get_closing(),
					'sent_to_admin' 	=> false,
					'plain_text'    	=> false,
					'email'				=> $this,
				),
				'',
				$this->template_base
			);
			return ob_get_clean();
		}

		/**
		 * Get content plain
		 * 
		 */
		public function get_content_plain() {
			ob_start();
			wc_get_template( 
				$this->template_plain, 
				array(
					'order'				=> $this->object,
					'email_heading' 	=> $this->get_heading(),
					'greeting'			=> $this->get_greeting(),
					'renewal_date'		=> $this->renewal_date,
					'order_total'		=> $this->order_total,
					'closing'			=> $this->get_closing(),
					'sent_to_admin' 	=> false,
					'plain_text'    	=> true,
					'email'				=> $this,
				),
				'',
				$this->template_base
			);
			return ob_get_clean();
		}

		/**
		 * Initialize settings form fields
		 */
		public function init_form_fields() {
			$this->form_fields = array(

				'enabled' => array(
					'title'         => __( 'Enable/Disable', 'woocommerce' ),
					'type'          => 'checkbox',
					'label'         => __( 'Enable this email notification', 'woocommerce' ),
					'default'       => 'yes',
				),

				'subject' => array(
					'title'         => __( 'Subject', 'woocommerce' ),
					'type'          => 'text',
					'desc_tip'      => true,
					'description'   => sprintf( __( 'Available placeholders: %s', 'woocommerce' ),
						''
					),
					'placeholder'   => $this->get_default_subject(),
					'default'       => $this->get_subject(),
				),

				'heading' => array(
					'title'         => __( 'Email heading', 'woocommerce' ),
					'type'          => 'text',
					'desc_tip'      => true,
					'description'   => sprintf( __( 'Available placeholders: %s', 'woocommerce' ),
						''
					),
					'placeholder'   => $this->get_default_heading(),
					'default'       => $this->get_heading(),
				),

				'greeting' => array(
					'title'         => __( 'Email Greeting', 'woocommerce' ),
					'type'          => 'text',
					'placeholder'   => $this->get_default_greeting(),
					'default'       => $this->get_greeting(),
				),

				'closing' => array(
					'title'         => __( 'Email Closing', 'woocommerce' ),
					'type'          => 'text',
					'placeholder'   => $this->get_default_closing(),
					'default'       => $this->get_closing(),
				),

				'email_type' => array(
					'title'         => __( 'Email type', 'woocommerce' ),
					'type'          => 'select',
					'description'   => __( 'Choose which format of email to send.', 'woocommerce' ),
					'default'       => 'html',
					'class'         => 'email_type wc-enhanced-select',
					'options'       => $this->get_email_type_options(),
					'desc_tip'      => true,
				),
			);
		}
	}
}

return new OGK_WCS_Email_Pre_Renewal();