<?php
defined( 'ABSPATH' ) || exit;

class ONYX_Template_Loader {

	public static function init() {
		add_filter( 'template_include', array( __CLASS__, 'template_loader' ) );

		add_filter( 'woocommerce_locate_template', array( __CLASS__, 'locate_template' ), 10, 3 );
	}

	public static function template_loader( $template ) {
		$default_file = self::get_template_loader_default_file();

		if ( $default_file ) {
			$search_files    = self::get_template_loader_files( $default_file );
			$plugin_template = locate_template( $search_files );
			if ( !$plugin_template ) {
				$plugin_template = OPERATIONS_TEMPLATE_PATH . $default_file;
			}

			// If the template exists in the plugin, use it
			// dev:improve this, I don't like checking this here
			if ( is_readable( $plugin_template ) ) {
				$template = $plugin_template;
			}
		}

		return $template;
	}

	private static function get_template_loader_default_file() {

		if ( is_tax() ) {
			$object = get_queried_object();

			$default_file = 'taxonomies/taxonomy-' . $object->taxonomy . '.php';
		} elseif ( is_page() ) {

			if ( in_array( get_page_template_slug(), ONYX()->page_templates() ) ) {
				$default_file = 'pages/' . str_replace( 'onyx-', '', get_page_template_slug() );
			} else {
				$default_file = '';
			}
		} else {
			$default_file = '';
		}

		return $default_file;
	}

	/**
	 * Get an array of filenames to search for, given a default file
	 *
	 * @param 	string 	$default_file
	 *
	 * @return 	array
	 */
	private static function get_template_loader_files( $default_file ) {
		$templates   = array();
		$templates[] = $default_file;

		return array_unique( $templates );
	}

	/**
	 * Look for WooCommerce templates in our plugin templates
	 *
	 * @param 	string 	$template
	 * @param 	string 	$template_name
	 * @param 	string 	$template_path
	 *
	 * @return 	string
	 */
	public static function locate_template( $template, $template_name, $template_path ) {
		if ( is_readable( OPERATIONS_TEMPLATE_PATH . $template_path . $template_name ) ) {
			return OPERATIONS_TEMPLATE_PATH . $template_path . $template_name;
		}

		return $template;
	}
}
add_action( 'init', array( 'ONYX_Template_Loader', 'init' ) );
