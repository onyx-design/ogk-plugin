<?php
defined( 'ABSPATH' ) || exit;

class ONYX_Frontend_Assets {

	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'frontend_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'frontend_scripts' ) );
	}

	public function frontend_styles() {

		wp_enqueue_style(
			'jquery-modal',
			OGK()->plugin_url() . '/assets/css/vendor/jquery.modal.min.css',
			array(),
			OGK()->version
		);

		wp_enqueue_style(
			'onyx-css',
			OGK()->plugin_url() . '/dist/css/onyx.css',
			array(
				'main-styles',
				'dynamic-css',
				'woocommerce',
			),
			OGK()->version
		);
	}

	public function frontend_scripts() {

		if ( wcs_is_view_subscription_page() ) {
			wp_enqueue_script(
				'moment',
				OGK()->plugin_url() . '/assets/js/vendor/moment.min.js',
				array(),
				OGK()->version
			);
		}

		wp_enqueue_script(
			'jquery-modal',
			OGK()->plugin_url() . '/assets/js/vendor/jquery.modal.min.js',
			array( 'jquery' ),
			OGK()->version
		);

		wp_enqueue_script(
			'onyx',
			OGK()->plugin_url() . '/dist/js/onyx.js',
			array(
				'jquery',
				'selectWoo',
				'wc-checkout',
			),
			OGK()->version
		);
	}
}
return new ONYX_Frontend_Assets();
