<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

class Operations_Autoloader {

	/**
	 * Path to the includes directory
	 * @var string
	 */
	private $include_path = '';

	/**
	 * Prefixes for different ONYX Class Groups (Used to determine subdirectories)
	 * @var array
	 */
	private $prefixes = array(
		'credit'         => 'credit',
		'data-migration' => 'data-migration',
		'frontend'       => 'frontend',
		'html'           => 'html',
		'operation'      => 'operation',
		'model'          => 'model',
		'subscription'   => 'subscription',
		'table'          => 'table',
	);

	/**
	 * The Constructor
	 */
	public function __construct() {
		if ( function_exists( '__autoload' ) ) {
			spl_autoload_register( '__autoload' );
		}

		spl_autoload_register( array( $this, 'autoload' ) );

		$this->include_path = OGK_OPERATIONS_PATH . '/includes/';
	}

	private function get_file_name_from_class( $class ) {
		return 'class-' . str_replace( '_', '-', strtolower( $class ) ) . '.php';
	}

	/**
	 * Scan appropriate subdirs for file
	 * @param  string $class
	 * @return string
	 */
	private function load_class_from_subdir( $file_name ) {

		foreach ( $this->prefixes as $subdir => $prefix ) {

			// Check for prefix pattern in filename
			$file_prefix = 'class-onyx-' . $prefix;

			if (
				0 === strpos( $file_name, $file_prefix )
				&& $this->load_file( $subdir . '/' . $file_name )
			) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Include a class file
	 * @param  string $path
	 * @return bool successful or not
	 */
	private function load_file( $file ) {

		if ( $file && is_readable( $this->include_path . $file ) ) {

			include_once $this->include_path . $file;
			return true;
		}

		return false;
	}

	/**
	 * Auto-load classes on demand to reduce memory consumption.
	 *
	 * @param string $class
	 */
	public function autoload( $class ) {

		// Convert classname to filename
		$file_name = $this->get_file_name_from_class( $class );

		// Check base include path first
		if ( !$this->load_file( $file_name ) ) {

			$this->load_class_from_subdir( $file_name );
		}
	}
}
new Operations_Autoloader();
