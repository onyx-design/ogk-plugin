<?php
defined( 'ABSPATH' ) || exit;

class ONYX_Operation_export_order_items extends ONYX_Operation_Base {

	public static $optype = 'export_order_items';

	// public static $actions = array(
	// 	'check_wcs_user_role'
	// );
	public static $jump_interval = 45;

	protected $export_csv_fields = array(
		'Order ID'            => '',
		'Date Created'        => '',
		'Date Paid'           => '',
		'Date Completed'      => '',
		'Date Modified'       => '',
		'Order Status'        => '',
		'Old Order ID'        => '',
		'Customer ID'         => '',
		'Billing Email'       => '',
		'Billing First Name'  => '',
		'Billing Last Name'   => '',
		'Shipping First Name' => '',
		'Shipping Last Name'  => '',
		'Line Item Name'      => '',
		'Line Item SKU'       => '',
		'Line Item ID'        => '',
		'Line Item Price'     => '',
		'Line Item Qty'       => '',
		'Line Item Subtotal'  => '',
		'Line Item Total'     => '',
	);

	protected $wc_order_query_default_args = array(
		'orderby'  => 'date',
		'order'    => 'DESC',
		'limit'    => 50,
		'type'     => 'shop_order',
		'paged'    => 1,
		'paginate' => true,
	);

	protected $cached_product_skus = array();

	public function extra_init() {

		$this->init_stat( 'orders_exported', 'Orders Exported' );
		$this->init_stat( 'order_items_exported', 'Order Items Exported' );

		$this->new_export_file( 'order_items_export' );

		$this->write_csv_line_from_array( 'order_items_export', array_keys( $this->export_csv_fields ) );

		$this->opmeta( 'query_page', 1 );

		$orders = wc_get_orders( $this->wc_order_query_default_args );

		$this->opmeta( 'total_orders', $orders->total );

		$this->log( "Order Items Export Initialized - Total Items to export: " . $orders->total );

	}

	// Request-level properties
	// public $target_ids = array();

	public function run( $async = true ) {

		parent::run( $async );
		$running_op       = true;
		$outputted_totals = false;

		$query_args = $this->wc_order_query_default_args;

		while ( $running_op ) {

			// Get Orders
			$query_args['paged'] = $this->opmeta( 'query_page' );
			$results             = wc_get_orders( $query_args );

			// Maybe output updated totals
			if ( !$outputted_totals ) {
				$remaining_orders = $this->opmeta( 'total_orders' ) - $this->get_stat( 'orders_exported' );
				$this->log( "Remaining orders to export: " . $remaining_orders );
				$outputted_totals = true;
			}

			if ( count( $results->orders ) ) {
				foreach ( $results->orders as $order ) {

					$order_values = $this->export_csv_fields;

					$date_created   = $order->get_date_created();
					$date_paid      = $order->get_date_paid();
					$date_completed = $order->get_date_completed();
					$date_modified  = $order->get_date_modified();

					$order_values['Order ID']            = $order->get_id();
					$order_values['Date Created']        = $date_created ? $date_created->date( 'Y-m-d' ) : '';
					$order_values['Date Paid']           = $date_paid ? $date_created->date( 'Y-m-d' ) : '';
					$order_values['Date Completed']      = $date_completed ? $date_completed->date( 'Y-m-d' ) : '';
					$order_values['Date Modified']       = $date_modified ? $date_modified->date( 'Y-m-d' ) : '';
					$order_values['Order Status']        = $order->get_status();
					$order_values['Old Order ID']        = $order->get_meta( '_asc_id_old' ) ?: '';
					$order_values['Customer ID']         = $order->get_user_id();
					$order_values['Billing Email']       = $order->get_billing_email();
					$order_values['Billing First Name']  = $order->get_billing_first_name();
					$order_values['Billing Last Name']   = $order->get_billing_last_name();
					$order_values['Shipping First Name'] = $order->get_shipping_first_name();
					$order_values['Shipping Last Name']  = $order->get_shipping_last_name();

					$order_items = $order->get_items( 'line_item' );

					if ( count( $order_items ) ) {

						foreach ( $order_items as $order_item ) {

							$order_item_values = $order_values;

							$line_item_id  = '';
							$line_item_sku = '';

							$variation_id = $order_item->get_variation_id();

							if ( $variation_id && '0' != $variation_id ) {
								$line_item_id = $variation_id;
							} else if ( $product_id = $order_item->get_product_id() ) {
								$line_item_id = $product_id;
							}

							if ( $line_item_id ) {
								$line_item_sku = $this->get_product_sku_by_id( $line_item_id );
							}
							// $line_item_id = $order_item->get_meta( '_variation_id' ) ?: $order;

							$order_item_values['Line Item Name']     = $order_item->get_name();
							$order_item_values['Line Item SKU']      = $line_item_sku;
							$order_item_values['Line Item ID']       = $line_item_id;
							$order_item_values['Line Item Price']    = wc_format_decimal( $order_item->get_subtotal() / max( $order_item->get_quantity(), 1 ) );
							$order_item_values['Line Item Qty']      = $order_item->get_quantity();
							$order_item_values['Line Item Subtotal'] = $order_item->get_subtotal();
							$order_item_values['Line Item Total']    = $order_item->get_total();

							$this->write_csv_line_from_array( 'order_items_export', $order_item_values );

							$this->stat( 'order_items_exported' );
						}
					} else {
						$this->log( "WARNING - Order ID {$order->get_id()} does not have any order items" );
					}

					$this->stat( 'orders_exported' );
				}
			} else {
				$running_op = false;
				break;
			}

			$current_page = $this->opmeta( 'query_page' );

			if ( 0 == $current_page % 5 ) {
				$this->log( "Orders Exported: " . $this->get_stat( 'orders_exported' ) );
			}

			$next_page = $this->opmeta( 'query_page' ) + 1;
			$this->opmeta( 'query_page', $next_page );

			$this->maybe_jump_or_abort();

		}

		$this->successful_finish();

	}

	public function get_product_sku_by_id( $id ) {

		if ( !isset( $this->cached_product_skus[$id] ) ) {

			$product = wc_get_product( $id );

			$this->cached_product_skus[$id] = $product->get_sku();
		}

		return $this->cached_product_skus[$id];
	}

}