<?php
defined( 'ABSPATH' ) || exit;

class ONYX_Operation_Validate_Subscriptions extends ONYX_Operation_Base {

	public static $optype = 'validate_subscriptions';

	public static $jump_interval = 45;

	protected $wc_subscription_query_default_args = array(
		'subscriptions_per_page' => 5,
		'paged'                  => 1,
		'offset'                 => null,
		'orderby'                => 'ID',
		'order'                  => 'ASC',
		'customer_id'            => 0,
		'product_id'             => 0,
		'variation_id'           => 0,
		'order_id'               => 0,
		'subscription_status'    => array( 'any' ),
		'meta_query_relation'    => 'AND',
	);

	protected $export_csv_fields = array(
		'Subscription ID'         => '',
		'Status'                  => '',
		'Email Address'			  => '',
		'User ID'                 => '',
		'Shipping Country'        => '',
		'Total Initial Payment'   => '',
		'Subscription Total'      => '',

		'Billing Period'          => '',
		'Billing Interval'        => '',

		// Order Item Specific
		'Order Item Type'         => '',
		'Order Item Total'        => '',
		'Parent SKU'              => '',
		'Variation SKU'           => '',
		'Quantity'                => '',
		'Subtotal'                => '',

		'Line Item Price'         => '',
		'Variation Price'         => '',

		'Updated Variation ID'    => '',
		'Updated Variation SKU'   => '',
		'Updated Variation Price' => '',

		// Date Specific
		'Date Created'          => '',
		'Last Payment'			=> '',
		'Next Payment'          => '',
		'End Date'              => '',
		'Next Shipment'         => '',
		'Last Shipment'         => '',

		'GMT Date Created'      => '',
		'GMT Last Payment'		=> '',
		'GMT Next Payment'      => '',
		'GMT End Date'          => '',
		'GMT Next Shipment'     => '',
		'GMT Last Shipment'     => '',

		'Error Codes'          	=> array(),
		'Updates'				=> array()
	);

	protected $cached_product_skus = array();

	protected $valid_product_skus = array( 'GKC-JOIN', 'GKC-JOIN-JUNIOR', 'GKC-GIFT', 'GKC-GIFT-JUNIOR' );

	protected $valid_product_ids = array( 278117, 276469, 1585, 1473 );

	protected $valid_line_item_pricing = array( '29.95', '49.95', '83.85', '143.55', '161.70', '281.70', '299.40', '539.40' );

	protected $order_item_types = array( 'line_item', 'tax', 'shipping', 'fee', 'coupon' );

	public function extra_init() {
		$this->init_stat( 'subscriptions_validated', 'Subscriptions Validated' );
		$this->init_stat( 'line_items_updated', 'Line Iems Updated' );

		// VALIDATION ERROR CODES
		
		// Subscription validation
		$this->init_stat( 'subscription_no_items', 'Subscription has no order items' );
		$this->init_stat( 'subscription_no_line_items', 'Subscription has no product line items' );
		$this->init_stat( 'subscription_has_no_shipping_line_item', 'Subscription does not have a shipping line item' );
		$this->init_stat( 'subscription_missing_necessary_dates', 'Subscription missing necessary dates' );
		$this->init_stat( 'subscription_1_mo_has_next_shipment', '1 Month subscription has next shipment date' );
		$this->init_stat( 'subscription_too_many_shipments', 'Too many shipments' );
		$this->init_stat( 'subscription_unexpected_next_shipment_date', 'Unexpected next shipment date' );
		$this->init_stat( 'subscription_unexpected_next_payment_date', 'Unexpected next payment date' );
		$this->init_stat( 'subscription_unexpected_next_payment_date_future', 'Unexpected next payment date (in the future)' );
		$this->init_stat( 'subscription_unexpected_next_payment_date_after_end', 'Unexpected next payment date (after end date)' );



		// Subscription order item validation
		$this->init_stat( 'line_item_is_parent', 'Line item is a Parent Product' );
		$this->init_stat( 'variation_not_found', 'Line item variation cannot be derived from Parent Product' );
		$this->init_stat( 'line_item_not_variation', 'Line item is a not a variation' );
		$this->init_stat( 'line_item_ids_mismatch', 'Line item product id != variation product id' );
		$this->init_stat( 'variation_invalid', 'Invalid variation (probably DLX)' );
		$this->init_stat( 'us_delivery_not_free', 'Delivery charge is not free in USA' );
		
		$this->init_stat( 'item_mismatch_current_pricing', 'Item does not match current product price' );
		$this->init_stat( 'line_multiple_errors', 'Line item had multiple errors' );
		
		

		$this->opmeta( 'query_page', 1 );
		$this->log( "Subscription Validation Initialized" );
		$this->log( "exporting subscription statuses: " . $this->config( 'include_statuses' ) );

		if ( $this->config( 'dry_run_val' ) ) {
			$this->log( 'DRY RUN ENABLED - NO ACTUAL UPDATES WILL BE MADE' );
		}

		$this->new_export_file( 'export_csv' );
		$this->write_csv_line_from_array( 'export_csv', array_keys( $this->export_csv_fields ) );
	}

	public function run( $async = true ) {
		parent::run( $async );
		$running_op = true;

		$query_args          = $this->wc_subscription_query_default_args;
		$query_args['paged'] = $this->opmeta( 'query_page' );

		if ( !empty( $this->config( 'include_statuses' ) ) ) {
			$query_args['subscription_status'] = (array) $this->config( 'include_statuses' );
		}

		while ( $running_op ) {
			// Get Subscriptions 
			$query_args['paged'] = $this->opmeta( 'query_page' );
			$results             = wcs_get_subscriptions( $query_args );

			if ( count( $results ) ) {
				foreach ( $results as $subscription ) {
					$subscription_updated                         	= false;
					$subscription_values                          	= $this->export_csv_fields; // Re-init subscription level CSV values as all blank
					$subscription_related_orders 					= $subscription->get_related_orders( 'all', array( 'any', 'shipment' ) );
					$subscription_related_order_months				= [];

					$subscription_next_shipment_date 				= $subscription->get_date( 'next_shipment', 'site' );
					$subscription_last_payment_date 				= $subscription->get_date( 'last_order_date_created', 'site' );
					$subscription_next_payment_date 				= $subscription->get_date( 'next_payment', 'site' );
					$subscription_last_shipment_order_date 			= $subscription->get_date( 'last_shipment_order', 'site' );
					$subscription_end_date 							= $subscription->get_date( 'end', 'site');

					$subscription_next_shipment_date_gmt 			= $subscription->get_date( 'next_shipment', 'gmt' );
					$subscription_last_payment_date_gmt 			= $subscription->get_date( 'last_order_date_created', 'gmt' );
					$subscription_next_payment_date_gmt 			= $subscription->get_date( 'next_payment', 'gmt' );
					$subscription_last_shipment_order_date_gmt 		= $subscription->get_date( 'last_shipment_order', 'gmt' );
					$subscription_end_date_gmt 						= $subscription->get_date( 'end', 'gmt');

					$subscription_billing_period 					= $subscription->get_billing_period();
					$subscription_billing_interval					= $subscription->get_billing_interval();


					$subscription_values['Subscription ID']       	= $subscription->get_id();
					$subscription_values['Status']                	= $subscription->get_status();
					$subscription_values['Email Address']			= $subscription->get_billing_email();
					$subscription_values['User ID']               	= $subscription->get_user_id();
					$subscription_values['Shipping Country']      	= $subscription->get_shipping_country();
					$subscription_values['Total Initial Payment'] 	= $subscription->get_total_initial_payment();
					$subscription_values['Subscription Total']    	= $subscription->get_total();
					$subscription_values['Billing Period']        	= $subscription_billing_period;
					$subscription_values['Billing Interval']      	= $subscription_billing_interval;
					

					$subscription_values['Date Created'] 			= $subscription->get_date( 'date_created' , 'site');
					$subscription_values['Last Payment']			= $subscription_last_payment_date;
					$subscription_values['Next Payment'] 			= $subscription_next_payment_date;
					$subscription_values['End Date']     			= $subscription_end_date;
					$subscription_values['Next Shipment']        	= $subscription_next_shipment_date;
					$subscription_values['Last Shipment']        	= $subscription_last_shipment_order_date;

					$subscription_values['GMT Date Created'] 			= $subscription->get_date( 'date_created' , 'gmt');
					$subscription_values['GMT Last Payment']			= $subscription_last_payment_date_gmt;
					$subscription_values['GMT Next Payment'] 			= $subscription_next_payment_date_gmt;
					$subscription_values['GMT End Date']     			= $subscription_end_date_gmt;
					$subscription_values['GMT Next Shipment']        	= $subscription_next_shipment_date_gmt;
					$subscription_values['GMT Last Shipment']        	= $subscription_last_shipment_order_date_gmt;

					// Subscription level validation flags
					$subscription_order_items_check 				= array(); // This will become an array of order item types where [key] = order item type AND [value] = true (if that order item is found)


					$order_items                                  = $subscription->get_items( $this->order_item_types );

					foreach ( $order_items as $order_item ) {
						$order_item_values = $subscription_values;
						$subscription_order_items_check[ $order_item->get_type() ] = true;

						/**
						 *  VERIFY LINE ITEM
						 */
						if ( $order_item->get_type() === 'line_item' ) {
							$variation_id    = $order_item->get_variation_id();
							$item_product_id = $order_item->get_product_id();
							$parent_product  = wc_get_product( $item_product_id );

							$order_item_values['Order Item Type']  = 'line_item';
							$order_item_values['Order Item Total'] = $order_item->get_total();
							$order_item_values['Parent SKU']       = $parent_product->get_sku();
							$order_item_values['Quantity']         = $order_item->get_quantity();
							$order_item_values['Subtotal']         = $order_item->get_subtotal();

							

							// Check If parent ID invalid
							if ( !in_array( $item_product_id, $this->valid_product_ids ) ) {
								$this->stat( 'variation_invalid' );
								array_push( $order_item_values['Error Codes'], 'variation_invalid' );
							}

							// Check if subscription is connected to a parent product
							else if ( empty( $variation_id ) ) {
								$this->stat( 'line_item_is_parent' );
								array_push( $order_item_values['Error Codes'], 'line_item_is_parent' );
								$new_variation_id = $this->get_expected_subscription_variation( $subscription, $order_item );

								if ( empty( $new_variation_id ) ) {
									$this->stat( 'variation_not_found' );
									array_push( $order_item_values['Error Codes'], 'variation_not_found' );
								} else {
									$new_variation                                = wc_get_product( $new_variation_id );
									$order_item_values['Updated Variation ID']    = $new_variation->get_id();
									$order_item_values['Updated Variation SKU']   = $new_variation->get_sku();
									$order_item_values['Updated Variation Price'] = $new_variation->get_price();
								}

								// Subscription Order Items do writes
								$order_item_values['Updates']['parent_line_item_fixed'] = $new_variation_id;
								if ( !$this->config( 'dry_run_val' ) ) {
									$subscription->add_product( $new_variation, $order_item->get_quantity() );
									$subscription->remove_item( $order_item->get_id() );
									$subscription->add_order_note( "Order item changed from parent to variation by " . implode( ',', $order_item_values['Error Codes'] ) . " - Operation ID: " . $this->ID . "." );
									$subscription_updated = true;
								}

							} else {

								$variation                          = wc_get_product( $variation_id );
								$order_item_values['Variation SKU'] = $variation->get_sku();

								// Check if line_item is not of type Product Variation
								if ( !is_a( $variation, 'WC_Product_Variation' ) ) {
									$this->stat( 'line_item_not_variation' );
									array_push( $order_item_values['Error Codes'], 'line_item_not_variation' );
								} else {
									$variation_product_id = $variation->get_parent_id();
									// Check if line item IDs mismatch
									if ( $variation_product_id != $item_product_id ) {
										$this->stat( 'line_item_ids_mismatch' );
										array_push( $order_item_values['Error Codes'], 'line_item_ids_mismatch' );
									}

									// Item Price
									$item_price                           = $order_item->get_subtotal() / $order_item->get_quantity();
									$variation_price					  = $variation->get_price();
									$order_item_values['Variation Price'] = $variation_price;
									$order_item_values['Line Item Price'] = $item_price;

									// Check if line item price is different than current offerings
									if ( (float) $item_price != (float) $variation_price ) {
										$this->stat( 'item_mismatch_current_pricing' );
										array_push( $order_item_values['Error Codes'], 'item_mismatch_current_pricing' );
										$new_price = $variation_price *  $order_item->get_quantity();

										$order_item_values['Updates']['variation_price_fixed'] = $new_price;
										/* 
											DON'T ALLOW AUTO-FIXING OF VARIATION PRICES BECAUSE OF VALID DISCOUNT CODES SUCH AS FAMILY20
										if ( !$this->config( 'dry_run_val' ) ) {
											$order_item->set_total( $new_price );
											$subscription->add_order_note( "Fixed line item price for {$variation->get_title()} from {$item_price} to {$variation_price} during ONYX OP {$this->ID}" );
											$subscription_updated = true;
										} */
									}
								}
							}
						}

						/**
						 * Verify Shipping Item
						 */
						if ( $order_item->get_type() === 'shipping' ) {
							$order_item_values['Order Item Type']  = 'shipping';
							$order_item_values['Order Item Total'] = $order_item->get_total();
							if ( strcmp( $order_item_values['Shipping Country'], 'US' ) === 0 ) {
								if ( $order_item->get_total() > 0 ) {
									$this->stat( 'us_delivery_not_free' );
									array_push( $order_item_values['Error Codes'], 'us_delivery_not_free' );


									$subscription_values['Updates']['free_shipping_added'] = true;
									if ( !$this->config( 'dry_run_val' ) ) {
										$order_item->set_total( 0 );
										$order_item->set_method_title( 'Free Shipping' );
										$order_item->save();
										$subscription_updated = true;
										$subscription->add_order_note( "Subscription changed to 'Free Shipping' by " . implode( ',', $order_item_values['Error Codes'] ) . " - Operation ID: " . $this->ID . "." );
									}
								}
							}
						}

						/**
						 * Verify
						 */

						/**
						 * Print Invalid Order item errors
						 */
						if ( !empty( $order_item_values['Error Codes'] ) ) {
							$order_item_values['Error Codes'] = implode( ' ', $order_item_values['Error Codes'] );

							$order_item_values['Updates'] = $this->format_updates_value( $order_item_values['Updates'] );
							$this->write_csv_line_from_array( 'export_csv', $order_item_values );
						}

						
					}

					/**
					 * Subscription check if multiple shipments. Currently only works for subscriptions with single order
					 */
					foreach ($subscription_related_orders as $related_order ) {
						$order_fulfillment_month = get_post_meta( $related_order->get_id(), '_fulfillment_month', true );
						array_push($subscription_related_order_months , $order_fulfillment_month);
					}


					/**
					 * Subscription level validation
					 */

					// Create copy of subscription values to modify for subscription CSV row (not the order items)
					$subscription_validation_row = $subscription_values;
					
					$subscription_validation_row['Order Item Type'] = 'subscription';

					if (  '1' == $subscription_billing_interval 
						&& 'month' == $subscription_billing_period
						&& !empty( $subscription_next_shipment_date ) 
					) {
						$this->stat( 'subscription_1_mo_has_next_shipment' );
						array_push( $subscription_validation_row['Error Codes'], 'subscription_1_mo_has_next_shipment');

						$subscription_validation_row['Updates']['next_shipment_date_removed'] = true;
						if( !$this->config( 'dry_run_val' ) ) {
							
							$subscription->update_dates( array( 'next_shipment' => 0 ) );
						}
					}	

					if( empty( $subscription_order_items_check ) ) {
						$this->stat( 'subscription_no_items' );
						array_push( $subscription_validation_row['Error Codes'], 'subscription_no_items' );
					}
					
					if( empty( $subscription_order_items_check['line_item'] ) ) {
						$this->stat( 'subscription_no_line_items' );
						array_push( $subscription_validation_row['Error Codes'], 'subscription_no_line_items' );
					}
					
					if( empty( $subscription_order_items_check['shipping'] ) ) {
						$this->stat( 'subscription_has_no_shipping_line_item' );
						array_push( $subscription_validation_row['Error Codes'], 'subscription_has_no_shipping_line_item' );
					}
					
					
					if ( empty( $subscription_end_date ) && empty( $subscription_next_shipment_date ) && empty( $subscription_next_payment_date ) ) {
						$this->stat('subscription_missing_necessary_dates');
						array_push( $subscription_validation_row['Error Codes'], 'subscription_missing_necessary_dates' );
					}

					
					if ( count($subscription_related_order_months) != count(array_flip($subscription_related_order_months))  ) {
						$this->stat( 'subscription_too_many_shipments' );
						array_push( $subscription_validation_row['Error Codes'], 'too_many_shipments' );

					} 

					// Validate and fix (calculated) next_shipment date
					if( empty( $subscription_validation_row['Updates']['next_shipment_date_removed'] ) ) {

						$expected_next_shipment_date_gmt = $subscription->calculate_date( 'next_shipment' );

						if( $expected_next_shipment_date_gmt != $subscription_next_shipment_date_gmt ) {

							$this->stat( 'subscription_unexpected_next_shipment_date' );
							array_push( $subscription_validation_row['Error Codes'], 'subscription_unexpected_next_shipment_date' );
	
							$subscription_validation_row['Updates']['update_next_shipment_date'] = $expected_next_shipment_date_gmt;
	
							if( !$this->config( 'dry_run_val' ) ) {
	
								$subscription->update_dates( array( 'next_shipment' => $expected_next_shipment_date_gmt ) );
							}
						}
					}
					
					// Validate and fix (calculated) next_payment date
					$expected_next_payment_date_gmt = $subscription->calculate_date( 'next_payment' );
					if( $expected_next_payment_date_gmt != $subscription_next_payment_date_gmt ) {

						$expected_time_diff = strtotime( $expected_next_payment_date_gmt ) - strtotime( $subscription_next_payment_date_gmt );

						/**
						 * DO IF:
						 *  - 0 == expected next_payment date
						 * 	- OR ( expected next payment date is not in the past
						 *  - 		AND ( difference between expected and actual date is greater than 75 hours
						 *  - 			OR expected and current next_payment dates are in different months 
						 * 	- 			OR ( current next_payment date is not empty
						 * 	-				AND current next_payment date is first or last day of the month 
						 *  - 			)
						 * 	-		)
						 * 	- 	)	
						 */	
						// Skip if:

						// 60 second tolerance window until next_payment date considered "unexpected"
						// Also skip if 
						if( empty( $expected_next_payment_date_gmt )
							OR ( strtotime( $expected_next_payment_date_gmt ) > time() 
								&& ( abs( $expected_time_diff ) > 270000
									|| 0 != OGK_Subscription_Dates::fulfillment_months_date_diff( $expected_next_payment_date_gmt, $subscription_next_payment_date_gmt, 'gmt' )	
									|| ( !empty( $subscription_next_payment_date )
										&& OGK_Subscription_Dates::is_date_first_or_last_day_of_month( $subscription_next_payment_date_gmt, 'gmt' ) 
										)
								)
							)
						) {
							$this->stat( 'subscription_unexpected_next_payment_date' );
							array_push( $subscription_validation_row['Error Codes'], 'subscription_unexpected_next_payment_date' );

							$subscription_validation_row['Updates']['update_next_payment_date'] = $expected_next_payment_date_gmt;

							// Skip if expected next payment date is after subscription (1 day before) end_date
							$end_time = $subscription->get_time( 'end' );
							if(  0 != $end_time && ( strtotime( $expected_next_payment_date_gmt ) + ( 23 * HOUR_IN_SECONDS ) ) > $end_time ) {
								$this->stat( 'subscription_unexpected_next_payment_date_after_end' );
								array_push( $subscription_validation_row['Error Codes'], 'subscription_unexpected_next_payment_date_after_end' );
								$subscription_validation_row['Updates']['SKIPPING_update_next_payment_date'] = 'after_end_date';
							} 
							else {

								// Protect from drastic payment date changes (that could be false positives on subscriptions where the payment date was changed manually)
								if( strtotime( $subscription_next_payment_date_gmt ) > strtotime( $expected_next_payment_date_gmt )
									&& abs( $expected_time_diff ) > 180000 						// Only worry about this is if the time diff is greater than 50 hours
								) {
									$this->stat( 'subscription_unexpected_next_payment_date_future' );
									array_push( $subscription_validation_row['Error Codes'], 'subscription_unexpected_next_payment_date_future' );
									$subscription_validation_row['Updates']['SKIPPING_update_next_payment_date'] = abs( $expected_time_diff ) . ' seconds';
								}
								else if( !$this->config( 'dry_run_val' ) ) {

									$subscription->update_dates( array( 'next_payment' => $expected_next_payment_date_gmt ) );
								}
							}	
						}
					}



					/**
					 * Print Subscription level errors
					 */

					if ( !empty( $subscription_validation_row['Error Codes'] ) ) {
						$subscription_validation_row['Error Codes'] = implode(' ', $subscription_validation_row['Error Codes']);
						$subscription_validation_row['Updates'] = $this->format_updates_value( $subscription_validation_row['Updates'] );
						$this->write_csv_line_from_array('export_csv', $subscription_validation_row);	
					}
					

					/**
					 * Add note to updated subscription
					 */
					if ( $subscription_updated && !$this->config( 'dry_run_val' ) ) {
						$subscription->calculate_totals();
						$subscription->add_order_note( "Totals recalculated by Operation ID:" . $this->ID . "." );
						$this->stat( 'line_items_updated' );
					}



					$this->stat( 'subscriptions_validated' );
				}
			} else {
				$running_op = false;
				break;
			}

			$current_page = $this->opmeta( 'query_page' );
			if ( 0 == $current_page % 5 ) {
				$this->log( "Subscriptions Validated: " . $this->get_stat( 'subscriptions_validated' ) );
			}
			$next_page = $this->opmeta( 'query_page' ) + 1;
			$this->opmeta( 'query_page', $next_page );
			$this->maybe_jump_or_abort();
		}

		$this->successful_finish();
	}

	// get_expected_subscription_variation // wc_variation || false
	public function get_expected_subscription_variation( $subscription, $order_item ) {
		$billing_period       = $subscription->get_billing_period();
		$billing_interval     = $subscription->get_billing_interval();
		$parent_product       = $order_item->get_product();
		$available_variations = $parent_product->get_available_variations();
		$current_sku          = $parent_product->get_sku();
		$is_sibling           = strpos( $current_sku, 'SIB' );
		foreach ( $available_variations as $variation ) {
			$subscription_period   = get_post_meta( $variation['variation_id'], '_subscription_period', true );
			$subscription_interval = get_post_meta( $variation['variation_id'], '_subscription_period_interval', true );
			if ( strcmp( $billing_period, $subscription_period ) === 0 && strcmp( $billing_interval, $subscription_interval ) === 0 ) {
				$is_variation_sibling = strpos( $variation['sku'], 'SIB' );
				if ( $is_sibling === $is_variation_sibling ) {
					return $variation['variation_id'];
				}
			}
		}
		return false;
	}

	public function format_updates_value( array $updates ) {
		
		$formatted_updates = array();

		foreach( $updates as $key => $update ) {

			$formatted_update = $key;

			if( !empty( $update ) && true !== $update ) {
				$formatted_update .= ':' . $update;
			}

			$formatted_updates[] = $formatted_update;
		}

		return implode( '|', $formatted_updates );
	}
}
