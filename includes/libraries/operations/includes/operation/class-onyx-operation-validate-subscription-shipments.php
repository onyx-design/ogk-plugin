<?php
defined( 'ABSPATH' ) || exit;

class ONYX_Operation_Validate_Subscription_Shipments extends ONYX_Operation_Base {

	public static $optype = 'validate_subscription_shipments';

	public static $jump_interval = 45;

	protected $wc_subscription_query_default_args = array(
		'subscriptions_per_page' => 40,
		'paged'                  => 1,
		'offset'                 => null,
		'orderby'                => 'ID',
		'order'                  => 'ASC',
		'customer_id'            => 0,
		'product_id'             => 0,
		'variation_id'           => 0,
		'order_id'               => 0,
		'subscription_status'    => array( 'any' ),
		'meta_query_relation'    => 'AND'
	);

	protected $export_csv_fields = array(
		'Subscription ID'         => '',
		'Status'                  => '',
		'User ID'                 => '',
		'Shipping Country'        => '',
		

		'Billing Period'          => '',
		'Billing Interval'        => '',

		'Target Shipments'	=> '',
		'Total Shipments'	=> '',
		'Interval Valid'		=> '',
		'Reason'			=> '',
		'Shipments Match'		=> '',
		
		'Start Order ID'		=> '',
		'Interval Start Date'	=> '',

		'Interval End Date'		=> '',
		'Next Interval Order ID'=> '',

		
		
		'Interval Complete'		=> '',
		'Interval Active'		=> ''
		
	);



	public function extra_init() {
		$this->init_stat( 'subscriptions_scanned', 'Subscriptions Scanned' );
		$this->init_stat( 'subscriptions_validated', 'Subscriptions Validated' );
		$this->init_stat( 'subscriptions_skipped', 'Subscriptions Skipped (pre May 2020)' );
		
		$this->init_stat( 'valid_prepaid_intervals', 'Valid Prepaid Intervals' );
		$this->init_stat( 'invalid_prepaid_intervals', 'Invalid Prepaid Intervals' );
		$this->init_stat( 'prepaid_intervals_checked', 'Total Prepaid Intervals Checked' );


		$this->opmeta( 'query_page', 1 );
		$this->log( "Subscription Prepaid Shipments Validation Initialized" );
		// $this->log( "exporting subscription statuses: " . $this->config( 'include_statuses' ) );

		$this->new_export_file( 'export_csv' );
		$this->write_csv_line_from_array( 'export_csv', array_keys( $this->export_csv_fields ) );
	}

	public function run( $async = true ) {
		parent::run( $async );
		$running_op = true;

		$query_args          = $this->wc_subscription_query_default_args;
		$query_args['paged'] = $this->opmeta( 'query_page' );

		// if ( !empty( $this->config( 'include_statuses' ) ) ) {
		// 	$query_args['subscription_status'] = (array) $this->config( 'include_statuses' );
		// }

		while ( $running_op ) {
			// Get Subscriptions 
			$query_args['paged'] = $this->opmeta( 'query_page' );
			$results             = wcs_get_subscriptions( $query_args );

			if ( count( $results ) ) {
				foreach ( $results as $subscription ) {
					$this->stat( 'subscriptions_scanned' );

					if( $subscription->get_date( 'date_created' ) < '2020-05-01 00:00:00' 
						&& ( !empty( $subscription->get_date( 'end' ) ) && $subscription->get_date( 'end' ) < '2020-05-01 00:00:00' )
					) {
						$this->stat( 'subscriptions_skipped' );
						continue;
					}

					
					


					$prepaid_intervals = new OGK_Subscription_Prepaid_Intervals( $subscription );

					if( $prepaid_intervals->intervals ) {

						$subscription_values                          = $this->export_csv_fields;
						$subscription_values['Subscription ID']       = $subscription->get_id();
						$subscription_values['Status']                = $subscription->get_status();
						$subscription_values['User ID']               = $subscription->get_user_id();
						$subscription_values['Shipping Country']      = $subscription->get_shipping_country();
						$subscription_values['Billing Period']        = $subscription->get_billing_period();
						$subscription_values['Billing Interval']      = $subscription->get_billing_interval();


						foreach( $prepaid_intervals->intervals as $interval ) {

							$this->stat( 'prepaid_intervals_checked' );

							if( $interval['is_valid'] ) {
								
								$this->stat( 'valid_prepaid_intervals' );
							}
							else {
								$this->stat( 'invalid_prepaid_intervals' );
							}
							
							if( !$interval['is_valid'] || $this->config( 'output_valid_intervals' ) ) {

								$prepaid_interval_values = $subscription_values;

								$prepaid_interval_values[ 'Target Shipments' ]	= $interval['target_shipments'];
								$prepaid_interval_values[ 'Total Shipments' ]	= $interval['total_shipments'];
								$prepaid_interval_values[ 'Interval Valid' ]	= strtoupper( boolstr( $interval['is_valid'] ) );
								$prepaid_interval_values[ 'Reason' ]	= $interval['reason'];
								$prepaid_interval_values[ 'Shipments Match' ]	= strtoupper( boolstr( $interval['shipments_match'] ) );
								$prepaid_interval_values[ 'Start Order ID' ]	= $interval['start_order']['order_id'];
								$prepaid_interval_values[ 'Interval Start Date' ]	= $interval['start_date'];
								$prepaid_interval_values[ 'Interval End Date' ]	= $interval['end_date'];
								$prepaid_interval_values[ 'Next Interval Order ID' ]	= $interval['end_order'] ? $interval['end_order']['order_id'] : '';
								$prepaid_interval_values[ 'Interval Complete' ]	= strtoupper( boolstr( $interval['is_complete'] ) );
								$prepaid_interval_values[ 'Interval Active' ]	= strtoupper( boolstr( $interval['is_active'] ) );
								

								$this->write_csv_line_from_array( 'export_csv', $prepaid_interval_values );

							}
						


						}

					}

			

					$this->stat( 'subscriptions_validated' );
				}
			} else {
				$running_op = false;
				break;
			}

			$current_page = $this->opmeta( 'query_page' );
			if ( 0 == $current_page % 5 ) {
				$this->log( "Subscriptions Validated: " . $this->get_stat( 'subscriptions_scanned' ) );
			}
			$next_page = $this->opmeta( 'query_page' ) + 1;
			$this->opmeta( 'query_page', $next_page );

			wp_cache_flush();

			$this->maybe_jump_or_abort();
		}

		$this->successful_finish();
	}

}
