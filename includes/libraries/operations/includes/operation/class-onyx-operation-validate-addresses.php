<?php
defined( 'ABSPATH' ) || exit;

class ONYX_Operation_Validate_Addresses extends ONYX_Operation_Base {

	public static $optype = 'validate_addresses';

	public static $jump_interval = 45;

	protected $user_address_data = array(
		'User Shipping First Name' => '',
		'User Shipping Last Name'  => '',
		'User Shipping Address 1'  => '',
		'User Shipping Address 2'  => '',
		'User Shipping City'       => '',
		'User Shipping State'      => '',
		'User Shipping Country'    => '',
	);
	protected $subscription_address_data = array(
		'Subscription Shipping First Name' => '',
		'Subscription Shipping Last Name'  => '',
		'Subscription Shipping Address 1'  => '',
		'Subscription Shipping Address 2'  => '',
		'Subscription Shipping City'       => '',
		'Subscription Shipping State'      => '',
		'Subscription Shipping Country'    => '',
	);

	protected $export_csv_fields = array(
		'Subscription ID'                  => '',
		'Status'                           => '',
		'Is Gift?'                         => '',
		'User ID'                          => '',
		'Number Of Subscriptions'          => '',
		'Date Created'                     => '',
		'Next Payment'                     => '',
		'End Date'                         => '',
		'Next Shipment'                    => '',
		'Last Shipment'                    => '',
		'User Shipping First Name'         => '',
		'User Shipping Last Name'          => '',
		'User Shipping Address 1'          => '',
		'User Shipping Address 2'          => '',
		'User Shipping City'               => '',
		'User Shipping State'              => '',
		'User Shipping Country'            => '',
		'Subscription Shipping First Name' => '',
		'Subscription Shipping Last Name'  => '',
		'Subscription Shipping Address 1'  => '',
		'Subscription Shipping Address 2'  => '',
		'Subscription Shipping City'       => '',
		'Subscription Shipping State'      => '',
		'Subscription Shipping Country'    => '',
		'User Billing First Name'          => '',
		'User Billing Last Name'           => '',
		'User Billing Address 1'           => '',
		'User Billing Address 2'           => '',
		'User Billing City'                => '',
		'User Billing State'               => '',
		'User Billing Country'             => '',
		'Subscription Billing First Name'  => '',
		'Subscription Billing Last Name'   => '',
		'Subscription Billing Address 1'   => '',
		'Subscription Billing Address 2'   => '',
		'Subscription Billing City'        => '',
		'Subscription Billing State'       => '',
		'Subscription Billing Country'     => '',
		'Differences'                      => '',
	);

	protected $wc_subscription_query_default_args = array(
		'subscriptions_per_page' => 20,
		'paged'                  => 1,
		'offset'                 => null,
		'orderby'                => 'ID',
		'order'                  => 'ASC',
		'customer_id'            => 0,
		'product_id'             => 0,
		'variation_id'           => 0,
		'order_id'               => 0,
		'subscription_status'    => array( 'wc-active', 'wc-pending-cancel' ),
		'meta_query_relation'    => 'AND',
	);

	public function extra_init() {

		$this->init_stat( 'addresses_validated', 'Addresses Validated' );

		$this->opmeta( 'query_page', 1 );

		$this->log( "Address Validation Initialized" );
		$this->log( "validating addresses for subscription status: active, pending-cancel" );

		$this->new_export_file( 'export_csv' );

		$this->write_csv_line_from_array( 'export_csv', array_keys( $this->export_csv_fields ) );
	}

	public function run( $async = true ) {

		parent::run( $async );
		$running_op       = true;
		$outputted_totals = false;

		$query_args          = $this->wc_subscription_query_default_args;
		$query_args['paged'] = $this->opmeta( 'query_page' );

		while ( $running_op ) {

			// Get Orders
			$query_args['paged'] = $this->opmeta( 'query_page' );
			$results             = wcs_get_subscriptions( $query_args );

			if ( count( $results ) ) {
				foreach ( $results as $subscription ) {
					$subscription_values       = $this->export_csv_fields;
					$user_address_info         = $this->user_address_data;
					$subscription_address_info = $this->subscription_address_data;

					$subscription_values['Subscription ID'] = $subscription->get_id();
					$subscription_values['Status']          = $subscription->get_status();
					$subscription_values['Is Gift?']        = empty( $subscription->get_date( 'next_payment' ) ) ? 'No' : 'Yes';
					$subscription_values['Date Created']    = $subscription->get_date( 'date_created' );
					$subscription_values['Next Payment']    = $subscription->get_date( 'next_payment' );
					$subscription_values['End Date']        = $subscription->get_date( 'end' );
					$subscription_values['Next Shipment']   = $subscription->get_date( 'next_shipment' );
					$subscription_values['Last Shipment']   = $subscription->get_date( 'last_shipment_order' );
					$user_id                                = $subscription->get_user_id();

					$subscription_values['User ID'] = $user_id;

					$subscription_values['User Shipping First Name'] = get_user_meta( $user_id, "shipping_first_name", true );
					$subscription_values['User Shipping Last Name']  = get_user_meta( $user_id, "shipping_last_name", true );
					$subscription_values['User Shipping Address 1']  = get_user_meta( $user_id, "shipping_address_1", true );
					$subscription_values['User Shipping Address 2']  = get_user_meta( $user_id, "shipping_address_2", true );
					$subscription_values['User Shipping City']       = get_user_meta( $user_id, "shipping_city", true );
					$subscription_values['User Shipping State']      = get_user_meta( $user_id, "shipping_state", true );
					$subscription_values['User Shipping Country']    = get_user_meta( $user_id, "shipping_country", true );

					$subscription_values['Subscription Shipping First Name'] = $subscription->get_shipping_first_name();
					$subscription_values['Subscription Shipping Last Name']  = $subscription->get_shipping_last_name();
					$subscription_values['Subscription Shipping Address 1']  = $subscription->get_shipping_address_1();
					$subscription_values['Subscription Shipping Address 2']  = $subscription->get_shipping_address_2();
					$subscription_values['Subscription Shipping City']       = $subscription->get_shipping_city();
					$subscription_values['Subscription Shipping State']      = $subscription->get_shipping_state();
					$subscription_values['Subscription Shipping Country']    = $subscription->get_shipping_country();

					$user_address_info         = array_intersect_key( $subscription_values, $user_address_info );
					$subscription_address_info = array_intersect_key( $subscription_values, $subscription_address_info );

					// Strip keys
					$user_address_keys = array_map( function ( $key ) {
						return str_replace( "User ", "", $key );
					}, array_keys( $user_address_info ) );

					$user_address_vals = array_map( 'trim', $user_address_info );
					$user_address_vals = array_map( 'strtolower', $user_address_vals );
					$user_address_info = array_combine( $user_address_keys, $user_address_vals );

					$subscription_address_keys = array_map( function ( $key ) {
						return str_replace( "Subscription ", "", $key );
					}, array_keys( $subscription_address_info ) );

					$subscription_address_vals = array_map( 'trim', $subscription_address_info );
					$subscription_address_vals = array_map( 'strtolower', $subscription_address_vals );
					$subscription_address_info = array_combine( $subscription_address_keys, $subscription_address_vals );

					$diff = array_diff_assoc( $user_address_info, $subscription_address_info );

					if ( !empty( $diff ) ) {
						// Add user & subscription billing info before adding to subscription_values
						$subscription_values['User Billing First Name']         = get_user_meta( $user_id, "billing_first_name", true );
						$subscription_values['User Billing Last Name']          = get_user_meta( $user_id, "billing_last_name", true );
						$subscription_values['User Billing Address 1']          = get_user_meta( $user_id, "billing_address_1", true );
						$subscription_values['User Billing Address 2']          = get_user_meta( $user_id, "billing_address_2", true );
						$subscription_values['User Billing City']               = get_user_meta( $user_id, "billing_city", true );
						$subscription_values['User Billing State']              = get_user_meta( $user_id, "billing_state", true );
						$subscription_values['User Billing Country']            = get_user_meta( $user_id, "billing_country", true );
						$subscription_values['Subscription Billing First Name'] = $subscription->get_billing_first_name();
						$subscription_values['Subscription Billing Last Name']  = $subscription->get_billing_last_name();
						$subscription_values['Subscription Billing Address 1']  = $subscription->get_billing_address_1();
						$subscription_values['Subscription Billing Address 2']  = $subscription->get_billing_address_2();
						$subscription_values['Subscription Billing City']       = $subscription->get_billing_city();
						$subscription_values['Subscription Billing State']      = $subscription->get_billing_state();
						$subscription_values['Subscription Billing Country']    = $subscription->get_billing_country();

						$user_subscriptions = wcs_get_users_subscriptions( $user_id );
						$user_sub_count     = 0;

						foreach ( $user_subscriptions as $user_sub ) {
							$status = $user_sub->get_status();
							if ( $status === "active" || $status === "pending-cancel" ) {
								$user_sub_count++;
							}
						}

						$subscription_values['Number Of Subscriptions'] = count( wcs_get_users_subscriptions( $user_id ) );
						$subscription_values['Differences']             = strtolower( implode( " ", str_replace( " ", "_", array_keys( $diff ) ) ) );
						$this->write_csv_line_from_array( 'export_csv', $subscription_values );
					}

					$this->stat( 'addresses_validated' );
				}
			} else {
				$running_op = false;
				break;
			}
			$current_page = $this->opmeta( 'query_page' );

			if ( 0 == $current_page % 5 ) {
				$this->log( "Addresses Validated: " . $this->get_stat( 'addresses_validated' ) );
			}

			$next_page = $this->opmeta( 'query_page' ) + 1;
			$this->opmeta( 'query_page', $next_page );

			$this->maybe_jump_or_abort();
		}

		$this->successful_finish();
	}
}
