<?php
defined( 'ABSPATH' ) || exit;

class ONYX_Operation_Base {

	public $ID = 0;
	public $status;

	// public $files = array();
	// protected $op_file_name;
	// protected $log_file_name;

	public $op_data = array(
		'ID'        => null,
		'type'      => null,
		'status'    => null,
		'start'     => null,
		'end'       => null,
		'last_seen' => null,
		'user_id'   => null,
		'op_file'   => null,
		'log_file'  => null,
		'config'    => array(),
		'files'     => array(),
		'meta'      => array(),
		'data'      => array(),
		'stats'     => array(),
	);

	public static $optype  = null;
	public static $ops_dir = ONYX_CONTENT_DIR . '/ops';

	// public static $log_dir = ONYX_CONTENT_DIR . '/log';

	public $request;
	public $loaded            = false;
	public $jumped            = false;
	public $response_data     = array();
	public $use_log_prefix    = false;
	public $last_status_check = false;

	public static $request_op;
	public static $jump_interval = 50;

	// OP TYPES
	public static $valid_op_types = array(

        'export_subscriptions',
		'export_orders',
        'export_sub_line_items',
		'validate_subscriptions',
		'validate_subscription_shipments',
		'send_may_shipping_emails',
		'generate_missed_shipments',
		'validate_addresses',
		'export_blog_posts',
		'update_subscription_dates_feb_2021',
		'fix_db_issues_202206',
		'export_subscriptions_snapshot'
	);

	// OP STATUSES
	public static $active_statuses = array(
		'init', // Opertation has been initialized but not commenced
		'running', // Operation is running (only one operation can be running at any given moment)
		'jumping', // Operation is currently jumping (if no error has occured, the operation is still in progress)
	);

	public static $inactive_statuses = array(
		'complete', // Operation completed successfully
		'cancelled', // Operation was cancelled while it was queued (cancelled before it began)
		'aborted', // Operation aborted itself due to error
		'user_aborted', // Operation was aborted by user
		'reset', // Operation status was forcefully reset
		'aborted_after_reset', // Operation detected that it was reset and aborted itself
		'failed', // Operation failed initial validation
	);

	public function __construct( $ID ) {

		$this->ID = $ID;
		$this->load_op();
	}

	public function run( $async = true ) {

		if ( $async ) {
			session_write_close(); // Allows for asynchronous execution and db calls
		}

		if ( !isset( $this->op_data['start'] ) ) {
			$this->op_data['start'] = current_time( 'mysql', 1 );
		}

		self::$request_op = $this;

		$this->set_status( 'running' );

		define( 'ONYX_RUNNING_OP', true );
	}

	public static function get_op( $ID, $optype ) {

		if ( !( $classname = self::get_op_class( $optype ) ) ) {
			return false;
		}

		$op = new $classname( $ID );

		return $op;
	}

	public static function init_op( $optype, $config = array() ) {

		if ( !self::check_op_dirs() ) {
			onyx_dev_log( 'FAILED TO INITIALIZE OPERATION - CHECK_OP_DIRS FAILED' );
			return false;
		} else if ( empty( $optype ) || !in_array( $optype, self::$valid_op_types ) ) {
			onyx_dev_log( 'FAILED TO INITIALIZE OPERATION - INVALID OPTYPE' );
			return false;
		} else if ( !( $classname = self::get_op_class( $optype ) ) ) {
			onyx_dev_log( 'FAILED TO INITIALIZED OPERATION - CLASS DOES NOT EXIST FOR optype: ' . $optype );
			return false;
		}

		$op_ID = date( 'Ymd_His' );

		$op_file  = self::generate_op_filename( 'json', $op_ID, $optype );
		$log_file = self::generate_op_filename( 'log', $op_ID, $optype );

		$op_data = array(
			'ID'        => $op_ID,
			'type'      => $optype,
			'status'    => 'init',
			'start'     => null,
			'end'       => null,
			'last_seen' => current_time( 'mysql', 1 ),
			'user_id'   => get_current_user_id(),
			'op_file'   => $op_file,
			'log_file'  => $log_file,
			'config'    => array_map( 'maybe_str_to_bool', $config ),
			'meta'      => array(),
			'data'      => array(),
			'stats'     => array(),
		);

		// Initialize op and log files
		self::write_file( $op_file, json_encode( $op_data, JSON_NUMERIC_CHECK ) );

		$log_header = 'OPERATION LOG - {Operation ID:' . $op_ID . ',Type:' . $optype . ',init_time: ' . current_time( 'mysql', 1 ) . '}';
		self::write_file( $log_file, $log_header );

		$op = new $classname( $op_ID );

		if ( method_exists( $op, 'extra_init' ) ) {

			$init_success = $op->extra_init();
		}

		if ( false === $init_success ) {
			$op->set_status( 'failed' );
			$op->log( 'Operation initialization failed.' );
			return false;
		}

		register_shutdown_function( array( $op, 'jump_op' ) );

		return $op_data;
	}

	// public function commence_op() {

	// 	session_write_close();  // Allows simultaneous asynchronous script execution and database calls

	// }

	public function jump_op() {

		session_write_close(); // Allows simultaneous asynchronous script execution and database calls
		$this->check_user_abort();

		$jump_post_data = array(

			'action'    => 'onyx_commence_op',
			'onyx_ajax' => array(
				'opID'   => $this->ID,
				'optype' => static::$optype,
			),
		);

		if ( $this->status == 'init' ) {
			$this->log( "Preparing for initialization jump." );
		} else {
			$this->log( "" );
			$this->set_status( 'jumping' );
			$this->log( 'Total Request time: ' . ox_exec_time() );
		}

		$this->write_op_file();

		$ch = curl_init();

		$curl_options = array(
			CURLOPT_URL            => admin_url( 'admin-ajax.php' ),
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => http_build_query( $jump_post_data ),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FRESH_CONNECT  => true,
			CURLOPT_TIMEOUT        => 3, // seconds
			CURLOPT_CONNECTTIMEOUT => 2,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_SSL_VERIFYHOST => false,
		);

		curl_setopt_array( $ch, $curl_options );

		$op_curl_exec = curl_exec( $ch );

		$op_curl_errno = curl_errno( $ch );
		$op_curl_error = curl_error( $ch );

		curl_close( $ch );

		if (
			'jumping' == $this->status
			&& false === $op_curl_exec
			// Make sure it's not a timeout as we're forcing timeouts
			 && 28 != $op_curl_errno
		) {
			$op_curl_info = curl_getinfo( $ch );

			$this->log( "Jumping operation has failed for OVCOP ID: " . $this->ID );
			$this->log( "Response from failed OP - Error Code: " . $op_curl_errno . " URL: " . $op_curl_info['url'] . " HTTP Code: " . $op_curl_info['http_code'] . " Total Time: " . $op_curl_info['total_time'] );

			// return false;
		}

		// return true;

		die();
	}

	public function abort_op( $msg = null, $user_abort = false, $suicide = true ) {

		$msg = $msg ?? 'OPERATION ABORTED';

		$this->use_log_prefix = false;
		$this->op_data['end'] = current_time( 'mysql', 1 );

		$new_status = $user_abort ? 'user_aborted' : 'aborted';
		$this->set_status( $new_status );

		$this->log( $msg );

		if ( $suicide ) {
			die();
		} else {
			return false;
		}
	}

	public function successful_finish() {
		$this->use_log_prefix = false;

		foreach ( array( 'before_successful_finish', 'generate_final_report', 'after_successful_finish' ) as $method ) {
			if ( method_exists( $this, $method ) ) {
				$this->$method();
			}
		}

		$this->op_data['end'] = current_time( 'mysql', 1 );
		$this->set_status( 'complete' );

		$op_duration = date_diff( date_create( current_time( 'mysql', 1 ) ), date_create( $this->op_data['start'] ) )->format( '%H:%I:%S' );
		$this->log();
		$this->log( 'Operation Complete. Duration: ' . $op_duration );
	}

	public function set_status( $new_status, $update_file = true ) {

		$current_status = $this->status;

		$this->status = $new_status;

		$this->log( "Status change: {$current_status} > {$new_status}" );

		if ( $update_file ) {
			$this->write_op_file();
		}
	}

	/**
	 * User abort set / check functions
	 */

	public function maybe_jump_or_abort( $force_jump = false ) {

		$this->check_user_abort();

		if ( $force_jump || static::$jump_interval < ox_exec_time() ) {

			$this->jump_op();
			return false;
		} else {
			return true;
		}
	}

	public static function request_user_abort( $ID ) {

		$abort_data = array(
			'ID'   => $ID,
			'time' => current_time( 'mysql', 1 ),
		);

		$abort_filename = $ID . '-abort.json';

		return self::write_file( $abort_filename, json_encode( $abort_data ) );
	}

	public function check_user_abort() {

		$abort_file = static::$ops_dir . '/' . $this->ID . '-abort.json';

		if ( file_exists( $abort_file ) ) {

			$this->write_op_file();
			$this->abort_op( "OPERATION ABORTED BY USER", true );
		}
	}

	/**
	 * Loading / Unloading operation
	 */
	public function load_op() {

		if ( $op_file = file_get_contents( static::$ops_dir . '/' . $this->get_op_filename() ) ) {

			if ( $this->op_data = json_decode( $op_file, true ) ) {

				foreach ( $this->op_data as $key => $value ) {
					if ( property_exists( $this, $key ) ) {

						$this->$key = $value;
					}
				}

				$this->loaded = true;

				return true;
			} else {
				onyx_dev_log( "ERROR LOADING OP - ERROR DECODING OP_FILE JSON FOR {$this->ID} - " . static::$optype );
			}
		} else {
			onyx_dev_log( "ERROR LOADING OP - COULD NOT LOAD OP_FILE FOR {$this->ID} - " . static::$optype );
		}

		return false;
	}

	protected function generate_final_report() {
		$this->log();
		$this->log();
		$this->log( static::$optype . " OPERATION COMPLETE =======" );

		foreach ( $this->op_data['stats'] as $stat ) {
			$this->log( str_pad( $stat['name'] . ": ", 70 ) . $stat['value'] );
		}

		$this->log();
		$this->log();
	}

	public function config( $key ) {

		return $this->op_data['config'][$key] ?? null;
	}

	public function opmeta( $key = null, $set_val = null, $save = false ) {

		if ( isset( $key ) ) {

			if ( !isset( $set_val ) ) {

				return $this->op_data['meta'][$key] ?? null;
			} else {

				$this->op_data['meta'][$key] = $set_val;

				if ( $save ) {

					$this->write_op_file();
				}
			}
		}
	}

	/*
	 * Stats functions
	 */
	// Initialize a stat
	public function init_stat( $key, $name ) {
		$this->op_data['stats'][$key] = array( 'name' => $name, 'value' => 0 );
	}

	public function set_stat( $key, $value ) {
		if ( isset( $this->op_data['stats'][$key] ) ) {
			$this->op_data['stats'][$key]['value'] = $value;
		}
	}

	public function get_stat( $key ) {
		return $this->op_data['stats'][$key]['value'] ?? null;
	}

	public function stat( $key, $add = 1 ) {
		if ( isset( $this->op_data['stats'][$key] ) ) {
			$this->op_data['stats'][$key]['value'] += $add;
		}
	}

	// Logging
	public function format_log_msg( $msg = '', $prefix = null ) {

		// Determine whether to prepend log prefix to $msg
		if (
			method_exists( $this, 'log_prefix' )
			&& (
				( isset( $prefix ) && $prefix )
				|| ( !isset( $prefix ) && $this->use_log_prefix ) )
		) {
			$msg = $this->log_prefix() . $msg;
		}

		// Wrap the message
		$msg = date( 'Y-m-d H:i:s' ) . ' - ' . $msg . "\r\n";

		return $msg;
	}

	public function log( $msg = '', $prefix = null ) {

		// $log_file_path = static::$ops_dir . '/' . $this->op_data['log_file'];

		return self::write_file( $this->op_data['log_file'], $this->format_log_msg( $msg, $prefix ), true );
	}

	protected function new_export_file( $file_id = '', $file_ext = 'csv' ) {

		$this->op_data['files'][$file_id] = $this->ID . '-' . $file_id . '.' . $file_ext;
	}

	protected function get_export_file_path( $file_id ) {

		return static::$ops_dir . '/' . $this->op_data['files'][$file_id];
	}

	public function csv_wrap_value( $value = '', $escape = true, $force_quotes = false ) {

		$value = (string) $value;

		if ( $escape ) {
			$value = str_replace( '"', "'", stripslashes( $value ) );
		}

		if ( $force_quotes || false !== strpos( $value, ',' ) ) {
			$value = '"' . $value . '"';
		}

		return $value;
	}

	public function csv_unwrap_value( $value = '' ) {
		return str_replace( "'", '"', trim( $value, '"' ) );
	}

	protected function write_csv_line_from_array( $file_id, $values = array() ) {

		$values = array_map( array( $this, 'csv_wrap_value' ), $values );

		$csv_line = implode( ',', $values ) . "\r\n";

		return file_put_contents( $this->get_export_file_path( $file_id ), $csv_line, FILE_APPEND );
	}

	protected function write_op_file() {

		// $op_file_path = static::$ops_dir . '/' . $this->op_data['op_file'];

		$op_data = $this->op_data;

		$op_data['status']    = $this->status;
		$op_data['last_seen'] = current_time( 'mysql', 1 );

		return self::write_file( $this->op_data['op_file'], json_encode( $op_data, JSON_NUMERIC_CHECK ) );
	}

	protected static function write_file( $filename, $data, $append = false ) {

		$filepath = static::$ops_dir . '/' . $filename;

		if ( $append ) {
			return file_put_contents( $filepath, $data, FILE_APPEND );
		} else {
			return file_put_contents( $filepath, $data );
		}
	}

	public static function check_op_dirs() {
		$success = true;

		if ( !file_exists( ONYX_CONTENT_DIR ) ) {

			if ( $success = mkdir( ONYX_CONTENT_DIR ) ) {

				if ( !file_exists( static::$ops_dir ) ) {

					$success = mkdir( static::$ops_dir );
				}
			}
		}

		return $success;
	}

	public function get_op_filename( $ID = null, $optype = null ) {

		$ID     = $ID ?? $this->ID;
		$optype = $optype ?? static::$optype;

		return $ID . '-' . $optype . '.json';
	}

	public function get_log_filename( $ID = null, $optype = null ) {

		$ID     = $ID ?? $this->ID;
		$optype = $optype ?? static::$optype;

		return $ID . '-' . $optype . '.log';
	}

	public static function generate_op_filename( $filetype, $ID, $optype ) {

		return "{$ID}-{$optype}.{$filetype}";
	}

	public static function get_op_class( $optype ) {

		$classname = 'ONYX_Operation_' . $optype;

		return class_exists( $classname ) ? $classname : false;
	}
}
