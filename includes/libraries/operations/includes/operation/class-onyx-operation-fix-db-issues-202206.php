<?php
defined( 'ABSPATH' ) || exit;

class ONYX_Operation_fix_db_issues_202206 extends ONYX_Operation_Base {

	public static $optype = 'fix_db_issues_202206';

	protected $src_tables_prefix = 'dbfixes202206_';

	public $target_tables_cache = array();

	protected $tbl_pkeys = array(
		'users' => 'ID',
		'usermeta' => 'umeta_id',
		'woocommerce_payment_tokens' => 'token_id',
		'woocommerce_payment_tokenmeta' => 'meta_id',
		'posts'	=> 'ID',
		'postmeta'	=> 'meta_id',
		'woocommerce_order_items' => 'order_item_id',
		'woocommerce_order_itemmeta' => 'meta_id',
		'comments' => 'comment_ID'
	);

	protected $tbl_foreign_keys = array(

	);
	
	protected $table_info_cache = array();
	protected $table_names_cache;

	protected $id_map = array(
	);

	protected $schema = array(
		'users' => array(
			'pKey'	=> 'ID'
		),
		'usermeta'	=> array(
			'pKey'	=> 'umeta_id'
		),
		'woocommerce_payment_tokens' => array(
			'pKey'	=> 'token_id'
		),
		'woocommerce_payment_tokenmeta'	=> array(
			'pKey'	=> 'meta_id'
		),
		'posts'	=> array(
			'pKey'	=> 'ID',
		),
		'postmeta'	=> array(
			'pKey'	=> 'meta_id'
		),
		'woocommerce_order_items' => array(

		),
		'woocommerce_order_itemmeta' => array(

		),
		'comments'	=> array(

		)
	);


	public function extra_init() {

		$this->opmeta('foreign_keys', array());

		if( $this->config( 'dry_run' ) ) {
			global $wpdb;
			$this->log( 'DRY RUN ACTIVE - DATA WILL BE INSERTED INTO STAGING TABLES' );
			$target_tables = $this->target_table();

			// $sql = '';

			foreach( $target_tables as $table_name => $target_table ) {

				// DROP IF EXISTS QUERY
				if( false === $wpdb->query( "DROP TABLE IF EXISTS {$target_table};" ) 
					|| false === $wpdb->query( "CREATE TABLE {$target_table} LIKE {$wpdb->prefix}{$table_name};" ) 
				) {
					$this->LOG( "ERROR INITIALIZING DRY RUN TARGET TABLES");
					return false;
				}
				// $sql .= "DROP TABLE IF EXISTS {$target_table};\r\nCREATE TABLE {$target_table} LIKE {$wpdb->prefix}{$table_name};\r\n";
			}

			$this->LOG( "SUCCESSFULLY INTIIALIZED DRY RUN TARGET TABLES: \r\n" . implode( "\r\n", array_values( $target_tables ) ) );
			// $this->LOG("DRY RUN SQL: \r\n\r\n{$sql}");

			// if( false !== $wpdb->query( $sql ) ) {
			// }
			// else {
			// 	$this->LOG( "ERROR INITIALIZING DRY RUN TARGET TABLES");
			// 	return false;
			// }

		}
	}

	// Request-level properties
	// public $target_ids = array();

	public function run( $async = true ) {
		
		parent::run( $async );
		global $wpdb;
		$running_op       = true;
		$outputted_totals = false;

		$this->migrate_table_data('users');

		$this->migrate_table_data('posts');

		$this->migrate_table_data('usermeta');

		$this->migrate_table_data('woocommerce_payment_tokens');

		$this->migrate_table_data('woocommerce_payment_tokenmeta');

		$this->migrate_table_data('postmeta');

		$this->migrate_table_data('woocommerce_order_items');
		$this->migrate_table_data('woocommerce_order_itemmeta');
		$this->migrate_table_data('comments');
		
		
		
		$this->successful_finish();

	}

	public function migrate_table_data( $table, $where_sql = '') {
		global $wpdb;

		$src_table = $this->src_table($table);
		$src_pkey = $this->get_table_pkey( $src_table );
		$target_table = $this->target_table($table);
		$target_pkey = $this->get_table_pkey( $target_table );
		$src_data = $wpdb->get_results( "SELECT * FROM {$src_table} {$where_sql}", ARRAY_A );
		if( !isset( $this->id_map[ $table ] ) ) {
			$this->id_map[ $table ] = array();
		}

		$pre_process_method = "pre_process_{$table}";
		if( method_exists( $this, $pre_process_method ) ) {
			$src_data = $this->$pre_process_method( $src_data );
		}

		foreach( $src_data as $src_row ) {

			$src_id = $src_row[ $src_pkey ];
			$insert_data = $src_row;
			unset( $insert_data[ $target_pkey ] );

			if( false === $wpdb->insert( $target_table, $insert_data ) ) {
				return $this->abort_op( "ERROR COPYING ROW FROM {$src_table} INTO {$target_table} - SRC ID: {$src_id}" );
			}
			else {
				$new_id = $wpdb->insert_id;
				
				$this->id_map[ $table ][$src_id] = $new_id;
			}

		}

		$post_process_method = "post_process_{$table}";
		if( method_exists( $this, $post_process_method ) ) {
			$src_data = $this->$post_process_method();
		}

		$this->log( "SUCCESSFULLY COPIED DATA FROM {$src_table} INTO {$target_table} - ID MAP: \r\n" . print_r( $this->id_map[$table], true ) );
	}

	// PRE / POST DATA PROCESSING

	public function pre_process_posts( $src_data ) {
		// Parse subscription IDs and Order IDs
		$this->id_map['subscription_ids'] = array();
		$this->id_map['order_ids'] = array();

		foreach( $src_data as $src_row ) {
			if( 'shop_order' == $src_row['post_type'] ) {
				$this->id_map['order_ids'][ $src_row['ID'] ] = true;
			}
			else if( 'shop_subscription' == $src_row['post_type'] ) {
				$this->id_map['subscription_ids'][ $src_row['ID'] ] = true;
			}
		}

		return $src_data;
	}

	public function post_process_posts() {

		// $src_table = $this->src_table($table);
		// $src_pkey = $this->get_table_pkey( $src_table );
		global $wpdb;
		$target_table = $this->target_table('posts');
		$target_pkey = $this->get_table_pkey( $target_table );

		$flipped_id_map = array_flip( $this->id_map['posts'] );



		$migrated_posts = $wpdb->get_results( "SELECT * FROM {$target_table} WHERE ID IN(".implode(", ",$this->id_map['posts']).")", ARRAY_A);

		foreach( $migrated_posts as $post ) {

			$updated_data = array();

			if( isset( $this->id_map['order_ids'][ $post['post_parent'] ] ) ) {
				$updated_data['post_parent'] = $this->id_map['posts'][ $post['post_parent'] ];
			}

			$guid_pieces = explode( ';p=', $post['guid'] );

			$old_id = $guid_pieces[ count( $guid_pieces) - 1];

			if( isset( $this->id_map['posts'][ $old_id ] ) ) {
				$guid_pieces[ count( $guid_pieces) - 1] = $this->id_map['posts'][ $old_id ];
			}

			$updated_data['guid'] = implode(';p=', $guid_pieces);

			if( false === $wpdb->update( $target_table, $updated_data, array('ID' => $post['ID'] ) ) ) {
				$this->log( "POST PROCESSING ON POST ID {$post['ID']} FAILED");
			}
		}
	}

	public function pre_process_usermeta( $src_data ) {

		foreach( $src_data as $index => $src_row ) {
			if( '_wcs_subscription_ids_cache' == $src_row['meta_key'] ) {
				unset( $src_data[ $index ] );
			}
			else if( '_last_order' == $src_row['meta_key'] ){
				
				$old_order_id = $src_row['meta_value'];
				if( isset( $this->id_map['posts'][ $old_order_id ] ) ) {

					$src_data[$index]['meta_value'] = $this->id_map['posts'][ $old_order_id ];	
				}
				else {
					unset( $src_data[$index] );
				}
			}

			$src_data[ $index ]['user_id'] = $this->id_map['users'][ $src_row['user_id'] ];
		}

		return $src_data;
	}

	public function pre_process_woocommerce_payment_tokens( $src_data ) {
		
		foreach( $src_data as $index => $src_row ) {

			$src_data[ $index ]['user_id'] = $this->id_map['users'][ $src_row['user_id'] ];
		}

		return $src_data;
	}

	public function pre_process_woocommerce_payment_tokenmeta( $src_data ) {
		
		foreach( $src_data as $index => $src_row ) {

			$src_data[ $index ]['payment_token_id'] = $this->id_map['woocommerce_payment_tokens'][ $src_row['payment_token_id'] ];
		}

		return $src_data;
	}

	public function pre_process_postmeta( $src_data ) {
		
		foreach( $src_data as $index => $src_row ) {

			$src_data[ $index ]['post_id'] = $this->id_map['posts'][ $src_row['post_id'] ];

			if( '_customer_user' == $src_row['meta_key'] ) {
				$src_data[ $index ]['meta_value'] = $this->id_map['users'][ $src_row['meta_value']];
			}
		}

		return $src_data;
	}

	public function pre_process_woocommerce_order_items( $src_data ) {
		
		foreach( $src_data as $index => $src_row ) {

			$src_data[ $index ]['order_id'] = $this->id_map['posts'][ $src_row['order_id'] ];
		}

		return $src_data;
	}

	public function pre_process_woocommerce_order_itemmeta( $src_data ) {
		
		foreach( $src_data as $index => $src_row ) {

			$src_data[ $index ]['order_item_id'] = $this->id_map['woocommerce_order_items'][ $src_row['order_item_id'] ];
		}

		return $src_data;
	}

	public function pre_process_comments( $src_data ) {
		
		foreach( $src_data as $index => $src_row ) {

			$src_data[ $index ]['comment_post_ID'] = $this->id_map['posts'][ $src_row['comment_post_ID'] ];
		}

		return $src_data;
	}




	// public function migrate_data( $db_table, )

	public function get_table_info( string $table_name ) {

		global $wpdb;
		
		if( !isset( $this->table_info_cache[ $table_name ] ) ) {

			// Do no initialize the table in $this->table_info_cache,
			// before we know that the table exists
			$table_info_result = $wpdb->get_results( "DESCRIBE {$table_name}", ARRAY_A);


			if( !empty( $table_info_result ) ) {
				$table_primary_key = false;

				// Determine primary key
				foreach( $table_info_result as $column_info ) {
					if( 'PRI' == $column_info['Key'] ) {
						$table_primary_key = $column_info['Field'];
						break;
					}
				}

				if( empty( $table_primary_key ) ) {
					$this->log( "ERROR: UNABLE TO DETERMINE PRIMARY KEY FOR TABLE $table_name" );
				}

				$this->table_info_cache[ $table_name ] = array(
					'cols' => array_column( $table_info_result, 'Field' ),
					'pkey'	=> $table_primary_key
				);
			}
			else {
				$this->log( "ERROR: UNABLE TO RETRIEVE TABLE INFORMATION FOR TABLE {$table_name}");
				return false;
			}

		}

		return $this->table_info_cache[ $table_name ];
	}

	public function get_table_pkey( $table_name ) {

		$table_info = $this->get_table_info( $table_name );
		return $table_info['pkey'] ?? null;
	}

	public function get_table_columns( $table_name, $omit_pkey = false ) {

		$table_columns = null;
		$table_info = $this->get_table_info( $table_name );

		if( $table_info ) {
			$table_columns = $table_info['cols'];

			if( $omit_pkey ) {
				$table_columns = array_diff( $table_columns, array( $table_info['pkey'] ) );
			}
		}

		return $table_columns;

	}

	public function src_table( $table ) {
		return $this->src_tables_prefix . $table;
	}

	public function target_table( $get_table = null ) {

		if( empty( $this->target_tables_cache ) ) {
			global $wpdb;

			$target_table_prefix = $this->config('dry_run') ? "{$this->src_tables_prefix}_dry_run_" : $wpdb->prefix;

			foreach( $this->tbl_pkeys as $table => $pkey ) {

				$this->target_tables_cache[$table] = $target_table_prefix . $table;
			}
		}

		return ( $get_table && isset( $this->target_tables_cache[ $get_table ] ) ) ? $this->target_tables_cache[$get_table] : $this->target_tables_cache;
	}

}