<?php
defined( 'ABSPATH' ) || exit;

class ONYX_Operation_export_orders extends ONYX_Operation_Base {

	public static $optype = 'export_orders';

	// public static $actions = array(
	// 	'check_wcs_user_role'
	// );
	public static $jump_interval = 20;




	protected $export_csv_fields = array(
		'Order ID'            => '',
		'Date Created'        => '',
		'Date Paid'           => '',
		'Date Completed'      => '',
		'Date Modified'       => '',
		'Order Status'        => '',
		'Order Total'		  => '',
		'Customer ID'         => '',
		'Billing Email'       => '',
		'Billing First Name'  => '',
		'Billing Last Name'   => '',
		'Billing Address Line 1' => '',
		'Billing Address Line 2' => '',
		'Billing City'		  => '',
		'Billing State'	  => '',
		'Billing Zip'	 	  => '',
		'Billing Country'	  => '',
		'Shipping First Name' => '',
		'Shipping Last Name'  => '',
		'Shipping Address Line 1' => '',
		'Shipping Address Line 2' => '',
		'Shipping City'		  => '',
		'Shipping State'	  => '',
		'Shipping Zip'	 	  => '',
		'Shipping Country'	  => '',
	);

	protected $wc_order_query_default_args = array(
		'orderby'  => 'date',
		'order'    => 'ASC',
		'limit'    => 20,
		'type'     => 'shop_order',
		'paged'    => 1,
		'paginate' => true,
	);


	public function extra_init() {


		$this->init_stat( 'orders_exported', 'Orders Exported' );
		

		$this->new_export_file( 'export_csv' );

		$this->write_csv_line_from_array( 'export_csv', array_keys( $this->export_csv_fields ) );

		$this->opmeta( 'query_page', 1 );
		

		$orders = wc_get_orders( $this->wc_order_query_default_args );


		$this->opmeta( 'total_orders', $orders->total );

		$this->log( "Order Items Export Initialized - Total Items to export: " . $orders->total );

	}

	// Request-level properties
	// public $target_ids = array();

	public function run( $async = true ) {

		parent::run( $async );
		$running_op       = true;
		$outputted_totals = false;

		if ( '' !== $this->config( 'exportStart') && '' !== $this->config( 'exportEnd') )  {
			$time = $this->config( 'exportStart') . '...' . $this->config('exportEnd');
			$this->wc_order_query_default_args['date_created'] =  $time;
		} elseif ( '' !== $this->config( 'exportStart') && '' === $this->config( 'exportEnd')) {
			$time = '>=' . $this->config( 'exportStart');
			$this->wc_order_query_default_args['date_created'] =  $time;
		} elseif ( '' === $this->config( 'exportStart') && '' !== $this->config( 'exportEnd') ) {
			$time = '<=' . $this->config( 'exportEnd');
			$this->wc_order_query_default_args['date_created'] =  $time;
		}

		
		$query_args = $this->wc_order_query_default_args;
		error_log($time);


		while ( $running_op ) {

			// Get Orders
			$query_args['paged'] = $this->opmeta( 'query_page' );
			$results             = wc_get_orders( $query_args );

			if ( count( $results->orders ) ) {
				foreach ( $results->orders as $order ) {

					$order_values = $this->export_csv_fields;

					$date_created   = $order->get_date_created();
					$date_paid      = $order->get_date_paid();
					$date_completed = $order->get_date_completed();
					$date_modified  = $order->get_date_modified();

					$order_values['Order ID']            = $order->get_id();
					$order_values['Date Created']        = $date_created ? $date_created->date( 'Y-m-d' ) : '';
					$order_values['Date Paid']           = $date_paid ? $date_created->date( 'Y-m-d' ) : '';
					$order_values['Date Completed']      = $date_completed ? $date_completed->date( 'Y-m-d' ) : '';
					$order_values['Date Modified']       = $date_modified ? $date_modified->date( 'Y-m-d' ) : '';
					$order_values['Order Status']        = $order->get_status();
					$order_values['Order Total']        = $order->get_total();
					$order_values['Customer ID']         = $order->get_user_id();
					$order_values['Billing Email']       = $order->get_billing_email();
					$order_values['Billing First Name']  = $order->get_billing_first_name();
					$order_values['Billing Last Name']   = $order->get_billing_last_name();
					$order_values['Billing Address Line 1']	 = $order->get_billing_address_1();
					$order_values['Billing Address Line 2']	 = $order->get_billing_address_2();
					$order_values['Billing City']		 = $order->get_billing_city();
					$order_values['Billing State']		 = $order->get_billing_state();
					$order_values['Billing Zip']		 = $order->get_billing_postcode();
					$order_values['Billing Country'] 	 = $order->get_billing_country();
					$order_values['Shipping First Name'] = $order->get_shipping_first_name();
					$order_values['Shipping Last Name']  = $order->get_shipping_last_name();
					$order_values['Shipping Address Line 1']	 = $order->get_shipping_address_1();
					$order_values['Shipping Address Line 2']	 = $order->get_shipping_address_2();
					$order_values['Shipping City']		 = $order->get_shipping_city();
					$order_values['Shipping State']		 = $order->get_shipping_state();
					$order_values['Shipping Zip']		 = $order->get_shipping_postcode();
					$order_values['Shipping Country'] 	 = $order->get_shipping_country();


					$this->write_csv_line_from_array( 'export_csv', $order_values );

					$this->stat( 'orders_exported' );

			}
			} else {
				$running_op = false;
				break;
			}

			$current_page = $this->opmeta( 'query_page' );

			if ( 0 == $current_page % 5 ) {
				$this->log( "Orders Exported: " . $this->get_stat( 'orders_exported' )  );
			}

			$next_page = $this->opmeta( 'query_page' ) + 1;
			$this->opmeta( 'query_page', $next_page );

			$this->maybe_jump_or_abort();

		}

		$this->successful_finish();

	}

}