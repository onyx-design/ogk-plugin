<?php
defined( 'ABSPATH' ) || exit;

class ONYX_Operation_Generate_Missed_Shipments extends ONYX_Operation_Base {
	public static $optype = 'generate_missed_shipments';

	public static $jump_interval = 45;

	public function extra_init() {

		$this->init_stat( 'shipments_generated', 'Shipments Generated' );
		$this->init_stat( 'subscriptions_processed', 'Subscriptions Processed' );

		// VALIDATE CONFIG OPTIONS

		// Validate Order IDs
		$subscription_ids = array_map( 'intval', array_map( 'trim', explode( ',', $this->config( 'cos_missedships_subs_ids' ) ) ) );

		if ( empty( $subscription_ids ) ) {
			$this->log( 'Invalid Order IDs supplied' );
			return false;
		}

		sort( $subscription_ids );
		$this->opmeta( 'subscription_ids', $subscription_ids );
		$this->opmeta( 'last_subscription_processed', 0 );

		// Validate Fulfillment Month
		$fulfillment_month = $this->config( 'cos_missedships_month' );

		if ( !OGK_Subscription_Dates::validate_fulfillment_month_code( $fulfillment_month ) ) {
			$this->log( 'Invalid fulfillment month, it should be in the format YYYYMM' );
			return false;
		}

		$this->log( "Operation Initialized" );
		return true;
	}

	public function run( $async = true ) {
		parent::run( $async );

		$fulfillment_month = $this->config( 'cos_missedships_month' );
		// while ($running_op) {
		foreach ( $this->opmeta( 'subscription_ids' ) as $subscription_id ) {

			if ( $subscription_id <= $this->opmeta( 'last_subscription_processed' ) ) {
				continue;
			}

			$subscription = wcs_get_subscription( $subscription_id );

			if ( $subscription->had_shipment_for_month( $fulfillment_month ) ) {

				$this->log( "Subscription {$subscription->get_id()} already has a shipment order for fulfillment month {$fulfillment_month}" );
			} else {

				$shipment_order = wcs_create_order_from_subscription( $subscription, 'shipment_order' );
				WCS_Related_Order_Store::instance()->add_relation( $shipment_order, $subscription, 'shipment' );

				$shipment_order->update_meta_data( "_fulfillment_month", $fulfillment_month );
				$shipment_order->update_meta_data( "_shipstation_custom_field_3", "missed_{$fulfillment_month}_shipment" );

				$shipment_order->set_status( 'processing' );

				$note_text = "Missed {$fulfillment_month} Shipment order created from operation ID " . $this->op_data['ID'] . ' - order ID: ' . $shipment_order->get_id();
				$subscription->add_order_note( $note_text, 0, true );
				$shipment_order->add_order_note( $note_text, 0, true );
				$this->log( $note_text );

				$shipment_order->save();
				$subscription->save();

				$this->stat( 'shipments_generated' );
			}

			$this->opmeta( 'last_subscription_processed', $subscription_id );
			$this->stat( 'subscriptions_processed' );

			$this->maybe_jump_or_abort();
		}

		$this->successful_finish();
	}
}
