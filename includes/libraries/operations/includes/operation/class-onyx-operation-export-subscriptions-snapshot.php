<?php
defined( 'ABSPATH' ) || exit;

class ONYX_Operation_Export_Subscriptions_Snapshot extends ONYX_Operation_Base {

	public static $optype = 'export_subscriptions_snapshot';

	public static $jump_interval = 20;

 
    
	protected $export_csv_fields = array(
		'Subscription ID'       => '',
		'Present Status'        => '',
		'User ID'               => '',
		'Billing Email'         => '',
		'Billing First Name'    => '',
		'Billing Last Name'     => '',
		'Shipping First Name'   => '',
		'Shipping Last Name'    => '',
		'Total Initial Payment' => '',
		'Date Created'          => '',
		// 'Next Payment'          => '',
		'End Date'              => '',
		// 'Next Shipment'         => '',
		// 'Last Shipment'         => '',
		// 'Shipment This Month?'  => '',
		'Billing Period'        => '',
        'Billing Interval'      => '',
        'Line Item Name'      => '',
		'Line Item SKU'       => '',
		'Line Item ID'        => '',
		'Line Item Price'     => '',
		'Line Item Qty'       => '',
		'Line Item Subtotal'  => '',
		'Line Item Total'     => '',
		// Added for export_subscriptions_snapshot
		'Shipments per Renewal'	=> '',
		'Snapshot Shipments Sent'	=> '',
		'Snapshot Shipments Owed' => '',
		'Snapshot Boxes Owed' => ''
	);

	protected $wc_subscription_query_default_args = array(
		'subscriptions_per_page' => 20,
		'paged'                  => 1,
		'offset'                 => null,
		'orderby'                => 'ID',
		'order'                  => 'ASC',
		'customer_id'            => 0,
		'product_id'             => 0,
		'variation_id'           => 0,
		'order_id'               => 0
	);

	protected $cached_product_skus = array();

	public function extra_init() {

        $this->init_stat( 'subscription_line_items_exported', 'Subscriptions Line Items Exported' );
		$this->init_stat( 'subscriptions_exported', 'Subscriptions Exported' );
		$this->init_stat( 'no_related_orders' , 'No Related Orders Found');

		$this->opmeta( 'query_page', 1 );

		$this->log( "Subscriptions Snapshot Export Initialized" );
		$this->opmeta( 'snapshot_date', $this->config( 'snapshot_date' ) . ' 23:59:59' );
		$snapshot_date = $this->opmeta( 'snapshot_date' );
		$this->log( "Exporting Snapshot of Subscriptions for date: {$snapshot_date}" );

		$this->new_export_file( 'export_csv' );

        $this->write_csv_line_from_array( 'export_csv', array_keys( $this->export_csv_fields ) );

	}

	public function run( $async = true ) {

		parent::run( $async );
		$running_op       = true;
		$outputted_totals = false;

		$snapshot_date = $this->opmeta( 'snapshot_date' );

		$query_args          = $this->wc_subscription_query_default_args;

		$query_args['paged'] = $this->opmeta( 'query_page' );
		$query_args['meta_query'] = array(
			'relation' => 'AND',
			array( 
				'key' => '_schedule_start',
				'compare' => '<=',
				'value' => $snapshot_date
			),
			array( 
				'key' => '_schedule_start',
				'compare' => '>=',
				'value' => '2016-01-01 00:00:00'
			),
			array(
				'relation'	=> 'OR',
				array(
					'key' => '_schedule_end',
					'compare' => '=',
					'value' => '0'
				),
				array(
					'key' => '_schedule_end',
					'compare' => '>=',
					'value' => $snapshot_date
				)
			)
		);

		while ( $running_op ) {
			// Get Orders
			$query_args['paged'] = $this->opmeta( 'query_page' );
			$results             = wcs_get_subscriptions( $query_args );

			if ( count( $results ) ) {
				foreach ( $results as $subscription ) {
					// $this->log( 'Checking Subscription ID: ' . $subscription->get_ID() );


                    $subscription_values                        = $this->export_csv_fields;
					$subscription_values['Subscription ID']     = $subscription->get_id();
					$subscription_values['Present Status']      = $subscription->get_status();
					$subscription_values['User ID']             = $subscription->get_user_id();
					$subscription_values['Billing Email']       = $subscription->get_billing_email();
					$subscription_values['Billing First Name']  = $subscription->get_billing_first_name();
					$subscription_values['Billing Last Name']   = $subscription->get_billing_last_name();
					$subscription_values['Shipping First Name'] = $subscription->get_shipping_first_name();
					$subscription_values['Shipping Last Name']  = $subscription->get_shipping_last_name();

					$subscription_values['Total Initial Payment'] = $subscription->get_total_initial_payment();

					$subscription_values['Date Created'] = $subscription->get_date( 'date_created' );
					
					$subscription_values['End Date']     = $subscription->get_date( 'end' );
					
					$subscription_values['Billing Period']       = $subscription->get_billing_period();
					$subscription_values['Billing Interval']     = $subscription->get_billing_interval();

					$subscription_values['Shipments per Renewal']	= 'year' == $subscription->get_billing_period() ? 12 : $subscription->get_billing_interval();

					$subscription_values['Snapshot Shipments Sent'] = 1;
					$subscription_values['Snapshot Shipments Owed'] = 0;


					// Find parent or renewal order closest to and before snapshot_date
					if( !('month' == $subscription->get_billing_period() && '1' == $subscription->get_billing_interval()) ) {
						// We can skip checking related orders for 1 month interval subscriptions because the prepaid boxes owed is always 0

						$related_orders = $subscription->get_related_orders( 'all', array( 'parent', 'renewal' ) );

						$snapshot_order = null;

						if( count( $related_orders ) ) {
							foreach( $related_orders as $related_order ) {
								$related_order_date_paid = $related_order->get_date_paid();

								if( $related_order_date_paid ) {
									$related_order_date_paid = $related_order_date_paid->format('Y-m-d H:i:s');

									if( 'completed' == $related_order->get_status() && $related_order_date_paid <= $snapshot_date  ) {

										if( empty( $snapshot_order ) ) {
											$snapshot_order = $related_order;
										}
										else {
											$snapshot_order_date_paid = $snapshot_order->get_date_paid()->format('Y-m-d H:i:s');

											if( $related_order_date_paid >= $snapshot_order_date_paid ) {
												$snapshot_order = $related_order;
												// $this->log( 'Line ' .__LINE__ . 'Subscription ID: ' . $subscription->get_ID() . ' snapshot order date_paid - ' . $snapshot_order_date_paid . ' related order date_paid - ' . $related_order_date_paid );
											}
										}

										
									}
								}
								
							}
						}
						else {
							// @todo
							// count stat: no related orders found
							$this->stat( 'no_related_orders' );


						}

						if( $snapshot_order ) {

							$interval_shipments_sent = 1;
							$shipment_orders = $subscription->get_related_orders( 'all', array( 'shipment' ) );
							$snapshot_order_date_paid = $snapshot_order->get_date_paid()->format('Y-m-d H:i:s');
							// $this->log( 'Line ' .__LINE__ . 'Subscription ID: ' . $subscription->get_ID() . ' snapshot_order_date_paid: ' . $snapshot_order_date_paid );

							// Account for the fact that Shipment orders started 
							if( $snapshot_order_date_paid < '2020-05-01 00:00:00' ) {
								
								$pre_may_2020_shipments_added = OGK_Subscription_Dates::fulfillment_months_date_diff( $snapshot_order_date_paid, '2020-05-02 00:00:00' ) - 1;
								
							}

							foreach( $shipment_orders as $shipment_order ) {
								$shipment_order_date_created = $shipment_order->get_date_created()->format('Y-m-d H:i:s');
								if( $shipment_order_date_created >= $snapshot_order_date_paid && $shipment_order_date_created <= $snapshot_date ) {
									$interval_shipments_sent++;
									// $this->log( 'Line ' .__LINE__ . 'Subscription ID: ' . $subscription->get_ID() . ' shipment order counted shipment_order_date_created:' . $shipment_order_date_created );
								}

							}

							// $this->log( 'Line ' .__LINE__ . 'Subscription ID: ' . $subscription->get_ID() . ' before may 2020 - extra interval shipments added: ' . $pre_may_2020_shipments_added );


							$total_shipments_owed = $subscription_values['Shipments per Renewal'];
							$snapshot_shipments_sent = min( $total_shipments_owed, ($interval_shipments_sent + $pre_may_2020_shipments_added));
							$subscription_values['Snapshot Shipments Sent'] = $snapshot_shipments_sent;
							$subscription_values['Snapshot Shipments Owed'] = $total_shipments_owed - $snapshot_shipments_sent;
						}
					}
					

		
                    
                    $order_items = $subscription->get_items();

                    if ( count( $order_items ) ) {

						foreach ( $order_items as $order_item ) {

							$subscription_item_values = $subscription_values;

							$line_item_id  = '';
							$line_item_sku = '';

							$variation_id = $order_item->get_variation_id();

							if ( $variation_id && '0' != $variation_id ) {
								$line_item_id = $variation_id;
							} else if ( $product_id = $order_item->get_product_id() ) {
								$line_item_id = $product_id;
							}

							if ( $line_item_id ) {
								$line_item_sku = $this->get_product_sku_by_id( $line_item_id );
							}
							// $line_item_id = $order_item->get_meta( '_variation_id' ) ?: $subscription;

							$subscription_item_values['Line Item Name']     = $order_item->get_name();
							$subscription_item_values['Line Item SKU']      = $line_item_sku;
							$subscription_item_values['Line Item ID']       = $line_item_id;
							$subscription_item_values['Line Item Price']    = wc_format_decimal( $order_item->get_subtotal() / max( $order_item->get_quantity(), 1 ) );
							$subscription_item_values['Line Item Qty']      = $order_item->get_quantity();
							$subscription_item_values['Line Item Subtotal'] = $order_item->get_subtotal();
							$subscription_item_values['Line Item Total']    = $order_item->get_total();
							$subscription_item_values['Snapshot Boxes Owed'] = $subscription_values['Snapshot Shipments Owed'] * $order_item->get_quantity();

							$this->write_csv_line_from_array( 'export_csv', $subscription_item_values );

							$this->stat( 'subscription_line_items_exported' );
						}

						
					} else {
						$this->log( "WARNING - Order ID {$subscription->get_id()} does not have any order items" );
					}

					$this->stat( 'subscriptions_exported' );

					// $this->write_csv_line_from_array( 'export_csv', $subscription_values );
					// $this->stat( 'subscriptions_exported' );

				}
			} else {
				$running_op = false;
				break;
			}
			
			

			// if ( 0 == $current_page % 5 ) {
				$this->log( "Subscriptions Exported: " . $this->get_stat( 'subscriptions_exported' ) );
				$this->log( "No Related Orders: " . $this->get_stat( 'no_related_orders' ) );
			// }

			$next_page = $this->opmeta( 'query_page' ) + 1;
			$this->opmeta( 'query_page', $next_page );

			$this->maybe_jump_or_abort();
		}

		$this->successful_finish();
	}

	public function get_product_sku_by_id( $id ) {

		if ( !isset( $this->cached_product_skus[$id] ) ) {

			$product = wc_get_product( $id );

			$this->cached_product_skus[$id] = $product->get_sku();
		}

		return $this->cached_product_skus[$id];
	}
}
