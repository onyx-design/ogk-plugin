<?php
defined( 'ABSPATH' ) || exit;

class ONYX_Operation_send_may_shipping_emails extends ONYX_Operation_Base {
	public static $optype = 'send_may_shipping_emails';

	// public static $actions = array(
	// 	'check_wcs_user_role'
	// );
	public static $jump_interval = 50;

	public $export_ids = array();

	public function extra_init() {

		$this->init_stat( 'emails_sent', 'Emails Sent' );

		// $subscriptions = wcs_get_subscriptions($this->wc_subscription_query_default_args);

		// $this->opmeta('total_subscriptions', $subscriptions->total);

		$order_ids = wc_get_orders( array(
			'date_created' => '1587818625...1589489025',
			'status'       => 'completed',
			'return'       => 'ids',
			'limit'        => -1,

		) );

		$this->opmeta( 'order_ids', $order_ids );

		$this->log( "Send May Orders Operation Initialized" );

	}

	public function run( $async = true ) {

		parent::run( $async );

		$order_ids = $this->opmeta( 'order_ids' );

		$this->log( count( $order_ids ) . ' remaining emails to send' );

		while ( $order_id = array_shift( $order_ids ) ) {

			$order = wc_get_order( $order_id );

			OGK_Hooks::ogk_send_customer_order_completed_email( $order );

			$this->log( 'Sent, order ' . $order_id . ' ' );
			$this->stat( 'emails_sent' );

			$this->opmeta( 'order_ids', $order_ids );

			$this->maybe_jump_or_abort();
		}

		$this->successful_finish();
	}
}
