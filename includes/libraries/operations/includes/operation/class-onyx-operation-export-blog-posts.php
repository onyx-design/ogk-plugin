<?php
defined( 'ABSPATH' ) || exit;

class ONYX_Operation_export_blog_posts extends ONYX_Operation_Base {

	public static $optype = 'export_blog_posts';

	public static $jump_interval = 45;

	/**
	 * Data structure of data to be exported
	 */
	protected $export_csv_fields = array(
		'Post ID'                 => '',
		'Title'                   => '',
		'Post Slug'               => '',
		'Post Status'             => '',
		'Post Template'           => '',
		'Post Thumbnail URL'      => '',
		'Post Date'               => '',
		'Post Modified'           => '',
		'Author ID'               => '',
		'Author Full Name'        => '',
		'Categories'              => '',
		'Tags'                    => '',
		'Rank Math SEO Score'     => '',
		'Rank Math Focus Keyword' => '',
		'Rank Math SEO Title'     => '',
	);

	/**
	 * Default get_posts query params
	 */
	protected $posts_query_default_args = array(
		'post_type'      => 'post',
		'posts_per_page' => 20,
		'post_status'    => 'any',
		'paged'          => 1,
		'offset'         => null,
		'orderby'        => 'ID',
		'order'          => 'ASC',
	);

	/**
	 * Configure stats, make new files, write CSV headings, configure opmeta & logs
	 */
	public function extra_init() {

		$this->init_stat( 'blog_posts_exported', 'Blog Posts Exported' );
		$this->new_export_file( 'export_csv' );
		$this->write_csv_line_from_array( 'export_csv', array_keys( $this->export_csv_fields ) );
		$this->opmeta( 'query_page', 1 );
		$posts_query = new WP_Query( $this->posts_query_default_args );
		$this->opmeta( 'total_posts', $posts_query->found_posts );
		$this->log( "Blog Posts Export Initialized - Total Items to export: " . $posts_query->found_posts );

	}

	public function run( $async = true ) {

		parent::run( $async );
		$running_op = true;
		$query_args = $this->posts_query_default_args;

		while ( $running_op ) {

			// Get Posts
			$query_args['paged'] = $this->opmeta( 'query_page' );
			$posts               = get_posts( $query_args );

			if ( count( $posts ) ) {
				foreach ( $posts as $post ) {
					$post_values = $this->export_csv_fields;

					// Post Data
					$post_id     = $post->ID;
					$post_title  = $post->post_title;
					$post_slug   = $post->post_name;
					$post_status = $post->post_status;
					if ( $post_status === "trash" ) {
						continue;
					}
					$date_created  = $post->post_date;
					$date_modified = $post->post_modified;

					// Post Meta Data
					$post_template      = get_page_template_slug( $post_id );
					$post_thumbnail_url = get_the_post_thumbnail_url( $post_id, 'full' );

					// Author Data
					$author_id   = $post->post_author;
					$author_name = get_the_author_meta( 'display_name', $author_id );

					// Categories Data
					$category_slugs              = wp_get_post_categories( $post_id, array( "fields" => "names" ) );
					$concatenated_category_slugs = empty( $category_slugs ) ? "" : implode( ",", $category_slugs );

					// Tags Data
					$tag_slugs              = wp_get_post_tags( $post_id, array( "fields" => "names" ) );
					$concatenated_tag_slugs = empty( $tag_slugs ) ? "" : implode( ",", $tag_slugs );

					// Rank Math Data
					$rank_math_seo_score     = get_post_meta( $post_id, "rank_math_seo_score", true );
					$rank_math_focus_keyword = get_post_meta( $post_id, "rank_math_focus_keyword", true );
					$rank_math_title         = get_post_meta( $post_id, "rank_math_title", true );

					// Populate Post Values Array
					$post_values['Post ID']                 = $post_id;
					$post_values['Title']                   = $post_title;
					$post_values['Post Slug']               = $post_slug;
					$post_values['Post Status']             = $post_status;
					$post_values['Post Template']           = $post_template;
					$post_values['Post Thumbnail URL']      = $post_thumbnail_url;
					$post_values['Post Date']               = $date_created;
					$post_values['Post Modified']           = $date_modified;
					$post_values['Author ID']               = $author_id;
					$post_values['Author Full Name']        = $author_name;
					$post_values['Categories']              = $concatenated_category_slugs;
					$post_values['Tags']                    = $concatenated_tag_slugs;
					$post_values['Rank Math SEO Score']     = $rank_math_seo_score;
					$post_values['Rank Math Focus Keyword'] = $rank_math_focus_keyword;
					$post_values['Rank Math SEO Title']     = $rank_math_title;

					// Write To File
					$this->write_csv_line_from_array( 'export_csv', $post_values );
					$this->stat( 'blog_posts_exported' );
				}
			} else {
				$running_op = false;
				break;
			}

			$current_page = $this->opmeta( 'query_page' );

			// Output data to logs every 5 pages
			if ( 0 == $current_page % 5 ) {
				$this->log( "Orders Exported: " . $this->get_stat( 'blog_posts_exported' ) );
			}

			// Iterate to next page in page meta
			$next_page = $this->opmeta( 'query_page' ) + 1;
			$this->opmeta( 'query_page', $next_page );

			// Check if jump is necessary or if user has aborted operation
			$this->maybe_jump_or_abort();

		}
		$this->successful_finish();

	}
}