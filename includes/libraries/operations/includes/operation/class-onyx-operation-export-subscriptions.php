<?php
defined( 'ABSPATH' ) || exit;

class ONYX_Operation_Export_Subscriptions extends ONYX_Operation_Base {

	public static $optype = 'export_subscriptions';

	// public static $actions = array(
	// 	'check_wcs_user_role'
	// );
	public static $jump_interval = 45;

	protected $export_csv_fields = array(
		'Subscription ID'       => '',
		'Status'                => '',
		'User ID'               => '',
		'Billing Email'         => '',
		'Billing First Name'    => '',
		'Billing Last Name'     => '',
		'Shipping First Name'   => '',
		'Shipping Last Name'    => '',
		'Total Initial Payment' => '',
		'Date Created'          => '',
		'Next Payment'          => '',
		'End Date'              => '',
		'Next Shipment'         => '',
		'Last Shipment'         => '',
		'Shipment This Month?'  => '',
		'Billing Period'        => '',
		'Billing Interval'      => '',
	);

	protected $wc_subscription_query_default_args = array(
		'subscriptions_per_page' => 20,
		'paged'                  => 1,
		'offset'                 => null,
		'orderby'                => 'ID',
		'order'                  => 'ASC',
		'customer_id'            => 0,
		'product_id'             => 0,
		'variation_id'           => 0,
		'order_id'               => 0,
		'subscription_status'    => array( 'any' ),
		'meta_query_relation'    => 'AND',
	);

	public function extra_init() {

		$this->init_stat( 'subscriptions_exported', 'Subscriptions Exported' );

		$this->opmeta( 'query_page', 1 );

		$this->log( "Subscription Export Initialized" );
		$this->log( "exporting subscription statuses: " . $this->config( 'include_statuses' ) );

		if ( $this->config( 'update_next_shipment_date' ) ) {
			$this->log( 'Updating next shipment dates' );
			$this->init_stat( 'update_next_shipment_date', 'Next shipment date updated' );
			$this->export_csv_fields['Months since last shipment'] = '';
			$this->export_csv_fields['Months until term end']      = '';
			$this->export_csv_fields['Next Shipment Date UPDATED'] = '';
		}

		if ( $this->config( 'create_shipment_orders' ) ) {
			$this->log( 'Validating (and creating) shipment orders' );
			$this->init_stat( 'created_shipment_order', 'Shipment Orders Created' );
			$this->export_csv_fields['Shipment Order CREATED'] = '';
		}

		if ( $this->config( 'dry_run' ) ) {
			$this->log( 'DRY RUN ENABLED - NO ACTUAL UPDATES WILL BE MADE' );
		}

		$this->new_export_file( 'export_csv' );

		$this->write_csv_line_from_array( 'export_csv', array_keys( $this->export_csv_fields ) );
	}

	public function run( $async = true ) {

		parent::run( $async );
		$running_op       = true;
		$outputted_totals = false;

		$query_args          = $this->wc_subscription_query_default_args;
		$query_args['paged'] = $this->opmeta( 'query_page' );

		if ( !empty( $this->config( 'include_statuses' ) ) ) {
			$query_args['subscription_status'] = (array) $this->config( 'include_statuses' );
		}

		if ( $this->config( 'update_next_shipment_date' ) ) {
			$this->export_csv_fields['Months since last shipment'] = '';
			$this->export_csv_fields['Months until term end']      = '';
			$this->export_csv_fields['Next Shipment Date UPDATED'] = '';
		}

		if ( $this->config( 'create_shipment_orders' ) ) {
			$this->export_csv_fields['Shipment Order CREATED'] = '';
		}

		while ( $running_op ) {

			// Get Orders
			$query_args['paged'] = $this->opmeta( 'query_page' );
			$results             = wcs_get_subscriptions( $query_args );

			if ( count( $results ) ) {
				foreach ( $results as $subscription ) {
					$subscription_values                        = $this->export_csv_fields;
					$subscription_values['Subscription ID']     = $subscription->get_id();
					$subscription_values['Status']              = $subscription->get_status();
					$subscription_values['User ID']             = $subscription->get_user_id();
					$subscription_values['Billing Email']       = $subscription->get_billing_email();
					$subscription_values['Billing First Name']  = $subscription->get_billing_first_name();
					$subscription_values['Billing Last Name']   = $subscription->get_billing_last_name();
					$subscription_values['Shipping First Name'] = $subscription->get_shipping_first_name();
					$subscription_values['Shipping Last Name']  = $subscription->get_shipping_last_name();

					$subscription_values['Total Initial Payment'] = $subscription->get_total_initial_payment();

					$subscription_values['Date Created'] = $subscription->get_date( 'date_created' );
					$subscription_values['Next Payment'] = $subscription->get_date( 'next_payment' );
					$subscription_values['End Date']     = $subscription->get_date( 'end' );

					$subscription_values['Next Shipment']        = $subscription->get_date( 'next_shipment' );
					$subscription_values['Last Shipment']        = $subscription->get_date( 'last_shipment_order' );
					$subscription_values['Shipment This Month?'] = boolstr( $subscription->had_shipment_this_month() );
					$subscription_values['Billing Period']       = $subscription->get_billing_period();
					$subscription_values['Billing Interval']     = $subscription->get_billing_interval();

					if ( $this->config( 'update_next_shipment_date' ) ) {
						$valid_next_shipment_date = $subscription->get_date( 'next_shipment' );

						$next_payment_date = $subscription->get_date( 'next_payment' );
						$end_date          = $subscription->get_date( 'end' );

						if ( !empty( $end_date ) ) {
							$end_of_prepaid_term = $end_date;
						} else if ( !empty( $next_payment_date ) ) {
							$end_of_prepaid_term = $next_payment_date;
						} else {
							return false;
						}

						$end_of_prepaid_term_month_diff = OGK_Subscription_Dates::fulfillment_months_date_diff( "now", $end_of_prepaid_term );

						$last_shipment_order_date            = $subscription->get_date( 'last_shipment_order' );
						$last_shipment_order_date_month_diff = OGK_Subscription_Dates::fulfillment_months_date_diff( "now", $last_shipment_order_date );

						$subscription_values['Months since last shipment'] = $last_shipment_order_date_month_diff;
						$subscription_values['Months until term end']      = $end_of_prepaid_term_month_diff;

						if ( $subscription->is_shippable_status() ) {

							if ( $last_shipment_order_date_month_diff == 0 && $end_of_prepaid_term_month_diff >= 2 ) {
								$valid_next_shipment_date = OGK_Subscription_Dates::date_modify_subscription_month( "now", 1 );
							}

							if ( $last_shipment_order_date_month_diff < 0 && $end_of_prepaid_term_month_diff >= 1 ) {
								$valid_next_shipment_date = OGK_Subscription_Dates::date_modify_subscription_month( "now", 0 );
							}
						}

						$current_next_shipment_date = $subscription->get_date( 'next_shipment' );

						if ( $valid_next_shipment_date !== $current_next_shipment_date ) {

							$subscription_values['Next Shipment Date UPDATED'] = $valid_next_shipment_date;
							$this->stat( 'update_next_shipment_date' );

							if ( !$this->config( 'dry_run' ) ) {
								$subscription->update_dates( array( 'next_shipment' => $valid_next_shipment_date ) );
								$note_verb = empty( $valid_next_shipment_date ) ? 'removed' : 'updated';
								$subscription->add_order_note( 'Next shipment ' . $note_verb . ' by operation ID ' . $this->op_data['ID'] );
							}
						}
					}

					if ( $this->config( 'create_shipment_orders' ) && $subscription->is_shippable_status() ) {

						if ( !$subscription->had_shipment_this_month() && empty( $subscription->get_date( 'next_shipment' ) ) ) {

							if ( !$this->config( 'dry_run' ) ) {

								$shipment_order = wcs_create_order_from_subscription( $subscription, 'shipment_order' );

								WCS_Related_Order_Store::instance()->add_relation( $shipment_order, $subscription, 'shipment' );

								$shipment_order->set_status( 'processing' );
								$shipment_order->save();

								$subscription->add_order_note( 'New shipment order created from operation ID ' . $this->op_data['ID'] . ' - order ID: ' . $shipment_order->get_id(), 0, true );

								apply_filters( 'wcs_shipment_order_created', $shipment_order, $subscription );

								$subscription_values['Shipment Order CREATED'] = $shipment_order->get_id();
							} else {

								$subscription_values['Shipment Order CREATED'] = 'YES';
							}

							$this->stat( 'created_shipment_order' );
						}
					}

					$this->write_csv_line_from_array( 'export_csv', $subscription_values );
					$this->stat( 'subscriptions_exported' );
				}
			} else {
				$running_op = false;
				break;
			}
			$current_page = $this->opmeta( 'query_page' );

			if ( 0 == $current_page % 5 ) {
				$this->log( "Subscriptions Exported: " . $this->get_stat( 'subscriptions_exported' ) );
			}

			$next_page = $this->opmeta( 'query_page' ) + 1;
			$this->opmeta( 'query_page', $next_page );

			$this->maybe_jump_or_abort();
		}

		$this->successful_finish();
	}
}
