<?php
defined( 'ABSPATH' ) || exit;

class ONYX_Operation_export_sub_line_items extends ONYX_Operation_Base {

    public static $optype = 'export_sub_line_items';

    public static $jump_interval = 45;

 
    
	protected $export_csv_fields = array(
		'Subscription ID'       => '',
		'Status'                => '',
		'User ID'               => '',
		'Billing Email'         => '',
		'Billing First Name'    => '',
		'Billing Last Name'     => '',
		'Shipping First Name'   => '',
		'Shipping Last Name'    => '',
		'Total Initial Payment' => '',
		'Date Created'          => '',
		'Next Payment'          => '',
		'End Date'              => '',
		'Next Shipment'         => '',
		'Last Shipment'         => '',
		'Shipment This Month?'  => '',
		'Billing Period'        => '',
        'Billing Interval'      => '',
        'Line Item Name'      => '',
		'Line Item SKU'       => '',
		'Line Item ID'        => '',
		'Line Item Price'     => '',
		'Line Item Qty'       => '',
		'Line Item Subtotal'  => '',
		'Line Item Total'     => '',
	);

	protected $wc_subscription_query_default_args = array(
		'subscriptions_per_page' => 20,
		'paged'                  => 1,
		'offset'                 => null,
		'orderby'                => 'ID',
		'order'                  => 'ASC',
		'customer_id'            => 0,
		'product_id'             => 0,
		'variation_id'           => 0,
		'order_id'               => 0,
		'subscription_status'    => array( 'active', 'on-hold', 'pending-cancel' ),
		'meta_query_relation'    => 'AND',
	);

	protected $cached_product_skus = array();

	public function extra_init() {

        $this->init_stat( 'subscription_line_items_exported', 'Subscriptions Line Items Exported' );
		$this->init_stat( 'subscriptions_exported', 'Subscriptions Exported' );

		$this->opmeta( 'query_page', 1 );

		$this->log( "Subscription Line Item Export Initialized" );
		$this->log( "Exporting Order Items for subscriptions with status: " . implode( ', ', $this->wc_subscription_query_default_args['subscription_status'] ) );

		$this->new_export_file( 'export_csv' );

        $this->write_csv_line_from_array( 'export_csv', array_keys( $this->export_csv_fields ) );

	}

	public function run( $async = true ) {

		parent::run( $async );
		$running_op       = true;
		$outputted_totals = false;

		$query_args          = $this->wc_subscription_query_default_args;
		$query_args['paged'] = $this->opmeta( 'query_page' );


		while ( $running_op ) {
			// Get Orders
			$query_args['paged'] = $this->opmeta( 'query_page' );
			$results             = wcs_get_subscriptions( $query_args );

			if ( count( $results ) ) {
				foreach ( $results as $subscription ) {
                    $subscription_values                        = $this->export_csv_fields;
					$subscription_values['Subscription ID']     = $subscription->get_id();
					$subscription_values['Status']              = $subscription->get_status();
					$subscription_values['User ID']             = $subscription->get_user_id();
					$subscription_values['Billing Email']       = $subscription->get_billing_email();
					$subscription_values['Billing First Name']  = $subscription->get_billing_first_name();
					$subscription_values['Billing Last Name']   = $subscription->get_billing_last_name();
					$subscription_values['Shipping First Name'] = $subscription->get_shipping_first_name();
					$subscription_values['Shipping Last Name']  = $subscription->get_shipping_last_name();

					$subscription_values['Total Initial Payment'] = $subscription->get_total_initial_payment();

					$subscription_values['Date Created'] = $subscription->get_date( 'date_created' );
					$subscription_values['Next Payment'] = $subscription->get_date( 'next_payment' );
					$subscription_values['End Date']     = $subscription->get_date( 'end' );

					$subscription_values['Next Shipment']        = $subscription->get_date( 'next_shipment' );
					$subscription_values['Last Shipment']        = $subscription->get_date( 'last_shipment_order' );
					$subscription_values['Shipment This Month?'] = boolstr( $subscription->had_shipment_this_month() );
					$subscription_values['Billing Period']       = $subscription->get_billing_period();
					$subscription_values['Billing Interval']     = $subscription->get_billing_interval();
                    
                    $order_items = $subscription->get_items();

                    if ( count( $order_items ) ) {

						foreach ( $order_items as $order_item ) {

							$subscription_item_values = $subscription_values;

							$line_item_id  = '';
							$line_item_sku = '';

							$variation_id = $order_item->get_variation_id();

							if ( $variation_id && '0' != $variation_id ) {
								$line_item_id = $variation_id;
							} else if ( $product_id = $order_item->get_product_id() ) {
								$line_item_id = $product_id;
							}

							if ( $line_item_id ) {
								$line_item_sku = $this->get_product_sku_by_id( $line_item_id );
							}
							// $line_item_id = $order_item->get_meta( '_variation_id' ) ?: $subscription;

							$subscription_item_values['Line Item Name']     = $order_item->get_name();
							$subscription_item_values['Line Item SKU']      = $line_item_sku;
							$subscription_item_values['Line Item ID']       = $line_item_id;
							$subscription_item_values['Line Item Price']    = wc_format_decimal( $order_item->get_subtotal() / max( $order_item->get_quantity(), 1 ) );
							$subscription_item_values['Line Item Qty']      = $order_item->get_quantity();
							$subscription_item_values['Line Item Subtotal'] = $order_item->get_subtotal();
							$subscription_item_values['Line Item Total']    = $order_item->get_total();

							$this->write_csv_line_from_array( 'export_csv', $subscription_item_values );

							$this->stat( 'subscription_line_items_exported' );
						}

						
					} else {
						$this->log( "WARNING - Order ID {$subscription->get_id()} does not have any order items" );
					}

					$this->stat( 'subscriptions_exported' );

					// $this->write_csv_line_from_array( 'export_csv', $subscription_values );
					// $this->stat( 'subscriptions_exported' );

				}
			} else {
				$running_op = false;
				break;
			}
			$current_page = $this->opmeta( 'query_page' );

			if ( 0 == $current_page % 5 ) {
				$this->log( "Subscriptions Exported: " . $this->get_stat( 'subscriptions_exported' ) );
			}

			$next_page = $this->opmeta( 'query_page' ) + 1;
			$this->opmeta( 'query_page', $next_page );

			$this->maybe_jump_or_abort();
		}

		$this->successful_finish();
	}

	public function get_product_sku_by_id( $id ) {

		if ( !isset( $this->cached_product_skus[$id] ) ) {

			$product = wc_get_product( $id );

			$this->cached_product_skus[$id] = $product->get_sku();
		}

		return $this->cached_product_skus[$id];
	}

}
