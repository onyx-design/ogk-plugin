<?php
defined( 'ABSPATH' ) || exit;

class ONYX_Operation_Update_Subscription_Dates_Feb_2021 extends ONYX_Operation_Base {

	public static $optype = 'update_subscription_dates_feb_2021';

	public static $jump_interval = 45;

    protected $wc_subscription_query_default_args = array(
		'subscriptions_per_page' => 20,
		'paged'                  => 1,
		'offset'                 => null,
		'orderby'                => 'ID',
		'order'                  => 'ASC',
		'customer_id'            => 0,
		'product_id'             => 0,
		'variation_id'           => 0,
		'order_id'               => 0,
		'subscription_status'    => array( 'active', 'on-hold', 'pending-cancel' ),
		'meta_query_relation'    => 'AND',
	);

	protected $export_csv_fields = array(
		'Subscription ID'       => '',
		'Status'                => '',
		'User ID'               => '',
		'Billing Email'         => '',
		'Billing First Name'    => '',
		'Billing Last Name'     => '',
		'Shipping First Name'   => '',
		'Shipping Last Name'    => '',
		'Total Initial Payment' => '',
		'Date Created'          => '',
		'Next Payment'          => '',
		'Next Payment GMT'      => '',
		'End Date'              => '',
		'Next Shipment'         => '',
		'Next Shipment GMT'     => '',
		'Last Shipment'         => '',
		'Last Shipment GMT'     => '',
		'Shipment This Month?'  => '',
		'Billing Period'        => '',
		'Billing Interval'      => '',
		'Count Jan Shipments'	=> '',
		'Error Codes'			=> '',
		'Updated Date Type'		=> '',
		'Updated Date'  			=> '',
		'Updated Date GMT'  		=> '',
	);


	public function extra_init() {
		$this->init_stat( 'subscriptions_scanned', 'Subscriptions Scanned' );
		$this->init_stat( 'subscriptions_date_modified', 'Subscriptions Date Modified' );
		$this->init_stat( 'subscriptions_to_be_updated', 'Subscriptions to be updated' );

		$this->init_stat( 'next_shipment_0228', 'Next Shipment Feb 28th, 2021' );
		$this->init_stat( 'extra_jan_shipments', 'Too many shipments, Jan, 2021' );
		
		$this->new_export_file( 'export_csv' );
		$this->write_csv_line_from_array( 'export_csv', array_keys( $this->export_csv_fields ) );

		if ( $this->config( 'dry_run' ) ) {
			$this->log( 'DRY RUN ENABLED - NO ACTUAL UPDATES WILL BE MADE' );
		}

		$this->opmeta( 'query_page', 1 );
		$this->log( "Subscription Shipments Date Update Validation Initialized" );

	}

	public function run( $async = true ) {
		parent::run( $async );
		$running_op = true;

		$query_args          = $this->wc_subscription_query_default_args;
		$query_args['paged'] = $this->opmeta( 'query_page' );

		$updated_shipment_datetime 	= new DateTime('2021-03-02 14:00:00', new DateTimeZone( get_option( 'timezone_string' ) ) );
		$updated_shipment_date 		= $updated_shipment_datetime->format( 'Y-m-d H:i:s' );
		$updated_shipment_date_gmt 	= $updated_shipment_datetime->setTimezone( new DateTimeZone( 'UTC' ) )->format( 'Y-m-d H:i:s' );



		while ( $running_op ) {
			// Get Subscriptions 
			$query_args['paged'] = $this->opmeta( 'query_page' );
			$results             = wcs_get_subscriptions( $query_args );

			if ( count( $results ) ) {
				foreach ( $results as $subscription ) {
					$this->stat( 'subscriptions_scanned' );

					$next_shipment_0228 = false;
					if( $subscription->get_date( 'next_shipment', 'site' ) >= '2021-02-28 00:00:00' 
						&& $subscription->get_date( 'next_shipment', 'site' ) <= '2021-02-28 23:59:59'
					) {

						$next_shipment_0228 = true;
						$this->stat( 'next_shipment_0228' );					
					}

					$next_payment_0228 = false;
					if( $subscription->get_date('next_payment', 'site') >= '2021-02-28 00:00:00' 
						&&  $subscription->get_date( 'next_payment', 'site' ) <= '2021-02-28 23:59:59'
					) {
						$next_payment_0228 = true;
						$next_shipment_0228 = true;
					}

					$shipment_count_202101 = $subscription->had_shipment_for_month( '202101', true );
					$extra_jan_shipments = ( $shipment_count_202101 > 1 );

					if( $extra_jan_shipments ) {
						$this->stat( 'extra_jan_shipments' );

					}

					if( $extra_jan_shipments || $next_shipment_0228 ) {
						
						$subscription_values = $this->export_csv_fields;

						$subscription_values                        = $this->export_csv_fields;
						$subscription_values['Subscription ID']     = $subscription->get_id();
						$subscription_values['Status']              = $subscription->get_status();
						$subscription_values['User ID']             = $subscription->get_user_id();
						$subscription_values['Billing Email']       = $subscription->get_billing_email();
						$subscription_values['Billing First Name']  = $subscription->get_billing_first_name();
						$subscription_values['Billing Last Name']   = $subscription->get_billing_last_name();
						$subscription_values['Shipping First Name'] = $subscription->get_shipping_first_name();
						$subscription_values['Shipping Last Name']  = $subscription->get_shipping_last_name();

						$subscription_values['Total Initial Payment'] = $subscription->get_total_initial_payment();

						$subscription_values['Date Created'] = $subscription->get_date( 'date_created' );
						$subscription_values['Next Payment'] = $subscription->get_date( 'next_payment', 'site' );
						$subscription_values['Next Payment GMT'] = $subscription->get_date( 'next_payment' );
						$subscription_values['End Date']     = $subscription->get_date( 'end' );

						$subscription_values['Next Shipment']        = $subscription->get_date( 'next_shipment', 'site' );
						$subscription_values['Next Shipment GMT']        = $subscription->get_date( 'next_shipment' );
						$subscription_values['Last Shipment']        = $subscription->get_date( 'last_shipment_order', 'site' );
						$subscription_values['Last Shipment GMT']        = $subscription->get_date( 'last_shipment_order' );
						$subscription_values['Shipment This Month?'] = boolstr( $shipment_count_202101 );
						$subscription_values['Billing Period']       = $subscription->get_billing_period();
						$subscription_values['Billing Interval']     = $subscription->get_billing_interval();
						$subscription_values['Count Jan Shipments']     = $shipment_count_202101;

						
		

						if( $next_shipment_0228 ) {
							$subscription_values['Error Codes'] .= 'next_shipment_0228';
						}

						if( $extra_jan_shipments ) {
							$subscription_values['Error Codes'] .= ' extra_jan_shipments';
						}

						if( $extra_jan_shipments && $next_shipment_0228 ) {
						
							$this->stat( 'subscriptions_to_be_updated' );

							$update_next_date_type = $next_payment_0228 ? 'next_payment' : 'next_shipment';

 							$subscription_values['Updated Date Type']	= $update_next_date_type;
							$subscription_values['Updated Date']		= $updated_shipment_date;
							$subscription_values['Updated Date GMT']	= $updated_shipment_date_gmt;
							

							if( !$this->config( 'dry_run' ) ) {

								$subscription->update_dates( array( $update_next_date_type => $updated_shipment_date_gmt ) );
								
							}
						}

						

						// output CSV line
						$this->write_csv_line_from_array( 'export_csv', $subscription_values );

					}

					

				}
					
			} else {
				$running_op = false;
				break;
			}
			

			$current_page = $this->opmeta( 'query_page' );
			if ( 0 == $current_page % 5 ) {
				$this->log( "Subscriptions Validated: " . $this->get_stat( 'subscriptions_scanned' ) );
				wp_cache_flush();

			}
			$next_page = $this->opmeta( 'query_page' ) + 1;
			$this->opmeta( 'query_page', $next_page );


			$this->maybe_jump_or_abort();
		}

		$this->successful_finish();
	}

	}