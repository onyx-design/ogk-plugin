<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * This class contains the action Triggers for sending out WooCommerce Emails.
 * When an action occurs, this class will send out the email for that action.
 *
 * dev:todo This can be setup so that emails can be turned off
 */
class ONYX_Admin {

	public $page_slug;

	public function __construct() {
		$this->page_slug = isset( $_GET['page'] ) ? $_GET['page'] : '';
		add_action( 'admin_menu', array( $this, 'custom_menu_pages' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
	}

	public function custom_menu_pages() {
		add_menu_page( 'ONYX Tools', 'ONYX Tools', 'manage_options', 'onyx-admin-page', array( $this, 'onyx_admin_page_content' ), 'dashicons-admin-tools' );
		if ( 2 == get_current_user_id() ) {
			add_menu_page( 'ONYX Sandbox', 'ONYX Sandbox', 'manage_options', 'onyx-admin-sandbox', array( $this, 'onyx_admin_page_sandbox' ), 'dashicons-admin-tools' );
		}
	}

	public function admin_enqueue_scripts() {
		if ( 'onyx-admin-page' == $this->page_slug ) {
			wp_enqueue_style( 'asc-onyx-admin-css', OPERATIONS_URL . '/dist/css/asc-onyx-admin.css' );
			wp_enqueue_script( 'asc-onyx-admin-js', OPERATIONS_URL . '/dist/js/onyx.js', array( 'jquery' ) );
		}
	}

	public function onyx_admin_page_content() {
		include OPERATIONS_TEMPLATE_PATH . '/admin/admin-page-onyx-tools.php';
	}
}
return new ONYX_Admin();
