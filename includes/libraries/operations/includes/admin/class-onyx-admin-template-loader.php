<?php
defined( 'ABSPATH' ) || exit;

class ONYX_Admin_Template_Loader {

	public function __construct() {
		add_filter( 'theme_page_templates', array( $this, 'add_custom_page_templates_to_admin' ), 10, 4 );
	}

	public function add_custom_page_templates_to_admin( $post_templates, $wp_theme, $post, $post_type ) {
		foreach ( OGK()->page_templates() as $template ) {
			$post_templates[$template] = __( $this->get_template_name_from_template( $template ) );
		}

		return $post_templates;
	}

	private function get_template_name_from_template( $template ) {
		$template_metadata = get_file_data( OPERATIONS_TEMPLATE_PATH . 'pages/' . str_replace( 'onyx-', '', $template ), array( 'name' => 'Template Name' ) );

		return ( isset( $template_metadata['name'] ) && !empty( $template_metadata['name'] ) ) ? $template_metadata['name'] : ucwords( trim( str_replace( array( 'template', '-', '.php' ), ' ', strtolower( $template ) ) ) );
	}
}
return new ONYX_Admin_Template_Loader();
