<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

class ONYX_Ajax {

	/**
	 * @var array
	 */
	public static $messages = array();

	protected static $response_data;

	protected static $allow_send_json = true;

	protected static $response_status = 'success';

	protected static $refresh_tables = false;

	protected static $clear_notices = false;

	protected static $notice_target = null;

	protected static $response_redirect = null;

	/**
	 * Init function for ONYX_Ajax
	 *
	 * @return 	void
	 */
	public static function init() {

		add_action( 'init', array( __CLASS__, 'define_ajax' ), 0 );

		self::add_ajax_events();
	}

	/**
	 * Set WC AJAX constant and header.
	 *
	 * @return 	void
	 */
	public static function define_ajax() {

		if ( !empty( $_POST['onyx_ajax'] ) ) {

			wc_maybe_define_constant( 'DOING_AJAX', true );
			wc_maybe_define_constant( 'ONYX_DOING_AJAX', true );
			if ( !WP_DEBUG || ( WP_DEBUG && !WP_DEBUG_DISPLAY ) ) {

				@ini_set( 'display_errors', 0 ); // Turn off display_errors during AJAX events to prevent malformed JSON
			}

			$GLOBALS['wpdb']->hide_errors();
		}
	}

	/**
	 * Hook in methods - uses WordPress AJAX handlers (admin-ajax).
	 *
	 * @return 	void
	 */
	public static function add_ajax_events() {

		// woocommerce_EVENT => nopriv
		$ajax_events = array(
			// General
			// 'login'				=> true,
			// 'create_account'	=> true,

			// Operations
			'init_op'     => false,
			'view_op'     => false,
			'abort_op'    => false,
			'commence_op' => true,
			'get_ops_log' => false,

			// Referrals
			// 'update_referral'	=> false,
			// 'send_referral'		=> false,
		);

		foreach ( $ajax_events as $ajax_event => $nopriv ) {
			add_action( 'wp_ajax_onyx_' . $ajax_event, array( __CLASS__, $ajax_event ) );

			if ( $nopriv ) {
				add_action( 'wp_ajax_nopriv_onyx_' . $ajax_event, array( __CLASS__, $ajax_event ) );

				// WC AJAX can be used for frontend ajax requests.
				add_action( 'w_ajax_' . $ajax_event, array( __CLASS__, $ajax_event ) );
			}
		}
	}

	/**
	 * Generalized AJAX nonce check
	 *
	 * @param 	string $nonce
	 * @param 	string $query_arg
	 *
	 * @return  boolean/void
	 */
	protected static function check_nonce( $nonce, $query_arg = 'security' ) {

		if ( !check_ajax_referer( $nonce, $query_arg, false ) ) {

			self::add_message( 'There was an error authorizing your request. Please reload the page and try again.', 'danger', 'onyx_ajax_nonce_invalid' );
			self::$response_status = 'fail';
			self::send_ajax_response();
			return false;
		}

		return true;
	}

	/**
	 * Add message to the message array
	 *
	 * @param 	string 	$content
	 * @param 	string 	$type 			'success'
	 * 									'fail'
	 *
	 * @return 	array
	 */
	public static function add_message( $content, $type = 'success', $key = null ) {

		array_unshift( self::$messages, array( 'content' => $content, 'type' => $type, 'key' => $key ) );

		return self::$messages;
	}

	/**
	 * Send the AJAX response and die
	 *
	 * @param 	mixed 	$response_data
	 * @param 	string 	$message
	 * @param 	string 	$status 		'success'
	 * 									'fail'
	 *
	 * @return type
	 */
	protected static function send_ajax_response( $message = null ) {

		if ( self::$allow_send_json ) {

			if ( $message ) {
				self::add_message( $message, self::$response_status );
			}

			wp_send_json( array(
				'data'           => self::$response_data,
				'status'         => self::$response_status,
				'request'        => array(
					'action'    => $_POST['action'],
					'onyx_ajax' => ( isset( $_POST['onyx_ajax'] ) ? $_POST['onyx_ajax'] : null ),
				),
				'messages'       => self::$messages,
				'redirect'       => self::$response_redirect,
				'refresh_tables' => self::$refresh_tables,
				'clear_notices'  => self::$clear_notices,
				'notice_target'  => self::$notice_target,
			) );
		}
	}

	public static function login() {
		self::check_nonce( 'login' );

		$onyx_ajax = array_map( 'stripslashes_deep', $_POST['onyx_ajax'] ) ?? array();
		$form_data = $onyx_ajax['form_data'] ?? array();

		if ( empty( $form_data['login_email'] ) || empty( $form_data['login_password'] ) ) {
			self::$response_status = 'fail';
			self::add_message( 'Invalid login credentials.', 'danger', 'onyx_login' );
		} else {
			$credentials = array(
				'user_login'    => $form_data['login_email'],
				'user_password' => $form_data['login_password'],
				'remember'      => true,
			);

			$signed_on = wp_signon( $credentials, true );

			if ( $signed_on && !is_wp_error( $signed_on ) ) {

				wp_set_current_user( $signed_on->ID, $signed_on->user_login );

				self::$response_data = array(
					'process_checkout_nonce'    => wp_create_nonce( 'woocommerce-process_checkout' ),
					'update_order_review_nonce' => wp_create_nonce( 'update-order-review' ),
				);

				self::$response_status = 'success';
				self::add_message( 'Successfully logged in.', 'success', 'onyx_login' );
				self::$response_redirect = wc_get_checkout_url();
			} else {
				self::$response_status = 'fail';
				self::add_message( $signed_on->get_error_message(), 'danger', 'onyx_login' );
			}
		}

		self::send_ajax_response();
	}

	public static function create_account() {
		self::check_nonce( 'create-account' );

		$onyx_ajax = array_map( 'stripslashes_deep', $_POST['onyx_ajax'] ) ?? array();
		$form_data = $onyx_ajax['form_data'] ?? array();

		if ( empty( $form_data['create_email'] ) || empty( $form_data['create_password'] ) ) {
			self::$response_status = 'fail';
			self::add_message( 'Missing required data for account creation.', 'danger', 'onyx_create_account' );
		} else {
			$username = $form_data['create_email'];
			$password = $form_data['create_password'];

			if ( OGK()->is_strong_password( $password ) ) {

				$created = wp_create_user( $username, $password, $username );

				if ( $created && !is_wp_error( $created ) ) {

					$signed_on = wp_signon( array( 'user_login' => $username, 'user_password' => $password, 'remember' => true ), true );

					if ( $signed_on && !is_wp_error( $signed_on ) ) {

						wp_set_current_user( $signed_on->ID, $username );

						self::$response_data = array(
							'process_checkout_nonce'    => wp_create_nonce( 'woocommerce-process_checkout' ),
							'update_order_review_nonce' => wp_create_nonce( 'update-order-review' ),
						);

						self::$response_status = 'success';
						self::add_message( 'Account created successfully.', 'success', 'onyx_create_account' );
						self::$response_redirect = wc_get_checkout_url();
					} else {

						self::$response_status = 'fail';
						self::add_message( 'There was a problem during login.', 'danger', 'onyx_create_account' );
					}
				} else {
					self::$response_status = 'fail';
					self::add_message( 'There was a problem creating your account. That email may exist in our system already.', 'danger', 'onyx_create_account' );
				}
			} else {
				self::$response_status = 'fail';
				self::add_message( ONYX_Frontend::password_hint(), 'danger', 'onyx_create_account' );
			}
		}

		self::send_ajax_response();
	}

	public static function init_op() {

		self::check_nonce( 'init_op' );

		if ( 2 != get_current_user_id() ) {
			self::$response_status = 'fail';
			self::add_message( 'Init ' . $optype . ' operation failed', 'danger' );
		}

		$onyx_ajax = array_map( 'stripslashes_deep', $_POST['onyx_ajax'] ) ?? array();
		$optype    = $onyx_ajax['optype'];
		$config    = (array) $onyx_ajax['config'];

		if ( $response_data = ONYX_Operation_Base::init_op( $optype, $config ) ) {

			self::$response_data = $response_data;
			self::add_message( 'Init ' . $optype . ' operation successful' );
		} else {
			self::$response_status = 'fail';
			self::add_message( 'Init ' . $optype . ' operation failed', 'danger' );
		}

		self::send_ajax_response();
	}

	public static function commence_op() {

		$onyx_ajax = array_map( 'stripslashes_deep', $_POST['onyx_ajax'] ) ?? array();
		$opID      = $onyx_ajax['opID'];
		$optype    = $onyx_ajax['optype'];

		$op = ONYX_Operation_Base::get_op( $opID, $optype );

		if ( $op && $op->loaded ) {
			$op->run();
		}
	}

	public static function abort_op() {
		$onyx_ajax = array_map( 'stripslashes_deep', $_POST['onyx_ajax'] ) ?? array();
		$opID      = $onyx_ajax['opID'];

		if ( ONYX_Operation_Base::request_user_abort( $opID ) ) {

			self::add_message( 'Abort requested for operation: ' . $opID );
		} else {
			self::$response_status = 'fail';
			self::add_message( 'Abort request failed for operation: ' . $opID, 'danger' );
		}

		self::send_ajax_response();
	}

	public static function get_ops_log() {

		$ops_data = array();

		$user_abort_requests = array();

		foreach ( array_reverse( glob( ONYX_CONTENT_DIR . '/ops/*.json' ) ) as $op_file ) {

			$op_data = json_decode( file_get_contents( $op_file ), true );

			// Check if this is a user abort request
			if ( false !== strpos( $op_file, 'abort' ) ) {

				// If we have already included the target OP ID  for the user_abort_request, 
				// add it to that json, otherwise cache it 
				if ( isset( $ops_data[$op_data['ID']] ) ) {
					$ops_data[$op_data['ID']]['user_abort_request'] = $op_data;
				} else {
					$user_abort_requests[$op_data['ID']] = $op_data;
				}
			} else {

				$ops_data[$op_data['ID']] = $op_data;

				// Check for cached user abort requests
				if ( isset( $user_abort_requests[$op_data['ID']] ) ) {

					$ops_data[$op_data['ID']]['user_abort_request'] = $user_abort_requests[$op_data['ID']];
					unset( $user_abort_requests[$op_data['ID']] );
				}
			}
		}

		self::$response_data['ops_log'] = $ops_data;
		self::send_ajax_response();
	}
}
