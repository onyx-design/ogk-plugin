import _ from "../vendor/lodash.min";

module.exports = function (action, payload, callback, security, ajax_args) {
  var ajax_data = {
    action: action,
    security: security,
    onyx_ajax: _.defaultTo(payload, {}),
  };

  if (onyx.debug) {
    console.log("AJAX Data", ajax_data);
  }

  var ajaxStart = Date.now();

  ajax_args = _.assignIn(
    {
      url: ajaxurl,
      type: "POST",
      data: ajax_data,
      cache: false,
      dataType: "json",
      complete: function (response, status) {
        if (onyx.debug) {
          console.log("AJAX Request Duration", Date.now() - ajaxStart);
          console.log("Raw AJAX Response", response);
        }

        if ("success" == _.get(response, "responseJSON.status")) {
          jQuery(document).trigger(action, response.responseJSON, response);
        }

        // Handle possible redirect from response
        if (_.get(response, "responseJSON.redirect")) {
          window.location = _.get(response, "responseJSON.redirect");
          // Do not continue processing response if redirecting
          return true;
        }

        // Handle possible table refresh from response //dev:generalize
        // if( _.get( response, 'responseJSON.refresh_tables' ) ) {
        // 	onyx.Tables.refreshTables();
        // }

        if (_.get(response, "responseJSON.clear_notices", false)) {
          onyx.notices.clearNotices();
        }

        if (
          _.isArray(_.get(response, "responseJSON.messages")) &&
          _.get(response, "responseJSON.showNotices", true)
        ) {
          onyx.notices.processNotices(response.responseJSON);
        }

        if (_.isFunction(callback)) {
          callback(response.responseJSON);
        }
      },
    },
    _.defaultTo(ajax_args, {})
  );

  return jQuery.ajax(ajax_args);
};
