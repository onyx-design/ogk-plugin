import _ from '../vendor/lodash.min';

module.exports = (function($) {

	var iconMap = {
		primary		: 'check',
		secondary	: 'question-circle',
		success		: 'check',
		danger		: 'exclamation-triangle',
		warning		: 'exclamation-triangle',
		info		: 'info',
		light		: 'question-circle',
		dark		: 'question-circle'
	};

	var noticeTargetDefault = '#onyx-dashboard-notices';

	var noticeTarget = '#onyx-dashboard-notices';

	function getNoticeLevel( notice ) {

		return _.get( notice, 'type', 'info' );
	}

	function getIconClass( notice ) {

		return ( 'fa-' + _.get( iconMap, getNoticeLevel( notice ), 'info' ) );
	}

	function setNotice( notice ) {

		// Build notice
		var $notice = $('<div>', 
			{
				class:'woocommerce-info alert alert-' + getNoticeLevel( notice ),
				role: 'alert',
				html: _.get( notice, 'content', '' )
			}
		)
		.prepend('<i class="fa ' + getIconClass(notice) + '"></i>');
		// .append('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');

		var $keyedNotice = false;

		if( _.get( notice, 'key' ) ) {

			$notice.attr('data-notice-key', notice.key );
			$keyedNotice = $('.alert[data-notice-key="' + notice.key + '"]');
		}

		if( $keyedNotice && $keyedNotice.length ) {

			$keyedNotice.replaceWith( $notice );
		}
		else {

			$(noticeTarget).append( $notice );
		}
	}

	function clearNotices(clearTarget) {
		clearTarget = _.defaultTo(clearTarget, noticeTarget);

		$(clearTarget + ' .woocommerce-info.alert').remove();
	}

	function processNotices( response ) {

		if( onyx.debug ) {
			console.log('processNotices', response);	
		}

		noticeTarget = _.get(response, 'notice_target', noticeTargetDefault );

		_.forEach( _.get(response, 'messages', {}), setNotice );
	}

	function removeNotice( key ) {
		
		$('.alert[data-notice-key="' + key + '"]').remove();
	}


	return {
		clearNotices: clearNotices,
		processNotices: processNotices,
		removeNotice: removeNotice,
		setNotice: setNotice
	};

})(jQuery);