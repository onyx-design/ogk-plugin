import _ from '../vendor/lodash.min';

module.exports = jQuery(function($) {

	var onyx_checkout = {
		init: function() {
			if( $(document.body).hasClass('woocommerce-checkout') ) {
				
				// Force select2 on billing_state since it's hidden
				$( '#billing_state' ).selectWoo();

				$(document.body).on( 'change', '#use-shipping-as-billing-address input', this.use_shipping_as_billing );

				// Switch between Login and Create accounts
				$(document.body).on( 'click', '.asc-login .show-create-account-form', this.switch_account_screen );
				$(document.body).on( 'click', '.asc-create-account .show-login-form', this.switch_account_screen );

				$(document.body).on( 'submit', '#asc-ajax-login', this.submit_login_form );
				$(document.body).on( 'submit', '#asc-ajax-create-account', this.submit_create_account_form );

				$(document.body).on( 'click', '.asc-checkout-step.step-inactive', this.switch_checkout_screen );

				$(document.body).on( 'click', '.woocommerce-shipping-fields .step-btn-wrapper', this.validate_checkout_form );
				$(document.body).on( 'click', '.woocommerce-shipping-fields .step-btn', this.proceed_to_payment );
				$(document.body).on( 'click', '.asc-checkout-step-edit', this.edit_shipping_fields );

				$(document.body).on( 'blur change', '.woocommerce-shipping-fields .validate-required input', this.validate_checkout_field );

				$(document.body).on( 'blur change', '.validate-required input', this.validate_continue_to_payment_step );

				$( 'form.checkout_coupon' ).off();
				$( 'form.checkout_coupon' ).show().submit( this.on_coupon_submit );
				$( document.body ).on( 'click', '.woocommerce-remove-coupon', this.on_remove_coupon );

				

				$(document.body).on( 'update_checkout', this.on_update_checkout );

				$(document.body).on( 'change', 'input#login-email, input#create-email', this.wc_set_cart_email );

				// Store Credit (Checkout)
				$(document.body).on( 'scroll', '.cart-credit-form input', function(e) {e.stopPropagation();e.preventDefault();return false;});
				// $(document.body).on('change', '.woocommerce-order_credit-fields__field-wrapper .update_totals_on_change', this.update_checkout);
				$(document.body).on('keypress', 'input#apply_credit_update', function(e) {
					if( e.which == 13 ) {
						$(this).trigger('change');

						var $cartCreditWrap 		= $('.cart-credit-form'),
						$cartCreditInputApply 	= $('input#apply_credit'),
						$cartCreditInputUpdate 	= $('input#apply_credit_update');

						$cartCreditInputApply.val( $cartCreditInputUpdate.val() );
						$('form.checkout').trigger('update_checkout');
					}
					
					

				});
				$(document.body).on('change', 'input#apply_credit_update', function() {
					// Force credit input to stay within it's valid range
					if( parseFloat( $(this).val() ) > parseFloat( $(this).attr('max') ) ) {
						$(this).val( $(this).attr('max') );
					} 
					else if ( parseFloat($(this).val()) < 0 ) {
						$(this).val(0.00);
					}

					$(this).val(parseFloat($(this).val()).toFixed(2));
				});


				$(document.body).on('click', '.cart-credit-action', function(e) {
					e.preventDefault();

					var $cartCreditWrap 		= $('.cart-credit-form'),
						$cartCreditInputApply 	= $('input#apply_credit'),
						$cartCreditInputUpdate 	= $('input#apply_credit_update');

					if( $(this).hasClass( 'cart-credit-save' ) ) {

						// Open / enable the cart credit edit form if it's not open
						if( !$cartCreditWrap.hasClass('credit-update-open') ) {

							$cartCreditWrap.addClass('credit-update-open' );
						}
						// Otherwise save / apply cart credit
						else {
							$cartCreditInputApply.val( $cartCreditInputUpdate.val() );
							$('form.checkout').trigger('update_checkout');
						}	
					}
					else if( $(this).hasClass( 'cart-credit-cancel' ) ) {
						$cartCreditWrap.removeClass('credit-update-open');
						$cartCreditInputUpdate.val($cartCreditInputUpdate.data('orig-value') );

					}
				});
			}
		},
		use_shipping_as_billing: function() {

			if( !$( this ).is( ':checked' ) ) {
				$( 'div.billing_address' ).slideDown();
			}
			else {
				$( 'div.billing_address').slideUp();
			}
		},
		switch_account_screen: function(event) {
			event.preventDefault();
			event.stopPropagation();
			$( '.asc-login' ).toggle();
			$( '.asc-create-account' ).toggle();
		},
		submit_login_form: function(event) {
			event.preventDefault();

			$('.asc-checkout-step[data-checkout-step="account"] .asc-checkout-step-body').block({
				message: null,
				overlayCSS: {
					background: '#fff',
					opacity: 0.6
				}
			});

			var serializedForm = $(this).serializeArray(),
				formData = {};

			$.each(serializedForm, function(index, value) {
				formData[value.name] = value.value;
			});

			onyx.ajax(
				'onyx_login',
				{
					form_data: formData
				},
				function(response) {
					// Do nonce stuff here to replace any invalidated nonces
					console.log('Login Response',response);

					if( 'success' == _.get( response, 'status' ) ) {

						$( '#woocommerce-process-checkout-nonce' ).val( _.get( response.data, 'process_checkout_nonce', '' ) );
						wc_checkout_params.update_order_review_nonce = _.get( response.data, 'update_order_review_nonce', '' );
						$( 'form.checkout' ).trigger( 'update' );
					}

					$('.asc-checkout-step[data-checkout-step="account"] .asc-checkout-step-body').unblock();
				},
				$(this).attr('data-form-nonce')
			);
		},
		submit_create_account_form: function(event) {
			event.preventDefault();

			$('.asc-checkout-step[data-checkout-step="account"] .asc-checkout-step-body').block({
				message: null,
				overlayCSS: {
					background: '#fff',
					opacity: 0.6
				}
			});

			var serializedForm = $(this).serializeArray(),
				formData = {};

			$.each(serializedForm, function(index, value) {
				formData[value.name] = value.value;
			});

			onyx.ajax(
				'onyx_create_account',
				{
					form_data: formData
				},
				function(response) {
					// Do nonce stuff here to replace any invalidated nonces
					console.log('Create Account Response',response);

					if( 'success' == _.get( response, 'status' ) ) {

						$( '#woocommerce-process-checkout-nonce' ).val( _.get( response.data, 'process_checkout_nonce', '' ) );
						wc_checkout_params.update_order_review_nonce = _.get( response.data, 'update_order_review_nonce', '' );
						$( 'form.checkout' ).trigger( 'update' );
					}
					
					$('.asc-checkout-step[data-checkout-step="account"] .asc-checkout-step-body').unblock();
				},
				$(this).attr('data-form-nonce')
			);
		},
		edit_shipping_fields: function(event) {
			event.preventDefault();

			var editStep = $(this).closest('.asc-checkout-step').data('checkout-step');

			// $('.asc-checkout-step.step-active').addClass('step-inactive').removeClass('step-active');
			$('.asc-checkout-step[data-checkout-step="'+editStep+'"]').removeClass('step-complete').addClass('step-active');
		},
		switch_checkout_screen: function(event) {

			var $checkoutSection = $(this),
				$checkoutSectionNotices = $checkoutSection.find('.asc-checkout-step-title .asc-checkout-step-notices');

			if($checkoutSection.data('checkout-step') === 'shipping'){

				if( !onyx_checkout.is_account_valid() ) {
					$checkoutSectionNotices.html('Please login or create an account above to continue.');
				} else {
					$('.asc-checkout-step-edit').click();
				}
			} else if($checkoutSection.data('checkout-step') === 'payment') {

				if( !onyx_checkout.is_account_valid() ) {
					$checkoutSectionNotices.html('Please login or create an account above to continue.');
				} else if( onyx_checkout.is_shipping_valid() ) {
					$('.woocommerce-shipping-fields .step-btn').click();
				} else {
					$checkoutSectionNotices.html('Please complete your shipping details above.');
					onyx_checkout.validate_checkout_form();
				}
			}
		},
		validate_checkout_form: function(event) {
			if($(this).find('button.step-btn').first().is(':disabled')) {
				$( '.woocommerce-shipping-fields .validate-required:not(.woocommerce-validated) input' ).each( function() {
					$(this).blur();
				});
			}
		},
		proceed_to_payment: function(event) {
			event.preventDefault();

			$('.asc-checkout-step[data-checkout-step="payment"]').find('.asc-checkout-step-title .asc-checkout-step-notices').html('');

			onyx_checkout.enable_place_order();

			$('.asc-checkout-step[data-checkout-step="shipping"]').removeClass('step-active').addClass('step-complete');
			$('.asc-checkout-step[data-checkout-step="payment"]').removeClass('step-inactive').addClass('step-active');
		},
		validate_checkout_field: function() {
			var wrapper = $(this).closest('.form-row');

			if( $(this).val() ) {
				wrapper.addClass('woocommerce-validated');
			} else {
				wrapper.addClass('woocommerce-invalid');
			}
		},
		is_account_valid: function() {
			return $('.asc-checkout-step.step-complete[data-checkout-step="account"]').length;
		},
		is_shipping_valid: function() {
			return !$('.woocommerce-shipping-fields .validate-required:not(.woocommerce-validated)').length;
		},
		validate_continue_to_payment_step: function() {

			if( onyx_checkout.is_shipping_valid() ) {
				$( '.woocommerce-shipping-fields .step-btn' ).attr( 'disabled', false );
			} else {
				$( '.woocommerce-shipping-fields .step-btn' ).attr( 'disabled', true );
			}
		},
		disable_place_order: function() {
			$('.place-order-btn').attr('disabled', true);
		},
		enable_place_order: function() {
			$('.place-order-btn').attr('disabled', false);
		},
		on_coupon_submit: function() {
			var $form = $( this );

			if ( $form.is( '.processing' ) ) {
				return false;
			}

			$form.addClass( 'processing' ).block({
				message: null,
				overlayCSS: {
					background: '#fff',
					opacity: 0.6
				}
			});

			var data = {
				security:		wc_checkout_params.apply_coupon_nonce,
				coupon_code:	$form.find( 'input[name="coupon_code"]' ).val()
			};

			$.ajax({
				type:		'POST',
				url:		wc_checkout_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'apply_coupon' ),
				data:		data,
				success:	function( code ) {
					$( '.woocommerce-error, .woocommerce-message' ).remove();
					$form.find( 'input[name="coupon_code"]' ).val('')
					$form.removeClass( 'processing' ).unblock();

					if ( code ) {
						$form.before( code );

						$( document.body ).trigger( 'update_checkout', { update_shipping_method: false } );
					}
				},
				dataType: 'html'
			});

			return false;
		},
		on_remove_coupon: function() {
			$( '.woocommerce-coupons-complete' ).block({
				message: null,
				overlayCSS: {
					background: '#fff',
					opacity: 0.6
				}
			});
		},
		update_checkout: function() {
			$('form.checkout').trigger('update_checkout');
		},
		on_update_checkout: function() {
			$( '.woocommerce-checkout-review-recurring-order-table' ).block({
				message: null,
				overlayCSS: {
					background: '#fff',
					opacity: 0.6
				}
			});

			$( '.woocommerce-shipping-address-complete' ).block({
				message: null,
				overlayCSS: {
					background: '#fff',
					opacity: 0.6
				}
			});

			$( '.woocommerce-billing-address-complete' ).block({
				message: null,
				overlayCSS: {
					background: '#fff',
					opacity: 0.6
				}
			});

			$( '.woocommerce-coupons-complete' ).block({
				message: null,
				overlayCSS: {
					background: '#fff',
					opacity: 0.6
				}
			});
		},
		wc_set_cart_email: function() {

			var email = $(event.target).val();

			$.post( ajax_object.ajax_url, {
				action: "fue_wc_set_cart_email",
				email: email,
				first_name: '',
				last_name: ''
			});
		}
	};

	onyx_checkout.validate_continue_to_payment_step();
	onyx_checkout.init();
});