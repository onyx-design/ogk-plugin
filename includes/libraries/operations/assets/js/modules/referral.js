import _ from '../vendor/lodash.min';

module.exports = jQuery(function($) {

	var onyx_referral = {
		init: function() {
			$(document.body).on( 'submit', '#asc-update-referral-code', this.submit_update_referral_code );
			$(document.body).on( 'submit', '#asc-send-referral', this.submit_send_referral );
		},
		// dev:improve Generalize these to be the same, too much copy/paste
		submit_update_referral_code: function(event) {
			event.preventDefault();

			var $submitBtn = $(this).find('[type=submit]');

			$submitBtn.prop('disabled',true);

			var serializedForm = $(this).serializeArray(),
				formData = {};

			$.each(serializedForm, function(index, value) {
				formData[value.name] = value.value;
			});

			onyx.ajax(
				'onyx_update_referral',
				{
					user_id: $(this).attr('data-user-id'),
					form_data: formData 
				},
				function(response) {
					$submitBtn.prop('disabled',false);
				},
				$(this).attr('data-form-nonce')
			);
		},
		submit_send_referral: function(event) {
			event.preventDefault();

			var serializedForm = $(this).serializeArray(),
				formData = {};

			$.each(serializedForm, function(index, value) {
				formData[value.name] = value.value;
			});

			onyx.ajax(
				'onyx_send_referral',
				{
					user_id: $(this).attr('data-user-id'),
					form_data: formData 
				},
				function(response) {
				},
				$(this).attr('data-form-nonce')
			);
		}
	};

	onyx_referral.init();
});