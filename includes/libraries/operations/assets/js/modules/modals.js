module.exports = (function($) {

	var instances = [];

	function Modal(container) {

		var $modal = (this.$modal = $(container));

		$modal[0].onyxModal = this;

		return {
			$modal: $modal,
		};
	}

	function initModals() {

		$('.onyx-modal').each(function() {
			instances.push( new Modal( $(this) ) );
		});
	}
	$(document).ready(initModals);

	$(document).ready(function() {

		$(document).on('click', 'a[href$="#onyx-modal"]', function(event) {
			event.preventDefault();
			$('.onyx-modal').modal({
				blockerClass: 'onyx-modal-wrapper',
				showClose: false
			});
		})

		$(document).on('click', 'a[href$="#onyx-modal-close"]', function(event) {
			event.preventDefault();
			$.modal.close();
		});

		// Set generalized modal event handlers
		$(document).on('click', '[data-onyx-toggle-modal]', function(event) {

			var $target = $(event.target);
			var $modal = $('#'+$target.data('onyx-toggle-modal'));

			if( $modal.length ) {

				$modal.modal();
			}
		});
	});

	return {
		instances: 	instances
	};
	
})(jQuery);