import _ from "./vendor/lodash.min";
import modal from "./vendor/jquery.modal.min";

global.onyx = window.onyx = {
  debug: false,
  ajax: require("./modules/ajax"),
  notices: require("./modules/notices"),
  checkout: require("./modules/checkout"),
  // referral: 		require('./modules/referral'),
  modals: require("./modules/modals"),
};

jQuery(document).ready(function($) {
  // PDP JS
  if ($("body").hasClass("single-product")) {
    // Sync PDP Options Blocks to select changes
    $("table.variations select")
      .change(function() {
        // Set corresponding block
        $(this)
          .closest("td.value")
          .children(".pdp-custom-option")
          .find('input[type="radio"]')
          .prop("checked", false)
          .filter('[data-option-value="' + $(this).val() + '"]')
          .prop("checked", true);

        var $selectedOption = $(this).find(
          'option[value="' + $(this).val() + '"]'
        );

        $(this)
          .closest("tr.asc-pdp-attribute")
          .children("td.label")
          .find(".attribute-selected-value")
          .html($selectedOption.html());
      })
      .each(function() {
        $(this).trigger("change");
      });

    $("table.variations .pdp-custom-option .asc-swatch").click(function() {
      var optionValue = $(this).attr("data-option-value");

      var $select = $(this)
        .closest("td.value")
        .find("select.select2-hidden-accessible");

      $select.val(optionValue).trigger("change");
    });

    // Make sure there is a variation selected
    $(".pdp-custom-option").each(function() {
      // Get associated select
      var $select = $(this)
        .closest("tr.asc-pdp-attribute")
        .children("td.value")
        .find("select");

      if (!$select.val()) {
        $(this)
          .find(".pdp-option-value")
          .first()
          .find("label")
          .first()
          .click();
      }
    });
  }

  // Subscription Frequency
  if ($("body").hasClass("woocommerce-view-subscription")) {
    $('input[name="new-frequency"], input[name="update-renewal"]').change(
      function(event) {
        var newRenewalDateTs,
          currentRenewalVal = $('input[name="current-renewal-date"]').val(),
          updateFromVal = $('input[name="update-from-date"]').val(),
          currentFrequencyVal = $('input[name="current-frequency"]').val(),
          newFrequencyVal = $('input[name="new-frequency"]:checked').val(),
          updateRenewalVal = $('input[name="update-renewal"]:checked').val();

        if (newFrequencyVal === currentFrequencyVal) {
          newRenewalDateTs = moment.unix(currentRenewalVal);
        } else if (updateRenewalVal === "next") {
          newRenewalDateTs = moment.unix(currentRenewalVal);
        } else {
          switch (newFrequencyVal) {
            case "2_months":
              newRenewalDateTs = moment.unix(updateFromVal).add(2, "months");
              if (newRenewalDateTs < moment()) {
                newRenewalDateTs = moment().add(2, "days");
              }
              break;
            case "4_months":
              newRenewalDateTs = moment.unix(updateFromVal).add(4, "months");
              break;
            default:
              return;
          }
        }

        $(".new-renewal-date .value").html(
          newRenewalDateTs.format("MMM D, YYYY")
        );
      }
    );

    $("#subscription-frequency-modal-form").on("submit", function(event) {
      var currentFrequencyVal = $('input[name="current-frequency"]').val(),
        newFrequencyVal = $('input[name="new-frequency"]:checked').val();

      if (newFrequencyVal === currentFrequencyVal) {
        event.preventDefault();
        event.stopPropagation();
        $.modal.close();
      }
    });
  }
});
