<?php
	defined( 'ABSPATH' ) || exit;

	$admin_disabled = is_onyx_admin() ? '' : 'disabled';
	$non_onyx_admin_hidden = is_onyx_admin() ? '' : ' style="display:none"';
?>
<h1>ONYX Custom Tools</h1>



<div class="container">
	<div class="row">
		<div class="col" id="onyx-dashboard-notices">

		</div>
	</div>
	<div class="row">
		<div class="col col-lg-6">
			<div class="bt-card" id="asc-opstart-card">
				<div class="card-header">
					<h5 class="card-title d-inline-block">Start Operation</h5>

					<select class="custom-select asc-op-select pr-4 float-right d-inline-block w-auto" id="asc-optype-select">
						<option selected>Select an operation</option>
						<option value="export_subscriptions_snapshot">Export Subscriptions Snapshot</option>
						<option value="export_orders">Export Orders</option>
						<option value="export_blog_posts">Export Blog Posts</option>
                        <option value="export_sub_line_items">Export Subscription Line Items </option>
						<option value="update_subscription_dates_feb_2021" <?php echo $admin_disabled;?>>Updated Subscription Dates (Feb 2021)</option>
						<option value="validate_subscriptions">Validate Subscriptions</option>
						<option value="validate_subscription_shipments">Validate Subscription Shipments</option>
						<option value="export_subscriptions">Export Subscriptions</option>
						<option value="send_may_shipping_emails" <?php echo $admin_disabled;?>>Send May Shipping Emails</option>
						<option value="generate_missed_shipments" <?php echo $admin_disabled;?>>Generate Missed Shipments</option>
						<option value="validate_addresses" <?php echo $admin_disabled;?>>Validate Addresses</option>
						<option value="fix_db_issues_202206" <?php echo $admin_disabled;?>>Fix DB Issues 202206</option>
					</select>

				</div>
				<div class="card-body ">
					<!-- fix_db_issues_202206 -->
					<form class="asc-op-config" id="op_config_fix_db_issues_202206" data-asc-optype="fix_db_issues_202206" data-form-nonce="<?php echo wp_create_nonce( 'init_op' ); ?>">

						<div class="form-group">
							<label>Fix DB Issues 202206</label>
						</div>

						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" id="fixdb202206_dry_run" name="dry_run" checked>
							<label class="custom-control-label" for="fixdb202206_dry_run">Dry Run (no actual updates)</label>
						</div>

						<button type="submit" class="btn btn-primary mb-2 float-right asc-op-start" value="fix_db_issues_202206">Start Operation</button>

					</form>
					<!-- /fix_db_issues_202206 -->
				<!-- export_subscriptions_snapshot -->
				<form class="asc-op-config" id="op_config_export_subscriptions_snapshot" data-asc-optype="export_subscriptions_snapshot" data-form-nonce="<?php echo wp_create_nonce( 'init_op' ); ?>">

						<div class="form-group">
							<label>Export Historical Snapshot of Subscriptions</label>
		

						</div>
						<div class="form-group">
						<label for="snapshot_date">Snapshot date:</label>
							<input type="date" id="snapshot_date" name="snapshot_date" max="<?php echo date("Y-m-d"); ?>">
						</div>
							


						<button type="submit" class="btn btn-primary mb-2 float-right asc-op-start" value="export_subscriptions_snapshot">Start Operation</button>

					</form>

					<!-- export_order_items -->
					<form class="asc-op-config" id="op_config_export_orders" data-asc-optype="export_orders" data-form-nonce="<?php echo wp_create_nonce( 'init_op' ); ?>">

						<div class="form-group">
							<label>Export Orders with State</label>
		

						</div>
						<div class="form-group">
						<label for="start">Start date:</label>
						<input type="date" id="start" name="exportStart"
							value="">
							</div>
							<div class="form-group">
							<label for="start">End date:</label>
						<input type="date" id="end" name="exportEnd"
							value="">
							</div>


						<button type="submit" class="btn btn-primary mb-2 float-right asc-op-start" value="export_orders">Start Operation</button>

					</form>

					<!-- export_order_items -->
					<form class="asc-op-config" id="op_config_export_sub_line_items" data-asc-optype="export_sub_line_items" data-form-nonce="<?php echo wp_create_nonce( 'init_op' ); ?>">

						<div class="form-group">
							<label>Export Sub Line Items</label>
						</div>

						<button type="submit" class="btn btn-primary mb-2 float-right asc-op-start" value="export_sub_line_items">Start Operation</button>

					</form>
					<!-- /export_order_items -->
					<form class="asc-op-config" id="op_config_update_subscription_dates_feb_2021" data-asc-optype="update_subscription_dates_feb_2021" data-form-nonce="<?php echo wp_create_nonce( 'init_op' ); ?>">

						<div class="form-group">
							<label>Update Subscription Dates (Feb 28 2021)</label>
						</div>
						<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input" id="cos_dry_run" name="dry_run" checked>
								<label class="custom-control-label" for="cos_dry_run">Dry Run (no actual updates)</label>
							</div>

						<button type="submit" class="btn btn-primary mb-2 float-right asc-op-start" value="update_subscription_dates_feb_2021">Start Operation</button>

						</form>


					<!-- check_orders -->
					<form class="asc-op-config" id="op_config_check_orders" data-asc-optype="check_orders" data-form-nonce="<?php echo wp_create_nonce( 'init_op' ); ?>">

						<div class="form-group">
							<label>Check Orders</label>
							<div class="form-group">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="cos_include_pending_orders" name="include_pending_orders" checked>
									<label class="custom-control-label" for="cos_include_pending_orders">Pending Orders</label>
								</div>
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="cos_include_processing_orders" name="include_processing_orders" checked>
									<label class="custom-control-label" for="cos_include_processing_orders">Processing Orders</label>
								</div>
							</div>
						</div>

						<button type="submit" class="btn btn-primary mb-2 float-right asc-op-start" value="check_orders">Start Operation</button>

					</form>
					<!-- /check_orders -->

					<!-- export_subscriptions -->
					<form class="asc-op-config" id="op_config_export_subscriptions" data-asc-optype="export_subscriptions" data-form-nonce="<?php echo wp_create_nonce( 'init_op' ); ?>">

						<div class="form-group">
							<label>Export Subscriptions</label>
						</div>

						<div class="form-group">
							<label>Include Statuses</label>
							<select class="custom-select" name="include_statuses" id="cos_include_statuses" >
								<?php
									$subscription_statuses = wcs_get_subscription_statuses();
									$default_statuses      = array( 'wc-active', 'wc-pending-cancel' );

									foreach ( $subscription_statuses as $status => $label ) {

										// $selected = in_array($status, $default_statuses) ? 'selected' : '';
										$selected = 'wc-active' == $status ? 'selected' : '';

										echo '<option value="' . $status . '" ' . $selected . '>' . $label . '</option>';
									}

								?>
							</select>
						</div>
						
						<div class="form-group" <?php echo $non_onyx_admin_hidden;?>>
							<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input" id="cos_update_next_shipment_date" name="update_next_shipment_date">
								<label class="custom-control-label" for="cos_update_next_shipment_date">Generate next shipment date?</label>
							</div>
						</div>

						<div class="form-group" <?php echo $non_onyx_admin_hidden;?>>
							<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input" id="cos_create_shipment_orders" name="create_shipment_orders">
								<label class="custom-control-label" for="cos_create_shipment_orders">Create shipment orders?</label>
							</div>
						</div>

						<div class="form-group" <?php echo $non_onyx_admin_hidden;?>>
							<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input" id="cos_dry_run" name="dry_run" checked>
								<label class="custom-control-label" for="cos_dry_run">Dry Run (no actual updates)</label>
							</div>
						</div>

						<button type="submit" class="btn btn-primary mb-2 float-right asc-op-start" value="export_subscriptions">Start Operation</button>

					</form>
					<!-- /export_subscriptions -->

					<!-- Validate Subscriptions -->
					<form class="asc-op-config" id="op_config_validate_subscriptions" data-asc-optype="validate_subscriptions" data-form-nonce="<?php echo wp_create_nonce( 'init_op' ); ?>">

						<div class="form-group">
							<label>Validate Subscriptions</label>
						</div>
						<div class="form-group">
							<label>Include Statuses</label>
							<select class="custom-select" name="include_statuses" id="cos_include_statuses" >
								<?php
									$subscription_statuses = wcs_get_subscription_statuses();
									$default_statuses      = array( 'wc-active', 'wc-pending-cancel' );

									foreach ( $subscription_statuses as $status => $label ) {

										// $selected = in_array($status, $default_statuses) ? 'selected' : '';
										$selected = 'wc-active' == $status ? 'selected' : '';

										echo '<option value="' . $status . '" ' . $selected . '>' . $label . '</option>';
									}

								?>
							</select>
						</div>


						<div class="form-group">
							<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input" id="cos_dry_run_val" name="dry_run_val" checked>
								<label class="custom-control-label" for="cos_dry_run_val">Dry Run (no actual updates)</label>
							</div>
						</div>

						<button type="submit" class="btn btn-primary mb-2 float-right asc-op-start" value="validate_subscriptions">Start Operation</button>

					</form>
					<!-- /Validate Subscriptions -->

					<!-- Validate Subscription Shipments -->
					<form class="asc-op-config" id="op_config_validate_subscription_shipments" data-asc-optype="validate_subscription_shipments" data-form-nonce="<?php echo wp_create_nonce( 'init_op' ); ?>">

						<div class="form-group">
							<label>Validate Subscription Shipments</label>
						</div>
						
						<div class="form-group">
							<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input" id="cos_output_valid_intervals" name="output_valid_intervals">
								<label class="custom-control-label" for="cos_output_valid_intervals">Output Valid Prepaid Intervals (as opposed to only invalid)</label>
							</div>
						</div>

						<button type="submit" class="btn btn-primary mb-2 float-right asc-op-start" value="validate_subscription_shipments">Start Operation</button>

					</form>
					<!-- /Validate Subscription Shipments -->

					<!-- send_may_shipping_emails -->
					<form class="asc-op-config" id="op_config_send_may_shipping_emails" data-asc-optype="send_may_shipping_emails" data-form-nonce="<?php echo wp_create_nonce( 'init_op' ); ?>">

						<div class="form-group">
							<label>Send May Shipping Emails</label>
						</div>
						<button type="submit" class="btn btn-primary mb-2 float-right asc-op-start" value="send_may_shipping_emails">Start Operation</button>

					</form>
					<!-- /send_may_shipping_emails -->


					<!-- generate_missed_shipments -->
					<form class="asc-op-config" id="op_config_generate_missed_shipments" data-asc-optype="generate_missed_shipments" data-form-nonce="<?php echo wp_create_nonce( 'init_op' ); ?>">

					<div class="form-group">
						<label>Generate Missed Shipments</label>
					</div>

					<div class="form-group">
						<label for="cos_missedships_subs_ids">Subscription IDs (comma separated)</label>
						<input type="text" class="form-control" id="cos_missedships_subs_ids" name="cos_missedships_subs_ids" >
					</div>

					<div class="form-group">
						<label for="cos_missedships_month">Fulfillment Month</label>
						<input type="text" class="form-control" id="cos_missedships_month" name="cos_missedships_month" >
						<small>Must be valid month format YYYYMM</small>
					</div>

					<button type="submit" class="btn btn-primary mb-2 float-right asc-op-start" value="generate_missed_shipments">Start Operation</button>

					</form>
          <!-- /generate_missed_shipments -->


	        <!-- validate_addresses -->
          <form class="asc-op-config" id="op_config_validate_addresses" data-asc-optype="validate_addresses" data-form-nonce="<?php echo wp_create_nonce( 'init_op' ); ?>">

            <div class="form-group">
              <label>Validate Addresses</label>
            </div>

            <button type="submit" class="btn btn-primary mb-2 float-right asc-op-start" value="validate_addresses">Start Operation</button>
          </form>
         <!-- /validate_addresses -->

	        <!-- export_blog_posts -->
          <form class="asc-op-config" id="op_config_export_blog_posts" data-asc-optype="export_blog_posts" data-form-nonce="<?php echo wp_create_nonce( 'init_op' ); ?>">

            <div class="form-group">
              <label>Export Blog Posts</label>
            </div>

            <button type="submit" class="btn btn-primary mb-2 float-right asc-op-start" value="export_blog_posts">Start Operation</button>
          </form>
         <!-- /export_blog_posts -->

				</div>
			</div>
		</div>
		<div class="col col-lg-6">
			<div class="bt-card">
				<div class="card-header">
					<h5 class="card-title d-inline-block">List Operations</h5>
					<button class="btn btn-primary float-right" id="refresh-ops-log">Refresh</button>

				</div>

				<div class="card-body list-group list-group-flush" id="asc-operations-list">

				</div>
			</div>
		</div>
	</div>

	<div class="row mt-4" id="view-op-wrap" style="display:none;">
		<div class="col-12 ">
			<div class="bt-card">
				<div class="card-header">
					<h5 class="card-title d-inline-block">View Operation: <span class="op-data" data-op-field="ID"></span></h5>
					<div class="float-right">
						<a href="" download class="btn btn-success" id="download-csv-view-op">Download CSV</a>
						<button class="btn btn-danger " id="abort-view-op">Abort</button>
						<button class="btn btn-primary " id="refresh-view-op">Refresh</button>
					</div>

				</div>
				<div class="row">
					<div class="col-12 col-lg-4">
						<table class="table">
							<tbody>
								<tr>
									<td>ID</td>
									<td class="op-data" data-op-field="ID"></td>
								</tr>
								<tr>
									<td>Type</td>
									<td class="op-data" data-op-field="type"></td>
								</tr>
								<tr>
									<td>Status</td>
									<td class="op-data" data-op-field="status"></td>
								</tr>
								<tr>
									<td>Start</td>
									<td class="op-data" data-op-field="start"></td>
								</tr>
								<tr>
									<td>End</td>
									<td class="op-data" data-op-field="end"></td>
								</tr>
								<tr>
									<td>Last Seen</td>
									<td class="op-data" data-op-field="last_seen"></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-12 col-lg-8">

						<pre id="view-op-log">

							</pre>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	jQuery(document).ready(function($) {

		var active_statuses = [
			'init', // Opertation has been initialized but not commenced
			'running', // Operation is running (only one operation can be running at any given moment)
			'jumping' // Operation is currently jumping (if no error has occured, the operation is still in progress)
		];

		var inactive_statuses = [
			'complete', // Operation completed successfully
			'cancelled', // Operation was cancelled while it was queued (cancelled before it began)
			'aborted', // Operation aborted itself due to error
			'user_aborted', // Operation was aborted by user
			'reset', // Operation status was forcefully reset
			'aborted_after_reset', // Operation detected that it was reset and aborted itself
			'failed' // Operation failed initial validation
		];

		$('#asc-optype-select').change(function() {
			$('.asc-op-config').hide().filter('[data-asc-optype="' + $(this).val() + '"]').show();
		});


		$('form.asc-op-config').submit(function(event) {
			event.preventDefault();
			// $(this).find('[type="submit"]').addClass('disabled').prop('disabled', true);

			// Get Form data
			var serializedForm = $(this).serializeArray();
			var formData = {};

			$.each(serializedForm, function(index, value) {
				formData[value.name] = value.value;
			});

			$(this).find('input[type="checkbox"]').each(function() {
				formData[$(this).attr('name')] = $(this).prop('checked');
			});

			// $(this).find('select').each(function() {
			// 	formData[$(this).attr('name')] = $(this).val().join(',');
			// });



			onyx.ajax(
				'onyx_init_op', {
					optype: $(this).attr('data-asc-optype'),
					config: formData
				},
				function(response) {
					if ('success' == response.status) {
						viewOp(response.data);
						refreshOpLog(true);
					}
				},
				$(this).attr('data-form-nonce')
			);




		});

		function refreshOpLog(force) {
			if (onyx.viewingOp === 'undefined' && typeof force !== 'undefined' && !force) {
				return false;
			}


			$.ajax({
				url: '/wp-content/onyx/ops/' + onyx.viewingOp.log_file,
				dataType: 'text',
				data: false,
				processData: false,
				cache: false,
				timeout: 2500,
				complete: function(data, status) {

					if (status == 'success') {
						var log_contents = data.responseText;
						// log_contents = log_contents.replace(/(?:\r\n|\n)/g, '<br />');
						$("#view-op-log").empty().append(log_contents);

						var viewOpLog = document.getElementById("view-op-log");
						viewOpLog.scrollTop = viewOpLog.scrollHeight;
					} else {
						$("#view-op-log").empty().append("Error retreiving log file...");
						// ovcop.stopLog();
					}
				}
			});

		}

		function viewOp(opData, refresh) {

			refresh = typeof refresh !== 'undefined' ? refresh : false;

			if (refresh) {

				$.ajax({
					url: '/wp-content/onyx/ops/' + onyx.viewingOp.op_file,
					dataType: 'json',
					data: false,
					processData: false,
					cache: false,
					timeout: 2500,
					complete: function(data, status) {



						if (status == 'success') {

							viewOp(data.responseJSON);
						} else {

							onyx.notices.setNotice({
								content: 'Failed to refresh OP data.',
								type: 'danger'
							});
						}
					}
				});
			} else {

				if (typeof opData !== 'undefined') {
					onyx.viewingOp = opData;

				}



				$('#view-op-wrap').find('.op-data').each(function() {
					$(this).html(onyx.viewingOp[$(this).data('op-field')]);
				});

				// $('#view-op-log').empty();

				refreshOpLog();

				// Maybe show download CSV link
				if ('complete' == onyx.viewingOp.status && typeof onyx.viewingOp.files !== 'undefined' && typeof onyx.viewingOp.files.export_csv !== 'undefined') {

					var csvFileUrl = '/wp-content/onyx/ops/' + onyx.viewingOp.files.export_csv;

					$('#download-csv-view-op').attr('href', csvFileUrl).show();
				} else {
					$('#download-csv-view-op').attr('href', '').hide();
				}

				$('#abort-view-op').toggle((-1 !== $.inArray(onyx.viewingOp.status, active_statuses) && !onyx.viewingOp.hasOwnProperty('user_abort_request')));

				$('#view-op-wrap').show();
			}



		}

		$('#refresh-view-op').click(function() {
			viewOp(false, true);
		});


		$('#abort-view-op').click(function() {
			onyx.ajax(
				'onyx_abort_op', {
					opID: onyx.viewingOp.ID,
				},
				function(response) {
					if ('success' == response.status) {
						viewOp(response.data);
						refreshOpLog();
					}
				}
				// $(this).attr('data-form-nonce')
			);
		});



		$('#asc-operations-list').on('click', '.ops-list-item', function() {

			viewOp(onyx.opsData[$(this).attr('data-op-id')]);
		});

		function refreshOpsLog() {
			onyx.ajax(
				'onyx_get_ops_log', {},
				function(response) {

					onyx.opsData = response.data.ops_log;

					// $opsList = $('#asc-operations-list');

					var opsListHtml = '';

					$.each(onyx.opsData, function(opID, opData) {

						// Determine contextual class
						var statusClass = false;
						switch (opData.status) {
							case 'running':
								statusClass = 'success';
								break;
							case 'user_aborted':
								statusClass = 'warning';
								break;
							case 'aborted':
								statusClass = 'danger';
								break;
							case 'jumping':
								statusClass = 'info';
								break;
							case 'failed':
								statusClass = 'danger';
								break;
							default:
								statusClass = false;
								break;
						}

						var opsListItemStatusClass = statusClass ? 'list-group-item-' + statusClass : '';
						var opsListItemBadgeClass = statusClass ? statusClass : 'info';

						opsListHtml += [
							'<div class="list-group-item list-group-item-action ops-list-item ' + opsListItemStatusClass + '" data-op-id="' + opData.ID + '">',
							opData.type,
							'<span class="badge badge-' + opsListItemBadgeClass + ' badge-pill float-right">' + opData.status + '</span>',
							'<div class="small text-muted">Last Seen: ' + opData.last_seen + '</div>',
							'</div>'
						].join("\n");

					});

					$('#asc-operations-list').html(opsListHtml);
				}
			);
		}

		$('#refresh-ops-log').click(refreshOpsLog);

		refreshOpsLog();
	});

	(function($) {


	})(jQuery);
</script>

<?php ?>