/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 11);
/******/ })
/************************************************************************/
/******/ ({

/***/ 11:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// Firefox back button fix
jQuery(window).bind('unload', function () {});

// Safari back button fix
jQuery(window).on('pageshow ', function (event) {
	if (event.originalEvent.persisted) {
		window.location.reload();
	}
});

var ascSignup = {
	step: 0
};

jQuery(document).ready(function ($) {

	function setVariation() {

		var $selectedVariation = $('input[type="radio"][name="variation_id"]').filter(':checked');
		var variationId = $selectedVariation.val();

		// Set variation IDs in other places
		$('.asc-signup-submit-btn.single_add_to_cart_button[type="submit"]').attr('data-product_id', variationId);
		$('input[name="add-to-cart"]').val(variationId);

		// Set handle color text
		var handleColorName = $selectedVariation.closest('[data-handle-color-name]').data('handle-color-name');
		$('.asc-signup-select-handle-value').html(handleColorName);
	}

	function setFrequency() {
		var $selectedFrequency = $('input[type="radio"][name="plan_frequency"]').filter(':checked');
		var frequencyName = $selectedFrequency.closest('[data-frequency-name]').data('frequency-name');

		$(".asc-signup-select-frequency-value").text(frequencyName);
	}

	// Add Classes for Functionality 

	$(".asc-slick-slider .asc-slick-slide").click(function () {

		$(this).addClass('selected').find('input[type="radio"]').prop('checked', true);
		$(this).siblings().removeClass('selected').find('input[type="radio"]').prop('checked', false);

		setVariation();
		setFrequency();
	});

	//    $('.asc-signup-select-handle-option').click(function() {    	

	// 	var clickedItemName = $(this).data('handle-color-name');
	// 	$(".asc-signup-select-handle-value").text(clickedItemName);

	//    });

	// $('.asc-signup-select-frequency-option').click(setFrequency);

	setVariation();
	setFrequency();

	// Step navigation
	$('[data-goto-asc-signup-step]').click(function () {

		var goToStep = $(this).data('goto-asc-signup-step');

		if ('checkout' == goToStep) {} else {

			$('.asc-signup-step.current-step').removeClass('current-step').fadeOut(300, function () {

				var $currentStep = $('.asc-signup-step[data-asc-signup-step="' + goToStep + '"]');

				$currentStep.addClass('current-step').fadeIn(300);
			});
		}
	});

	$('.asc-signup-addon-qty-wrap input[type="button"]').click(function () {

		var $qtyInput = $(this).siblings('input[type="number"]'),
		    qtyChange = $(this).hasClass('plus') ? 1 : -1;

		var newQty = parseInt($qtyInput.val()) + qtyChange;

		if (newQty >= 0) {
			$qtyInput.val(newQty);
		}
	});

	$('.asc-signup-addon-btn').click(function () {

		var $addonWrap = $(this).closest('.asc-signup-addon');

		if (!$addonWrap.hasClass('selected') && !$(this).hasClass('handle-not-selected')) {

			$addonWrap.addClass('selected');

			var addonType = $addonWrap.data('addon');

			$addonWrap.find('input.asc-addon-enable').prop('checked', true);
		}
	});

	$('.asc-signup-addon-remove-btn').click(function () {

		var $addonWrap = $(this).closest('.asc-signup-addon');

		if ($addonWrap.hasClass('selected')) {

			// Special handling to clear out Extra Handle Addon
			$addonWrap.find('.asc-signup-addon-handle-value').html('');
			$addonWrap.find('.asc-signup-addon-btn-extra-handle').addClass('handle-not-selected').html('Select Color');

			var addonType = $addonWrap.data('addon');
		}

		$addonWrap.removeClass('selected').find('input').prop('checked', false);
	});

	// Special handling for extra handle addon
	$('.asc-signup-addon-handle-swatches .asc-swatch').click(function () {

		var $addonWrap = $(this).closest('.asc-signup-addon');

		$addonWrap.find('.asc-signup-addon-btn').removeClass('handle-not-selected').html('Add');

		$addonWrap.find('.asc-signup-addon-handle-value').html($(this).data('option-title'));
	});

	$('.asc-signup-submit-btn').click(function (event) {

		if ($(this).hasClass('submitting')) {
			event.preventDefault();
			event.stopPropagation();
		}

		$(this).addClass('submitting');
	});
});

/***/ })

/******/ });
//# sourceMappingURL=onyx-asc-signup.js.map