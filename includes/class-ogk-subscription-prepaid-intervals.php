<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

class OGK_Subscription_Prepaid_Intervals {

	protected $subscription;

	public $intervals = array();

	public $non_shipment_orders = array();

	public $shipment_orders = array();

	protected $interval_template = array(
		
		'shipments_match'	=> null,
		'is_complete'		=> null,
		'is_valid'			=> null,
		'is_active'			=> null,
		'reason'			=> 'REASON_UNKNOWN',
		'target_shipments'	=> 1,
		'total_shipments'	=> 0,
		'shipment_stats'	=> array(
			'failed'			=> 0,
			'pending'			=> 0,
			'completed'			=> 0,
			'processing'		=> 0,
			'scheduled'			=> 0,
			'cancelled'			=> 0,
			'active'			=> 0,
			'on-hold'			=> 0,
			'expected'			=> 0
		),
		'start_order' 		=> null,
		'end_order'			=> null,
		'start_date'		=> null,
		'end_date'			=> null,
		'shipment_orders' 	=> array(),
	);
	

	public function __construct( OGK_WC_Subscription $subscription, $auto_analyze = true ) {


		$this->subscription = $subscription;


		if( $auto_analyze ) {
			$this->analyze_prepaid_intervals();
		}
		
	}


	public function analyze_prepaid_intervals() {

		// Ingest non-shipment orders
		foreach( array( 'parent', 'renewal', 'resubscribe' ) as $order_type ) {
			$this->initialize_orders_for_type( $order_type );
			ksort( $this->non_shipment_orders );
		}

		// Ingest shipment orders
		$this->initialize_orders_for_type( 'shipment' );

		$non_shipment_order_ids = array_keys( $this->non_shipment_orders );
		while( null !== ( $non_shipment_order_id = array_shift( $non_shipment_order_ids ) ) ) {
			
			$interval = $this->interval_template;

			$interval['target_shipments']	= $this->subscription->get_billing_interval_months();

			$interval['start_order'] = $this->non_shipment_orders[ $non_shipment_order_id ];
			$interval['start_date']	= $interval['start_order']['created'];
			$this->process_interval_order( $interval, $interval['start_order'] );

			// Determine if there is a next non-shipment order
			// This is the next (now first) item in $non_shipment_order_ids (or false if no more) because of the array_shift above
			$interval['end_order'] = current( $non_shipment_order_ids ) 
				? $this->non_shipment_orders[ current( $non_shipment_order_ids ) ]
				: false;
				
			/**
			 * Determine end date of interval, and if interval is current
			 * 	1. Next non-shipment order, if exists
			 *  2. If subscription is on hold, then does not matter
			 *  3. Scheduled end date or next payment date, if subscription is active (or pending-cancellation)
			 *  4. Subscription end date, if subscription has ended
			 */
			if( $interval['end_order'] ) {
				$interval['end_date'] = $interval['end_order']['created'];
				$interval['is_active'] = false;
				$interval['is_complete'] = false;
			}
			else {
				switch( $this->subscription->get_status() ) {
					case 'on-hold':
						$interval['is_active'] = false;
						$interval['is_valid'] = true;
						$interval['is_complete'] = false;
						$interval['reason']	= 'subscription_status_on_hold';
					break;
					case 'active':
					case 'pending-cancel':
						$interval['is_active'] = true;
						$interval['is_complete'] = false;

						// Modify end date to be the day before to avoid timezone issues with planned shipments
						$end_date = OGK_Subscription_Dates::init_datetime( $this->subscription->get_end_or_next_payment_date() );
						$interval['end_date'] = $end_date->modify( '-2 days' )->format( 'Y-m-d H:i:s');
					break;
					case 'ended':
					case 'cancelled':
						$interval['is_active'] = false;
						$interval['is_complete'] = false;
						$interval['end_date']	= $this->subscription->get_time( 'end' );
					break;
					default:
						$interval['is_active'] = false;
						$interval['is_valid'] = true;
						$interval['reason'] = 'subscription_status_invalid';
					break;
				}				
			}

			// Get existing shipment orders
			foreach( $this->shipment_orders as $shipment_order_id => $shipment_order_data ) {

				$shipment_order_date = $shipment_order_data['created'];
				if( $shipment_order_date > $interval['start_date']
					&& ( empty( $interval['end_date'] ) || $shipment_order_date < $interval['end_date'] )
				) {

					$interval['shipment_orders'][] = $shipment_order_data;

					unset( $this->shipment_orders[ $shipment_order_id ] );
				}
			}

			// Get scheduled and expected shipment orders
			if( $interval['is_active'] ) {

				$next_shipment_date = $this->subscription->get_date( 'next_shipment' );

				if( $next_shipment_date ) {

					$interval['shipment_orders'][] = array(
						'order_id'	=> 0,
						'relation'	=> 'shipment',
						'status'	=> 'scheduled',
						'created'	=> $next_shipment_date
					);

					$expected_shipment_date = $next_shipment_date;
					while( $interval['end_date'] > ( $expected_shipment_date = OGK_Subscription_Dates::date_modify_subscription_month( $expected_shipment_date ) ) ) {

						$interval['shipment_orders'][] = array(
							'order_id'	=> 0,
							'relation'	=> 'shipment',
							'status'	=> 'expected',
							'created'	=> $expected_shipment_date
						);
					}

				}
				

			}

			// Process interval shipment orders
			foreach( $interval['shipment_orders'] as $shipment_order_data ) {
				$this->process_interval_order( $interval, $shipment_order_data );
			}

			// Determine if interval is valid (if not already done)
			if( !isset( $interval['is_valid'] ) ) {

				if( $interval['total_shipments'] != $interval['target_shipments'] ) {

					$interval['shipments_match'] 	= false;
					$interval['is_valid']			= false;

					$interval['reason'] = ( $interval['total_shipments'] > $interval['target_shipments'] )
						? 'extra_shipments'
						: 'missed_shipments';
				}
				else {
					$interval['shipments_match'] 	= true;
					$interval['is_valid']			= true;
					$interval['reason']				= 'shipments_match';
				}
			}



			$this->intervals[] = $interval;
		}




		// Get parent order


	}


	protected function initialize_orders_for_type( $order_type, $skip_pre_may_2020 = true ) {
		
		foreach( $this->subscription->get_related_orders( 'all', $order_type ) as $order_id => $order ) {

			$order_date = $order->get_date_created();

			if( !is_a( $order_date, 'DateTime' ) 
				|| ( $skip_pre_may_2020 && $order_date->format( 'Y-m-d H:i:s' ) < '2020-05-01 00:00:00' )
			) {
				continue;
			}
			

			$order_data = array(
				'order_id'	=> $order_id,
				'relation'	=> $order_type,
				'status'	=> $order->get_status(),
				// 'order_obj'	=> $order,
				'created'	=> $order->get_date_created()->format( 'Y-m-d H:i:s' )
			);	

			$orders_prop_key = 'shipment' == $order_type ? 'shipment_orders' : 'non_shipment_orders';
			$this->{$orders_prop_key}[ $order_id ] = $order_data;
		};

		return true;
	}

	protected function process_interval_order( &$interval, $order_data ) {

		$order_status = $order_data['status'];
		$interval['shipment_stats'][ $order_status ]++;

		if( in_array( $order_status, array( 'completed', 'processing', 'scheduled', 'expected' ) ) ) {
			$interval['total_shipments']++;
		}
	}



	

}