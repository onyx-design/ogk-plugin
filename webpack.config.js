const path = require("path");
const webpack = require("webpack");
const CleanWebpackPlugin = require("clean-webpack-plugin");

// Paths
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
const paths = {
  dist: path.resolve(__dirname, "includes/libraries/operations/dist/js"),
  src: path.resolve(__dirname, "includes/libraries/operations/assets/js"),
};

// Webpack Config
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
module.exports = {
  watch: true,
  context: paths.src,
  entry: {
    onyx: "./onyx.js",
    "onyx-asc-signup": "./onyx-asc-signup.js",
  },
  output: {
    path: paths.dist,
    filename: "[name].js",
  },
  externals: {
    jquery: "jQuery",
  },
  devtool: "source-map",
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
            options: {
              presets: ["env"],
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(["includes/libraries/operations/dist/js"]),
    new webpack.ProgressPlugin(function (percentage, message) {
      const percent = Math.round(percentage * 100);
      process.stderr.clearLine();
      process.stderr.cursorTo(0);
      process.stderr.write(percent + "% " + message);
    }),
  ],
};
