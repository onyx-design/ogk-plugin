<?php

/**
 * Plugin Name: Onyx Green Kid
 * Description: Adds various functionality, tweaks, and fixes for greenkidcrafts.com
 * Version: 0.4.6
 * Author: Onyx Design
 * Author URI: http://onyxdesign.net/
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

define( 'ONYX_DEBUG', false );

if ( ONYX_DEBUG ) {
	// Define start time for debugging & performance measurement
	define( 'OGK_START_EXEC', ( microtime( true ) - $_SERVER["REQUEST_TIME_FLOAT"] ) );
}

final class OGK {

	protected static $_instance = null;

	public $version = '0.4.6';

	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	protected function __construct() {
		$this->define_constants();
		$this->includes();
		$this->init_hooks();
	}

	private function define_constants() {
		define( 'OGK_SLUG', 'ogk' );
		define( 'ONYX_SLUG', 'onyx' );

		define( 'OGK_PATH', realpath( dirname( __FILE__ ) ) );
		define( 'OGK_URL', plugins_url() . '/' . basename( dirname( __FILE__ ) ) . '/' );
		define( 'OGK_TEMPLATE_PATH', OGK_PATH . '/templates/' );

		define( 'ONYX_CONTENT_DIR', WP_CONTENT_DIR . '/onyx' );
		define( 'ONYX_CONTENT_URL', WP_CONTENT_URL . '/onyx' );
		define( 'ONYX_PLUGIN_FILE', __FILE__ );

		define( 'OGK_LIBRARIES_PATH', OGK_PATH . '/includes/libraries/' );
		
		define( 'OGK_ASSET_DIR', OGK_PATH . '/assets' );

		// Move these to something libraries specific?
		define( 'OGK_OPERATIONS_PATH', OGK_LIBRARIES_PATH . '/operations/' );
		define( 'OPERATIONS_TEMPLATE_PATH', OGK_OPERATIONS_PATH . '/templates/' );
		define( 'OPERATIONS_URL', plugins_url() . '/' . basename( dirname( __FILE__ ) ) . '/includes/libraries/operations/' );
	}

	private function includes() {
		include_once OGK_PATH . '/includes/class-onyx-autoloader.php';
		include_once OGK_PATH . '/includes/onyx-functions.php';

		// This should be loaded by the autoloader
		include_once OGK_PATH . '/includes/email/class-ogk-email-loader.php'; // OGK_Email_Loader

		include_once OGK_OPERATIONS_PATH . '/includes/class-operations-autoloader.php';

		if ( !$this->is_live_site() && ONYX_DEBUG ) {
			onyx_debug()->set_active();
		}
	}

	private function admin_includes() {
		include_once OGK_PATH . '/includes/admin/class-ogk-admin-page.php';

		/**
		 * OGK Operations
		 */
		include_once OGK_OPERATIONS_PATH . '/includes/admin/class-onyx-admin.php';
		include_once OGK_OPERATIONS_PATH . '/includes/admin/class-onyx-admin-template-loader.php';

		
	}

	private function init_hooks() {
		if ( !$this->is_live_site() ) {
			add_filter( 'woocommerce_subscriptions_is_duplicate_site', '__return_true' );
		}
		OGK_Hooks::init();
		add_action( 'init', array( $this, 'init' ), 0 );
	}

	public static function is_live_site() {
		return false !== strpos( get_site_url(), 'greenkidcrafts.com' );
	}

	public function init() {

		if ( !$this->is_live_site() ) {
			add_filter( 'https_local_ssl_verify', '__return_false' );
			add_filter( 'https_ssl_verify', '__return_false' );
		}

		if ( is_admin() ) {
			$this->admin_includes();

			// 
			add_action( 'init', function() {
				OGK_WCS_Action_Scheduler::check_scheduled_actions();
			}, 100 );
		}

		// Classes & actions loaded for the frontend
		if ( $this->is_request( 'frontend' ) ) {
			$this->frontend_includes();
		}

		if ( $this->is_request( 'ajax' ) ) {
			ONYX_AJAX::init();
		}
	}

	/**
	 * Get the plugin url.
	 *
	 * @return string
	 */
	public function plugin_url() {
		return untrailingslashit( plugins_url( '/', ONYX_PLUGIN_FILE ) );
	}

	/**
	 * Get the plugin path.
	 *
	 * @return string
	 */
	public function plugin_path() {
		return untrailingslashit( plugin_dir_path( ONYX_PLUGIN_FILE ) );
	}

	/**
	 * Get the template path.
	 *
	 * @return string
	 */
	public function template_path() {
		return apply_filters( 'onyx_template_path', 'onyx/' );
	}

	public function page_templates() {
		$page_templates = array();

		if( is_dir( OGK_TEMPLATE_PATH . 'pages/' ) ) {
			$page_templates = array_filter( array_map( function ( $template_path ) {

				if ( in_array( $template_path, array( '.', '..' ) ) ) {
					return '';
				}
	
				return 'onyx-' . wp_basename( $template_path );
			}, (array) scandir( OGK_TEMPLATE_PATH . 'pages/' ) ) );
		}

		return $page_templates;
	}

	public function frontend_includes() {

		// add_shortcode( 'gkc_subscribe_cta', function( $atts ) {
		// 	ob_start();
		// 	include_once( OGK_TEMPLATE_PATH . 'shortcode/ogk-shortcode-gkc-subscribe-cta.php' );
		// 	return ob_get_clean();
		// });

		// include_once(OGK_OPERATIONS_PATH . '/includes/frontend/class-onyx-frontend.php');
		// include_once(OGK_OPERATIONS_PATH . '/includes/frontend/class-onyx-frontend-assets.php');
		// include_once(OGK_OPERATIONS_PATH . '/includes/frontend/class-onyx-frontend-template-loader.php');

	}

	public function is_request( $type ) {
		switch ( $type ) {
			case 'admin':
				return is_admin();
			case 'ajax':
				return defined( 'DOING_AJAX' ) && DOING_AJAX;
			case 'cron':
				return defined( 'DOING_CRON' ) && DOING_CRON;
			case 'frontend':
				return ( !is_admin() || defined( 'DOING_AJAX' ) ) && !defined( 'DOING_CRON' );
			default:
				return false;
		}
	}
}

function OGK() {
	return OGK::instance();
}

// Make it happen
OGK();
