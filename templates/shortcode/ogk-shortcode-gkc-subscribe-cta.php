<?php
defined( 'ABSPATH' ) || exit;

/**
 * Improved GKC subscribe signup CTA
 */


?>

<style>
	<?php include_once( OGK_ASSET_DIR . '/dist/css/ogk.css' ); ?>
</style>
<div class="gkc--subscriptions gkc-subscribe-cta">
    <div class="image-container">
      	<img data-subscription-img src="https://cdn.greenkidcrafts.com/wp-content/uploads/2013/07/JoinBoxPhotos_01.jpg">
    </div>
    <div class="subscription-container">
		<div class="subscription-container-inner btns-shop-3d">

			<h2 style="text-align:center;">Subscribe now!</h2>
			
			<div class="box-variant variant-select">


				<input type="radio" id="juniorBoxes" value="junior" name="box-type">
				<label for="juniorBoxes" class="gkc-selectable-label">
					Junior Box
					<span class="label__subtitle">Ages 2-4</span>
				</label>
			
				<input type="radio" id="discoveryBoxes" name="box-type" value="discovery" checked>
				<label for="discoveryBoxes" class="gkc-selectable-label">
				Discovery Box
				<span class="label__subtitle">Ages 5-10+</span>
				</label>
			</div>
			
			<div class="subscription-type variant-select">
				<h4 class="variant-select__title">Prepay and save</h4>

				<input type="radio" id="monthly" name="subscription-type" value="1mo" checked>
				<label for="monthly" class="gkc-selectable-label">
					1 Month
					<span class="label__badge--pill">Save $5</span>
				</label>
			
				<input type="radio" id="3_months" name="subscription-type" value="3mo">
				<label for="3_months" class="gkc-selectable-label">
				3 Months
					<span class="label__badge--pill">Save $21!</span>
				</label>
			
				<input type="radio" id="6_months" name="subscription-type" value="6mo">
				<label for="6_months" class="gkc-selectable-label">
				6 Months
						<span class="label__badge--pill">Save $48!</span>
				</label>
				
				<input type="radio" id="12_months" name="subscription-type" value="12mo">
				<label for="12_months" class="gkc-selectable-label">
				12 Months
						<span class="label__badge--pill">Save $120!</span>
				</label>
			</div>
			<div class="subscription-info">
				
				<div class="subscription-price "><span data-variation-val="price">$29.95</span>/box</div>
				<div class="subscription-renewal-total">Pay <span data-variation-val="payment_total_due"></span> today</div>
				<div class="subscription-renewal-frequency">renews every <span data-variation-val="payment_frequency_text"></span></div>

				<div class="subscription-shipment-frequency">You will receive shipments <span data-variation-val="shipment_frequency_text">monthly</span></div>
				<div class="subscription-free-us-shipping" style="opacity:0;">Free shipping in the U.S!</div>
				
				


				<div class="subscription-cancellation">Cancel renewals anytime</div>
				
			</div>
			
			<a class="single_add_to_cart_button button" href="#" id="subscriptionPurchase">Add to Cart</a>
		</div>
    </div>
</div>


<script>

var variations = {
	junior_1mo: { 
		id: "276470", 
		title: "Monthly Subscription", 
		savings: "$5" , 
		price: "$29.95",
		payment_frequency_text: "month",
        payment_total_due: "$29.95",
		// shipment_frequency_text: "monthly",
		renewed: 30, 
		image: "https://cdn.greenkidcrafts.com/wp-content/uploads/2013/07/JoinBoxPhotos_01.jpg" 
	},
	junior_3mo: { 
		id: "276472", 
		title: "3 Month Subscription", 
		savings: "$21" , 
		price: "$27.95", 
		payment_frequency_text: "3 months",
        payment_total_due: "$83.85",
		renewed: 90, 
        image: "https://cdn.greenkidcrafts.com/wp-content/uploads/2013/07/JoinBoxPhotos_02.jpg"
	},
    junior_6mo: { 
        id: "276474", 
        title: "6 Month Subscription", 
        savings: "$48" , 
        price: "$26.95", 
        payment_frequency_text: "6 months",
        payment_total_due: "$161.70",
        renewed: 180, 
        image: "https://cdn.greenkidcrafts.com/wp-content/uploads/2013/07/JoinBoxPhotos_03.jpg" 
    },
    junior_12mo: { 
        id: "276476", 
        title: "12 Month Subscription", 
        savings: "$48" , 
        price: "$24.95", 
        payment_frequency_text: "12 months",
        payment_total_due: "$299.40",
        renewed: 360, 
        image: "https://cdn.greenkidcrafts.com/wp-content/uploads/2013/07/JoinBoxPhotos_04.jpg" 
    },
    discovery_1mo: { 
        id: "1588", 
        title: "Monthly Subscription", 
        savings: "$5" , 
        price: "$29.95", 
        payment_frequency_text: "month",
        payment_total_due: "$29.95",
        renewed: 30, 
        image: "https://cdn.greenkidcrafts.com/wp-content/uploads/2013/07/JoinBoxPhotos_01.jpg" 
    },
    discovery_3mo: { 
        id: "195672", 
        title: " 3 Month Subscription", 
        savings: "$21" , 
        price: "$27.95", 
        payment_frequency_text: "3 months",
        payment_total_due: "$83.85",
        renewed: 90, 
        image: "https://cdn.greenkidcrafts.com/wp-content/uploads/2013/07/JoinBoxPhotos_02.jpg" 
    },
    discovery_6mo: { 
        id: "195674", 
        title: "6 Month Subscription", 
        savings: "$48" , 
        price: "$26.95", 
        payment_frequency_text: "6 months",
        payment_total_due: "$161.70",
        renewed: 180, 
        image: "https://cdn.greenkidcrafts.com/wp-content/uploads/2013/07/JoinBoxPhotos_03.jpg" 
    },
    discovery_12mo: { 
        id: "1587", 
        title: "12 Month Subscription", 
        savings: "$120" , 
        price: "$24.95", 
        payment_frequency_text: "12 months",
        payment_total_due: "$299.40",
        renewed: 360, 
        image: "https://cdn.greenkidcrafts.com/wp-content/uploads/2013/07/JoinBoxPhotos_04.jpg" 
    }
    

};


function updateSubscriptionInfo() {
	var boxType = jQuery('input[name="box-type"]:checked').val();
	var subscriptionType = jQuery('input[name="subscription-type"]:checked').val();

	var variantKey = boxType + '_' + subscriptionType;

	var selectedVariant = variations[ variantKey ];


	jQuery.each( selectedVariant, function( key, value ) {
		if( jQuery('[data-variation-val="'+key+'"]').length > 0 ) {
            // console.log(key, value);
            // console.log(selectedVariant)
            jQuery('[data-variation-val="'+key+'"]').text( value );

		}
	})
    

	jQuery('[data-subscription-img]').attr('src', selectedVariant.image);
	jQuery('#subscriptionPurchase').attr('href', '/cart/?add-to-cart=' + selectedVariant.id );
}
jQuery(document).ready(function() {
	jQuery.getJSON('https://freegeoip.app/json/', function(data) {
		// If in US, show Free U.S. Shipping
		if( typeof data.country_code !== 'undefined' && 'US' == data.country_code ) {
			jQuery('.subscription-free-us-shipping').removeAttr('style');
		}
	})
	.error(function(data) {
		// If failed request, show Free U.S. shipping
		jQuery('.subscription-free-us-shipping').removeAttr('style');
	});
	
	updateSubscriptionInfo();
}); 
jQuery('.subscription-container input').on('click', updateSubscriptionInfo);


</script>