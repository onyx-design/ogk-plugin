<?php
	/**
	 * Customer completed order email
	 *
	 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-completed-order.php.
	 *
	 * HOWEVER, on occasion WooCommerce will need to update template files and you
	 * (the theme developer) will need to copy the new files to your theme to
	 * maintain compatibility. We try to do this as little as possible, but it does
	 * happen. When this occurs the version of the template file will be bumped and
	 * the readme will list any important changes.
	 *
	 * @see https://docs.woocommerce.com/document/template-structure/
	 * @package WooCommerce/Templates/Emails
	 * @version 3.7.0
	 */

	if ( !defined( 'ABSPATH' ) ) {
		exit;
	}

	/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email );?>

<?php /* translators: %s: Customer first name */
	$order_id = $order->get_id();
	$subscription = wcs_get_subscription($order_id);
	$payment_date = $subscription->get_date( 'next_payment' );


?>

<p><?php printf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $order->get_billing_first_name() ) );?></p>
<?php /* translators: %s: Site title */?>
<p><?php esc_html_e('We wanted to share with you an exciting development, we have just announced '); 
	echo '<a href="https://www.greenkidcrafts.com/green-kid-crafts-launches-a-new-line-of-boxes/">25 New Box themes for all subscribers</a>! '; 
	esc_html_e('In fact, you may have already noticed some new themes arriving and we hope your families are enjoying them. We know you have a choice, and we thank you for choosing us for your STEAM learning fun!'); ?></p> 
<p><?php esc_html_e( 'There is no further action needed to start receiving the new box themes as part of your subscription.' );?></p>
<p><?php esc_html_e( 'Your subscription is set to renew automatically on ' ); 
		 echo date('m-d-Y', strtotime($payment_date));
		 esc_html_e(' If you would like to manage your subscription please log in to your account '); echo '<a href="https://www.greenkidcrafts.com/my-account/">here</a>.'; ?>
		 </p>
<p><?php esc_html_e('Our support team can help you with any questions you may have, including:'); ?></p>
	<ul>
		<li><?php esc_html_e('Adding additional subscriptions to your account'); ?></li>
		<li><?php esc_html_e('Helping you switch between Junior (3-5) and Discovery (5-10+) to better meet your child’s needs!'); ?></li>
		<li><?php esc_html_e('Making changes to the billing cycle of your subscription'); ?></li>
	</ul>
	<p><?php esc_html_e('Please contact us at '); echo '<a href="mailto:care@greenkidcrafts.com">care@greenkidcrafts.com</a>'; esc_html_e(' if you need assistance or have questions.'); ?></p>
<?php

	
	/*
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
	do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

	/**
	 * Show user-defined additional content - this is set in each email's settings.
	 */
	if ( $additional_content ) {
		echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
	}
	?>
	<p>Thanks,</p>
	<p>The GKC Team</p>
	<?php
	/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
