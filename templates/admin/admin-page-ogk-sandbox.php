<?php
	// Exit if accessed directly
	defined( 'ABSPATH' ) || exit;

	/**
	 * Template Name: Sandbox
	 */

	error_reporting( E_ERROR | E_WARNING | E_PARSE );
	set_time_limit( 600 );
	ini_set( "max_execution_time", 600 );
	ini_set( "log_errors", 1 );

	// Easy Line breaks and text output
	define( 'BR', "<br />" );
	$br = "<br />";
	if ( !function_exists( 'br' ) ) {
		function br( $msg = '' ) {
			echo ( is_scalar( $msg ) ? ( is_bool( $msg ) ? boolstr( $msg ) . "<br />" : "$msg<br />" ) : print_r( $msg, true ) );
		}
	}

	function absmodulus( $dividend, $divisor ) {

		return ( ( ($dividend % $divisor) + $divisor ) % $divisor );

	}

?>
<h1>OGK Sandbox</h1>
<pre>
<?php

	br( 'months date diff - now -> 2021-06-20 (tz site): ' . OGK_Subscription_Dates::fulfillment_months_date_diff( 'now', '2021-06-20', 'site' ) );
	br( 'months date diff - now -> 2021-04-20 (tz site): ' . OGK_Subscription_Dates::fulfillment_months_date_diff( 'now', '2021-04-20', 'site' ) );
	br( 'months date diff - now -> 2021-07-01 01:20:00 (tz site): ' . OGK_Subscription_Dates::fulfillment_months_date_diff( 'now', '2021-07-01 01:20:00', 'site' ) );
	br( 'months date diff - now -> 2021-07-01 01:20:00 (tz gmt): ' . OGK_Subscription_Dates::fulfillment_months_date_diff( 'now', '2021-07-01 01:20:00', 'gmt' ) );
	br( 'months date diff - now -> 2021-07-01 10:20:00 (tz site): ' . OGK_Subscription_Dates::fulfillment_months_date_diff( 'now', '2021-07-01 10:20:00', 'site' ) );
	br( 'months date diff - now -> 2021-07-01 10:20:00 (tz gmt): ' . OGK_Subscription_Dates::fulfillment_months_date_diff( 'now', '2021-07-01 10:20:00', 'gmt' ) );
	br( 'months date diff - now -> 2021-06-01 01:20:00 (tz site): ' . OGK_Subscription_Dates::fulfillment_months_date_diff( 'now', '2021-06-01 01:20:00', 'site' ) );
	br( 'months date diff - now -> 2021-06-01 01:20:00 (tz gmt): ' . OGK_Subscription_Dates::fulfillment_months_date_diff( 'now', '2021-06-01 01:20:00', 'gmt' ) );

	$subscription = wcs_get_subscription( 247496 );
	$max_date_to_check = max( $subscription->get_date( 'last_shipment_order', 'site' ), $subscription->get_date( 'next_shipment', 'site' ) );
	$last_payment_time = max( $subscription->get_date( 'last_order_date_created' ), $subscription->get_date( 'last_order_date_paid' ) );

	br( 'current next_payment: ' . $subscription->get_date( 'next_payment' ) );
	br( 'calculated next_payment: ' . $subscription->calculate_date( 'next_payment' ) );
	br( 'max_date_to_check: ' . $max_date_to_check );
	br( 'last_payment_time: ' . $last_payment_time );

	br( 'current next_payment == calculated next_payment ?:');
	br( ( $subscription->get_date( 'next_payment' ) == $subscription->calculate_date( 'next_payment' ) ) );
	br();br();
	
	$test_order = wcs_get_subscription( 644667 );
	$shipment_orders = $test_order->get_related_orders( 'all', array( 'shipment' ) );
	$interval = 1;
	$test = '2020-05-20 23:59:59';
	foreach( $shipment_orders as $shipment_order ) {
		$shipment_order_date_created = $shipment_order->get_date_created()->format('Y-m-d H:i:s');
		if( $shipment_order_date_created <=  strtotime($test) ) {
			$interval++;
		br($shipment_order_date_created);
		br($interval);
		}

	}
	$dates = array(
		['2021-03-24 00:05:59',1],
		['2021-03-24 00:05:59',2],
		['2021-02-28 00:05:59',1],
		['2021-02-28 00:05:59',2],
		['2021-02-28 00:05:59',7],
		['2021-02-28 00:05:59',12],
		['2021-02-28 00:05:59',-1],
		['2021-02-28 00:05:59',-2],
		['2021-02-28 00:05:59',-7],
		['2021-02-28 00:05:59',-12],
		['2021-01-31 12:05:59',1],
		['2021-01-31 12:05:59',2],
		['2021-01-31 12:05:59',3],
		['2021-01-31 12:05:59',11],
		['2021-01-31 12:05:59',12],
		['2021-01-31 12:05:59',-1],
		['2021-01-31 12:05:59',-2],
		['2021-01-31 12:05:59',-3],
		['2021-01-31 12:05:59',-11],
		['2021-01-31 12:05:59',-12],
		
	);

	foreach ($dates as $datecheck) {
		br( "{$datecheck[0]} mod {$datecheck[1]} : ".OGK_Subscription_Dates::date_modify_subscription_month($datecheck[0],$datecheck[1]));
	}

	br( OGK_Subscription_Dates::date_modify_subscription_month() );
	
br();

	br( '-5 % 12: ' . (-5 % 12 ));
	br( '15 % 12: ' . (15 % 12 ));
	br( '5 % 12: ' . (5 % 12 ));
	br( '-7 % 12: ' . (-7 % 12 ));
	br( '-1 % 12: ' . (-1 % 12 ));
	br( '-13 % 12: ' . (-13 % 12 ));

	br();
	br('ABSMODULUS');
	br( '-5 % 12: ' . absmodulus(-5, 12 ));
	br( '15 % 12: ' . absmodulus(15, 12 ));
	br( '5 % 12: ' . absmodulus(5, 12 ));
	br( '-7 % 12: ' . absmodulus(-7, 12 ));
	br( '-1 % 12: ' . absmodulus(-1, 12 ));
	br( '-13 % 12: ' . absmodulus(-13, 12 ));

	br();
	br();
	br();
	$sub = wcs_get_subscription(494166);
	br( $sub->get_date( 'next_shipment') );
	br( $sub->get_date( 'next_shipment', 'site') );
	$time = new DateTime( '2021-03-01 04:15:42', new DateTimeZone( 'UTC' ) );
	// $time_site = new DateTime( $time_gmt->format('Y-m-d H:i:s'), new DateTimeZone( get_option( 'timezone_string') ) );
	br( 'time_gmt ' . $time->format('Y-m-d H:i:s') );
	$time->setTimezone( new DateTimeZone( get_option( 'timezone_string') ) );

	
	br( 'time_site ' . $time->format('Y-m-d H:i:s') );
	// All Current Active Emails
	// $email_classes['WC_Email_Customer_Processing_Order'] = new WC_Email_Customer_Processing_Order();
	// $email_classes['WC_Email_Customer_Completed_Order']  = new WC_Email_Customer_Completed_Order();
	// $email_classes['WC_Email_Customer_On_Hold_Order']    = new WC_Email_Customer_On_Hold_Order();

	// $email_classes['WCS_Email_Completed_Renewal_Order']        = new WCS_Email_Completed_Renewal_Order();
	// $email_classes['WCS_Email_Completed_Switch_Order']         = new WCS_Email_Completed_Switch_Order();
	// $email_classes['WCS_Email_Customer_On_Hold_Renewal_Order'] = new WCS_Email_Customer_On_Hold_Renewal_Order();
	// $email_classes['WC_Email_Customer_Reset_Password']   = new WC_Email_Customer_Reset_Password();
	// $email_classes['WC_Email_Customer_New_Account']      = new WC_Email_Customer_New_Account();
	// $email_classes['WCS_Email_Customer_Payment_Retry']         = new WCS_Email_Customer_Payment_Retry();

	// Other WCS Emails
	// $email_classes['WCS_Email_New_Renewal_Order']              = new WCS_Email_New_Renewal_Order();
	// $email_classes['WCS_Email_New_Switch_Order']               = new WCS_Email_New_Switch_Order();
	// $email_classes['WCS_Email_Processing_Renewal_Order'] = new WCS_Email_Processing_Renewal_Order();
	// $email_classes['WCS_Email_Customer_Renewal_Invoice']       = new WCS_Email_Customer_Renewal_Invoice();
	// $email_classes['WCS_Email_Cancelled_Subscription']         = new WCS_Email_Cancelled_Subscription();
	// $email_classes['WCS_Email_Expired_Subscription']           = new WCS_Email_Expired_Subscription();
	// $email_classes['WCS_Email_On_Hold_Subscription']           = new WCS_Email_On_Hold_Subscription();

	// $order_id = 445084;
	// foreach ( $email_classes as $email ) {
	// 	 $email->trigger( $order_id );
	// }

?>
</pre>